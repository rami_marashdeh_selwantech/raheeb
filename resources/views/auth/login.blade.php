@extends('dashboard.layouts.auth')

@section('content')



<section class="flexbox-container">
    <div class="col-12 d-flex align-items-center justify-content-center">
        <div class="col-md-4 col-10 box-shadow-2 p-0">
            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                <div class="card-header border-0">
                    <div class="card-title text-center">
                        <img src="/app-assets/images/logo/logo-dark.png" alt="branding logo">
                    </div>

                </div>
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}" novalidate>
                            @csrf
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="email" value="{{ old('email') }}" name="email"
                                    class="form-control @error('email') is-invalid @enderror" id="email"
                                    placeholder="@lang('global.enter_email')" required>
                                <div class="form-control-position">
                                    <i class="ft-mail"></i>
                                </div>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </fieldset>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="password" class="form-control @error('password') is-invalid @enderror"
                                    name="password" id="password" placeholder="@lang('global.enter_password')" required>
                                <div class="form-control-position">
                                    <i class="fa fa-key"></i>
                                </div>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </fieldset>
                            <div class="form-group row">
                                <div class="col-md-6 col-12 text-center text-sm-left">
                                    <fieldset>
                                        <input {{ old('remember') ? 'checked' : '' }} type="checkbox" name="remember"
                                            id="remember" class="chk-remember">
                                        <label for="remember">@lang('global.remember_me')</label>
                                    </fieldset>
                                </div>
                                @if (Route::has('password.request'))

                                <div class="col-md-6 col-12 float-sm-left text-center text-sm-right"><a
                                        href="{{ route('password.request') }}"
                                        class="card-link">@lang('global.forgot_password')</a></div>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-outline-info btn-block"><i class="ft-unlock"></i>
                                @lang('global.login')</button>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
@endsection