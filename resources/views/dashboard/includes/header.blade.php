<!-- fixed-top-->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-semi-dark navbar-shadow">
  <div class="navbar-wrapper">
    <div class="navbar-header">
      <ul class="nav navbar-nav flex-row">
        <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs"
            href="#"><i class="ft-menu font-large-1"></i></a></li>
        <li class="nav-item"><a class="navbar-brand" href="{{route('dashboard.index')}}"><img class="brand-logo"
              alt="robust admin logo" src="/app-assets/images/logo/logo-light-sm.png">
            <h3 class="brand-text">Raheeb</h3>
          </a></li>
        <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse"
            data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a></li>
      </ul>
    </div>
    <div class="navbar-container content">
      <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="nav navbar-nav mr-auto float-left">
          <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                class="ft-menu"> </i></a></li>


        </ul>
        <ul class="nav navbar-nav float-right">
         <ul class="nav navbar-nav float-right">
          @if(app()->isLocale('en'))
          <li class="dropdown dropdown-language nav-item"><a class="dropdown-toggle nav-link" id="dropdown-flag"
              href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                class="flag-icon flag-icon-gb"></i><span>@lang('global.english')</span><span class="selected-language"></span></a>
            @else
          <li class="dropdown dropdown-language nav-item"><a class="dropdown-toggle nav-link" id="dropdown-flag"
              href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                class="flag-icon flag-icon-sa"></i><span>@lang('global.arabic')</span><span class="selected-language"></span></a>

            @endif
         {{--   <div class="dropdown-menu" aria-labelledby="dropdown-flag"><a class="dropdown-item"
                href="{{route('dashboard.lang','en')}}"><i class="flag-icon flag-icon-gb"></i>@lang('global.english')</a>
              <a class="dropdown-item" href="{{route('dashboard.lang','ar')}}"><i class="flag-icon flag-icon-sa">
                </i>@lang('global.arabic')</a></div>
                --}}
          </li>





          <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#"
              data-toggle="dropdown"><span class="avatar avatar-online"><img src="{{auth()->user()->avatar != null ? auth()->user()->avatar : asset('imgs/profile-avatar-default.png')}}"
                  alt="avatar"><i></i></span><span class="user-name">{{auth()->user()->name}}</span></a>
            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="{{route('dashboard.admin.edit' , auth()->user()->id)}}"><i
                  class="ft-user"></i>@lang('global.edit_profile')</a>
              <div class="dropdown-divider"></div><a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        @lang('global.logout')
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </div>

          </li>
        </ul>
      </div>
    </div>
  </div>
</nav>



@push('js')

@endpush
