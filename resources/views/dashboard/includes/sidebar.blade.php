<div class="main-menu menu-fixed menu-dark menu-accordion    menu-shadow " data-scroll-to-active="true">
        <div class="main-menu-content">
                <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                        <li class=" nav-item ">
                                <a href="{{route('dashboard.index')}}">
                                        <i class="ft-home"></i>
                                        <span class="menu-title"></span>
                                        @lang('global.dashboard')
                                </a>
                        </li>
                        <!-- Start User Managment -->
                        <li class="nav-item">
                          <a href="index.html">
                            <i class="ft-users"></i>
                            <span class="menu-title">@lang('global.users_managment')</span>
                          </a>
                          <ul class="menu-content">
                            @can('m_user_view')
                            <li class="">
                              <a class="menu-item" href="{{route('dashboard.user.index')}}">@lang('global.users')</a>
                            </li>
                            @endcan
                            @can('m_super_admin_view')
                            <li class="">
                              <a class="menu-item" href="{{route('dashboard.admin.index')}}">@lang('global.employees')</a>
                            </li>
                            @endcan
                          </ul>
                        </li>
                        <!-- End User Managment -->

                        <!-- Start Category Managment -->
                        @can('m_categories_view')
                        <li class="">
                                <a href="{{route('dashboard.category.index')}}">
                                        <i class="fa fa-th-large"></i>
                                        <span class="menu-title"></span>
                                        @lang('global.categories')
                                </a>
                        </li>
                        @endcan
                        <!-- End Category Managment -->

                        <!-- Start Conditions Managment -->
                        @can('m_conditions_view')
                        <li class="">
                                <a href="{{route('dashboard.condition.index')}}">
                                        <i class="fa fa-th-large"></i>
                                        <span class="menu-title"></span>
                                        @lang('global.conditions')
                                </a>
                        </li>
                        @endcan
                        <!-- End Conditions Managment -->

                        <!-- Start Distance Managment -->
                        @can('m_settings_view')
                        <li class="">
                                <a href="{{route('dashboard.distance.index')}}">
                                        <i class="fa fa-th-large"></i>
                                        <span class="menu-title"></span>
                                        @lang('global.distance')
                                </a>
                        </li>
                        @endcan
                        <!-- End Distance Managment -->

                        <!-- Start Post Managment -->
                        @can('m_post_view')
                        <li class="">
                                <a href="{{route('dashboard.post.index')}}">
                                        <i class="fa fa-th-large"></i>
                                        <span class="menu-title"></span>
                                        @lang('global.products')
                                </a>
                        </li>
                        @endcan
                        <!-- End Post Managment -->

                        <!-- Start Post Sold Managment -->
                        @can('m_post_solds_view')
                        <li class="">
                                <a href="{{route('dashboard.post_sold.index')}}">
                                        <i class="fa fa-th-large"></i>
                                        <span class="menu-title"></span>
                                        @lang('global.products_sold')
                                </a>
                        </li>
                        @endcan
                        <!-- End Post Sold Managment -->

                        <!-- Start Post Report Managment -->
                        @can('m_post_report_view')
                        <li class="">
                                <a href="{{route('dashboard.post_report.index')}}">
                                        <i class="fa fa-th-large"></i>
                                        <span class="menu-title"></span>
                                        @lang('global.products_report')
                                </a>
                        </li>
                        @endcan
                        <!-- End Post Report Managment -->

                        <!-- Start Ticket Managment -->
                        @can('m_user_view')
                        <li class="">
                                <a href="{{route('dashboard.user_report.index')}}">
                                        <i class="fa fa-th-large"></i>
                                        <span class="menu-title"></span>
                                        @lang('global.user_report')
                                </a>
                        </li>
                        @endcan
                        <!-- End Ticket Managment -->

                        <!-- Start Ticket Managment -->
                        @can('m_ticket_view')
                        <li class="">
                                <a href="{{route('dashboard.ticket.index')}}">
                                        <i class="fa fa-th-large"></i>
                                        <span class="menu-title"></span>
                                        @lang('global.ticket')
                                </a>
                        </li>
                        @endcan
                        <!-- End Ticket Managment -->

                        <!-- Start Settings Managment -->
                        @can('m_settings_view')
                        <li class="">
                                <a href="{{route('dashboard.settings.index')}}">
                                    <i class="ft-settings"></i>
                                    <span class="menu-title">@lang('global.settings')</span>
                                </a>
                        </li>
                        @endcan
                        <!-- End Settings Managment -->
                </ul>
        </div>
</div>
