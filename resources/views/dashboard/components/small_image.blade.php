<div class="update-avatar-container">
  <div class="avatar-container">
    <img  style="width:150px; height: 150px;" id="img-avatar" src="{{ isset($avatar) ?$avatar:asset('imgs/profile-avatar-default.png')}}">
  </div>
  <button type="button" id="btn-select-avatar" class="btn btn-primary">
    <i class="ft-image"></i>
    @lang('global.select_image')</button>
  <input name="image" type="file" id="file-avatar">
</div>

@push('js')
<script>
  $("#btn-select-avatar").on("click",function(){
        $("#file-avatar").click();
        $("#file-avatar").change(function(event){
          var fullPath =URL.createObjectURL(event.target.files[0])
          $("#img-avatar").attr("src",fullPath);
        });
      });
</script>
@endpush