<div class="update-avatar-container">
  <div class="avatar-container">
    <img style="width:50px; height: 50px;" id="img-avatar" src="{{ isset($avatar) ?$avatar:asset('imgs/profile-avatar-default.png')}}">
  </div>
  <input name="image" type="file" id="file-avatar">
</div>

@push('js')
<script>
  $("#img-avatar").on("click",function(){
        $("#file-avatar").click();
        $("#file-avatar").change(function(event){
          var fullPath =URL.createObjectURL(event.target.files[0])
          $("#img-avatar").attr("src",fullPath);
        });
      });
</script>
@endpush