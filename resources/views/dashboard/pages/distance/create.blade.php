<div id="div_create-model-distance" class="modal-create">

     <div class="modal-content-create">
       <div class="modal-header-create">
         <span class="model-font-header">@lang('global.distance')</span>
       </div>
       <div class="modal-body-create">
         <form class="storForm-condition" id="storForm-distance" enctype="multipart/form-data" action="#"
           novalidate method="POST">
           @csrf



           <div class="row">
               <div class="form-check form-control-sm col-md-8 mb-0">
                 <label for="title">@lang('global.distance')</label>
                 <input type="number" id="distance" class="form-control form-control-sm @error('distance') is-invalid @enderror"
                   placeholder="@lang('global.distance')" required  name="distance">
               </div>
           </div>

           <div class="row">
             <div class="form-check form-control-sm col-md-12 mb-0">
               <button type="submit" id="submit" class="btn btn-success">@lang('global.add')</button>
             </div>
           </div>

         </form>
       </div>
       <div class="modal-footer-create">
         <span class="model-font-header">@lang('global.distance')</span>
       </div>
     </div>

   </div>



@push('js')
<script>

</script>
@endpush
