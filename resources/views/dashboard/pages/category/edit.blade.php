@extends('dashboard.layouts.index')
@section('title')
@lang('global.users')
@endsection

@section('content')
<form class="form" id="form" enctype="multipart/form-data" action="{{route("dashboard.category.update.all" , $category->id)}}" novalidate method="POST">
    @csrf
    <div class="card">
        <div class="card-header">
            <ul class="breadcrumb">
              <li><a href="{{route('dashboard.index')}}">@lang('global.dashboard')</a></li>
              <li><a href="{{route('dashboard.category.index')}}">@lang('global.categories')</a></li>
              <li>@lang('global.edit_category')</li>
            </ul>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="form-body">
                                <div class="row">

                                    <div class="form-group form-control-sm col-md-6 mb-0">
                                        <div class="form-group form-control-sm col-md-12 mb-0">
                                            <label for="name">@lang('global.name')</label>
                                            <input type="text" id="name" class="form-control form-control-sm @error('name') is-invalid @enderror" placeholder="@lang('global.name')" required value="{{$category->name}}" name="name">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="form-group form-control-sm col-md-12 mb-0">
                                            <label for="ordering">@lang('global.ordering')</label>
                                            <input type="number" id="ordering" class="form-control form-control-sm @error('ordering') is-invalid @enderror" placeholder="@lang('global.ordering')" required value="{{$category->ordering}}" name="ordering">
                                            @error('ordering')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="form-group form-control-sm col-md-12 mb-0">
                                            <input  type="checkbox"  id="is_ship_nationwide" value="1" name="is_ship_nationwide" @if($category->is_ship_nationwide == 1) checked @endif>
                                            <label for="is_ship_nationwide">@lang('global.is_ship_nationwide')</label>
                                        </div>

                                        <div class="form-group form-control-sm col-md-12 mb-0">
                                            <input  type="checkbox"  id="is_valid" value="1" name="is_valid" @if($category->is_valid == 1) checked @endif>
                                            <label for="is_valid">@lang('global.is_valid')</label>
                                        </div>

                                    </div>

                                    <div class="form-check form-control-sm col-md-6 mb-0">
                                        <div class="update-avatar-container">
                                            <label for="name">@lang('global.image')</label>
                                            <div>
                                                <img  style="width:200px; height: 100px;" id="img-image" src="{{ isset($category->image) ?$category->image:asset('images-app/image-not-found.png')}}">
                                                <div id="div_image-model-service" class="modal-create">
                                                    <div class="modal-content-create">
                                                        <div class="modal-body-create">
                                                            <img style="display:block; width:100%; height:100%;object-fit: cover;" id="img-image-model" src="{{ isset($category->image) ?$category->image:asset('images-app/image-not-found.png')}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" id="btn-select-avatar" class="btn btn-primary  btn-sm">
                                                <i class="ft-image"></i> @lang('global.select_image')
                                            </button>
                                            <input name="image" type="file" id="file-avatar">
                                        </div>
                                    </div>

                                    <div class="form-group form-control-sm col-md-12 mb-0">
                                        <a href="{{route("dashboard.category.update.all" , $category->id)}}" onclick="event.preventDefault();document.getElementById('form').submit();" class="btn btn-info btn-sm">
                                          <i class="ft-save"></i>
                                          @lang('global.save')
                                        </a>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@push('js')
<script>
  $("#btn-select-avatar").on("click",function(){
        $("#file-avatar").click();
        $("#file-avatar").change(function(event){
          var fullPath =URL.createObjectURL(event.target.files[0])
          $("#img-image").attr("src",fullPath);
          $("#img-image-model").attr("src",fullPath);
        });
      });

    var modal = document.getElementById("div_image-model-service");
     var img = document.getElementById("img-image");

     img.onclick = function() {
      modal.style.display = "block";
     }

     window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none";
        }
      }
</script>
@endpush
