@extends('dashboard.layouts.index')
@section('title')
@lang('global.categories')
@stop

@section('content')
<div class="card">
  <div class="card-header">
    <h4 class="card-title">@lang('global.categories')</h4>
    <div class="heading-elements">
      <a href="{{route('dashboard.category.create')}}" id="btn-create" class="btn btn-success">
        <i class="ft-plus"></i>
        @lang('global.create')
      </a>
    </div>
  </div>
  <div class="card-content">
    <div class="card-body">
      @if (session('success'))
      <div class="alert alert-success" role="alert">
        {{ session('success') }}
      </div>
      @endif
      <table id="dt-categories" class="table table-striped table-bordered complex-headers">
        <thead>
          <tr>
            <th>#</th>
            <th>@lang('global.image')</th>
            <th>@lang('global.name')</th>
            <th>@lang('global.is_ship_nationwide')</th>
            <th>@lang('global.is_valid')</th>
            <th>@lang('global.ordering')</th>
            <th>@lang('global.created_at')</th>
            <th>@lang('global.acction')</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
              <th>#</th>
              <th>@lang('global.image')</th>
              <th>@lang('global.name')</th>
              <th>@lang('global.is_ship_nationwide')</th>
              <th>@lang('global.is_valid')</th>
              <th>@lang('global.ordering')</th>
              <th>@lang('global.created_at')</th>
              <th>@lang('global.acction')</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
@stop

@push("js")
<script>
$(function(){
    var table = $("#dt-categories").DataTable({
        processing: true,
        serverSide: true,
        responsive: {
            details: true
        },
        ajax: '{{route("dashboard.category.datatable")}}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'image', name: 'image' },
            { data: 'name', name: 'name' },
            { data: 'is_ship_nationwide', name: 'is_ship_nationwide' },
            { data: 'is_valid', name: 'is_valid' },
            { data: 'ordering', name: 'ordering' },
            { data: 'created_at', name: 'created_at' },
            { data: 'actions', name: 'actions' },
        ],
        initComplete: function () {
            // Button is
            $("table").on("click","#dt-btn-action",function(){
                var action_id = $(this).attr("data-actionId");
                let url = $(this).data("url");
                $.ajax({
                    url: url,
                    method:"POST",
                    success:function(){
                        table.ajax.reload(null, false);
                    }
                })
            });
            //End Button is
        }
    });
    App.reloadTable = function(){
        table.ajax.reload();
    }
});
</script>
@endpush
