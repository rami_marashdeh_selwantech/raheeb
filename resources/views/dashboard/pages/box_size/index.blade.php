@extends('dashboard.layouts.index')
@section('title')
@lang('global.box_sizes')
@stop

@section('content')
<div class="card">
  <div class="card-header">
      <ul class="breadcrumb">
        <li><a href="{{route('dashboard.index')}}">@lang('global.dashboard')</a></li>
        <li><a href="{{route('dashboard.category.index')}}">@lang('global.categories')</a></li>
        <li>@lang('global.box_sizes')</li>
      </ul>
    <div class="heading-elements">
      <a href="{{route('dashboard.category.box_size.create' , $category->id)}}" id="btn-create" class="btn btn-success">
        <i class="ft-plus"></i>
        @lang('global.create')
      </a>
    </div>
  </div>
  <div class="card-content">
    <div class="card-body">
      @if (session('success'))
      <div class="alert alert-success" role="alert">
        {{ session('success') }}
      </div>
      @endif
      <table id="dt-box_size" class="table table-striped table-bordered complex-headers">
        <thead>
          <tr>
            <th>#</th>
            <th>@lang('global.image')</th>
            <th>@lang('global.category_name')</th>
            <th>@lang('global.size')</th>
            <th>@lang('global.unit')</th>
            <th>@lang('global.price')</th>
            <th>@lang('global.created_at')</th>
            <th>@lang('global.acction')</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
              <th>#</th>
              <th>@lang('global.image')</th>
              <th>@lang('global.category_name')</th>
              <th>@lang('global.size')</th>
              <th>@lang('global.unit')</th>
              <th>@lang('global.price')</th>
              <th>@lang('global.created_at')</th>
              <th>@lang('global.acction')</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
@stop

@push("js")
<script>
$(function(){
    var table = $("#dt-box_size").DataTable({
        processing: true,
        serverSide: true,
        responsive: {
            details: true
        },
        ajax: '{{route("dashboard.category.box_size.datatable" , $category->id)}}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'image', name: 'image' },
            { data: 'category_name', name: 'category_name' },
            { data: 'size', name: 'size' },
            { data: 'unit', name: 'unit' },
            { data: 'price', name: 'price' },
            { data: 'created_at', name: 'created_at' },
            { data: 'actions', name: 'actions' },
        ],
        initComplete: function () {
            // Button is
            $("table").on("click","#dt-btn-action",function(){
                var action_id = $(this).attr("data-actionId");
                let url = $(this).data("url");
                $.ajax({
                    url: url,
                    method:"POST",
                    success:function(){
                        table.ajax.reload(null, false);
                    }
                })
            });
            //End Button is
        }
    });
    App.reloadTable = function(){
        table.ajax.reload();
    }
});
</script>
@endpush
