@extends('dashboard.layouts.index')
@section('title')
@lang('global.edit_box_size')
@endsection

@section('content')
<form class="form" id="form" enctype="multipart/form-data" action="{{route("dashboard.category.box_size.update" ,["category" => $category_box_size->category_id , "category_box_size" => $category_box_size->id])}}" novalidate method="POST">
    @csrf
    <div class="card">
        <div class="card-header">
            <ul class="breadcrumb">
              <li><a href="{{route('dashboard.index')}}">@lang('global.dashboard')</a></li>
              <li><a href="{{route('dashboard.category.index')}}">@lang('global.categories')</a></li>
              <li><a href="{{route('dashboard.category.box_size.index' , $category_box_size->category_id)}}">@lang('global.box_sizes')</a></li>
              <li>@lang('global.edit_box_size')</li>
            </ul>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="form-body">
                                <div class="row">



                                    <div class="form-group form-control-sm col-md-6 mb-0">

                                        <div class="form-group form-control-sm col-md-12 mb-0">
                                            <label class="col-md-6"> <strong>@lang('global.category_name') : {{$category_box_size->category->name}} </strong>  </label>
                                        </div>

                                        <div class="form-group form-control-sm col-md-12 mb-0">
                                            <label for="size">@lang('global.size')</label>
                                            <input type="number" id="size" class="form-control form-control-sm @error('size') is-invalid @enderror" placeholder="@lang('global.size')" required value="{{$category_box_size->size}}" name="size">
                                            @error('size')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="form-group form-control-sm col-md-12 mb-0">
                                            <label for="unit">@lang('global.unit')</label>
                                            <input type="text" id="unit" class="form-control form-control-sm @error('unit') is-invalid @enderror" placeholder="@lang('global.unit')" required value="{{$category_box_size->unit}}" name="unit">
                                            @error('unit')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="form-group form-control-sm col-md-12 mb-0">
                                            <label for="price">@lang('global.price')</label>
                                            <input type="number" id="price" class="form-control form-control-sm @error('price') is-invalid @enderror" placeholder="@lang('global.price')" required value="{{$category_box_size->price}}" name="price">
                                            @error('price')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>



                                    </div>

                                    <div class="form-check form-control-sm col-md-6 mb-0">
                                        <div class="update-avatar-container">
                                            <label for="name">@lang('global.image')</label>
                                            <div>
                                                <img  style="width:200px; height: 100px;" id="img-image" src="{{ isset($category_box_size->image) ?$category_box_size->image:asset('images-app/image-not-found.png')}}">
                                                <div id="div_image-model-service" class="modal-create">
                                                    <div class="modal-content-create">
                                                        <div class="modal-body-create">
                                                            <img style="display:block; width:100%; height:100%;object-fit: cover;" id="img-image-model" src="{{ isset($category_box_size->image) ?$category_box_size->image:asset('images-app/image-not-found.png')}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" id="btn-select-avatar" class="btn btn-primary  btn-sm">
                                                <i class="ft-image"></i> @lang('global.select_image')
                                            </button>
                                            <input name="image" type="file" id="file-avatar">
                                        </div>
                                    </div>

                                    <div class="form-group form-control-sm col-md-12 mb-0">
                                        <a href="{{route("dashboard.category.box_size.update" ,["category" => $category_box_size->category_id , "category_box_size" => $category_box_size->id])}}" onclick="event.preventDefault();document.getElementById('form').submit();" class="btn btn-info btn-sm">
                                          <i class="ft-save"></i>
                                          @lang('global.save')
                                        </a>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@push('js')
<script>
  $("#btn-select-avatar").on("click",function(){
        $("#file-avatar").click();
        $("#file-avatar").change(function(event){
          var fullPath =URL.createObjectURL(event.target.files[0])
          $("#img-image").attr("src",fullPath);
          $("#img-image-model").attr("src",fullPath);
        });
      });

    var modal = document.getElementById("div_image-model-service");
     var img = document.getElementById("img-image");

     img.onclick = function() {
      modal.style.display = "block";
     }

     window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none";
        }
      }
</script>
@endpush
