@extends('dashboard.layouts.index')
@section('title')
@lang('global.admins')
@stop

@section('content')

<form class="form" id="form" enctype="multipart/form-data" action="{{route("dashboard.admin.store")}}" novalidate method="POST">
    @csrf
  <div class="card">
    <div class="card-header">
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard.index')}}">@lang('global.dashboard')</a></li>
          <li><a href="{{route('dashboard.admin.index')}}">@lang('global.admins')</a></li>
          <li>@lang('global.create_admin')</li>
        </ul>
      <div class="heading-elements">
        <a href="{{route("dashboard.user.index")}}" class="btn btn-black">
          <i class="ft-arrow-left"></i>
          @lang('global.back')
        </a>
        <a href="{{route("dashboard.admin.store")}}"
          onclick="event.preventDefault();document.getElementById('form').submit();" class="btn btn-info ">
          <i class="ft-save"></i>
          @lang('global.save')
        </a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 col-md-4">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">@lang('global.avatar')</h4>
        </div>
        <div class="card-content">
          <div class="card-body">
            @component('dashboard.components.edit_image')

            @endcomponent
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-md-8">

      <div class="card">
        <div class="card-header">
          <h4 class="card-title">@lang('global.admin_information')</h4>
        </div>
        <div class="card-content">
          <div class="card-body">
            <div class="form-body">
              <div class="row">
                <div class="form-group form-control-sm col-md-6 mb-0">
                  <label for="name">@lang('global.name')</label>
                  <input type="text" id="name" class="form-control form-control-sm @error('name') is-invalid @enderror"
                    placeholder="@lang('global.name')" required value="{{old("name")}}" name="name">
                  @error('name')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
                <div class="form-group form-control-sm col-md-6 mb-0">
                  <label for="email">@lang('global.email')</label>
                  <input type="email" id="email" class="form-control form-control-sm @error('email') is-invalid @enderror"
                    placeholder="@lang('global.email')" required value="{{old("email")}}" name="email">
                  @error('email')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="row">
                <div class="form-group form-control-sm col-md-6 mb-0">
                  <label for="phone_number">@lang('global.phone_number')</label>
                  <input type="number" id="phone_number" class="form-control form-control-sm @error('phone_number') is-invalid @enderror"
                    placeholder="@lang('global.phone_number')" required value="{{old("phone_number")}}"
                    name="phone_number">
                  @error('phone_number')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
                <div class="form-group form-control-sm col-md-6 mb-0">
                  <label for="email">@lang('global.password')</label>
                  <input type="password" id="password" class="form-control form-control-sm @error('password') is-invalid @enderror"
                    placeholder="@lang('global.password')" required autocomplete="new-password" name="password">
                    <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                  @error('password')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
                <div class="form-group form-control-sm col-md-12 mb-0">
                    <input  type="checkbox" onclick="checkboximageID()"  id="is_id_image" value="1" name="is_id_image" checked>
                    <label for="is_id_image">@lang('global.upload_id_image')</label>
                </div>


                <div  id="div_image_id" class="form-check form-control-sm col-md-6 mb-0">
                  <div class="update-avatar">
                    <label for="name">@lang('global.id_image')</label>
                    <div>
                      <img  style="width:200px; height: 100px;" id="img-image-id_image" src="{{asset('images-app/image-not-found.png')}}">
                    </div>
                    <div class="form-group form-control-sm col-md-12 mb-0">
                        <button type="button" id="btn-select-id_image" class="btn btn-primary  btn-sm">
                          <i class="ft-image"></i>
                          @lang('global.select_image')</button>
                        <input style="display:none;" name="id_image" type="file" id="id_image">
                    </div>
                  </div>
                </div>

                <div  id="div_number_id" class="form-check form-control-sm col-md-6 mb-0">
                    <label for="id_number">@lang('global.id_number')</label>
                    <input type="number" id="id_number" class="form-control form-control-sm @error('id_number') is-invalid @enderror"
                      placeholder="@lang('global.id_number')"  value="{{old("id_number")}}"
                      name="id_number">
                </div>

                <div class="form-check form-control-sm col-md-12 mb-0">
                    <div class="radio">
                      <label><input id="super_admin" onclick="checkedRadioButtonSuperAdmin()" type="radio" value="{{\App\user::ROLE_SUPER_ADMIN}}" name="adminrole" checked> @lang("global.manegare")</label>
                    </div>
                    <div class="radio">
                      <label><input id="sub_admin" onclick="checkedRadioButtonSubAdmin()" value="{{\App\user::ROLE_ADMIN}}" type="radio" name="adminrole"> @lang("global.sub_manegare")</label>
                    </div>
                </div>

                <div class="card" style="display : none;" id="permissions">
                    <div class="card-header">
                      <h4 class="card-title">@lang('global.permission')</h4>
                    </div>
                    <div class="row">
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"  id="m_user_view" value="1" name="m_user_view">
                            <label for="m_user_view">@lang('global.m_user_view')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"   id="m_user_create" value="1" name="m_user_create">
                            <label for="m_user_create">@lang('global.m_user_create')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_user_edit" value="1" name="m_user_edit" >
                            <label for="m_user_edit">@lang('global.m_user_edit')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_user_blocked" value="1" name="m_user_blocked">
                            <label for="m_user_blocked">@lang('global.m_user_blocked')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_categories_view" value="1" name="m_categories_view">
                            <label for="m_categories_view">@lang('global.m_categories_view')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_categories_create" value="1" name="m_categories_create">
                            <label for="m_categories_create">@lang('global.m_categories_create')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_categories_edit" value="1" name="m_categories_edit">
                            <label for="m_categories_edit">@lang('global.m_categories_edit')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_category_box_size_view" value="1" name="m_category_box_size_view">
                            <label for="m_category_box_size_view">@lang('global.m_category_box_size_view')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_category_box_size_create" value="1" name="m_category_box_size_create">
                            <label for="m_category_box_size_create">@lang('global.m_category_box_size_create')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_category_box_size_edit" value="1" name="m_category_box_size_edit">
                            <label for="m_category_box_size_edit">@lang('global.m_category_box_size_edit')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_conditions_view" value="1" name="m_conditions_view">
                            <label for="m_conditions_view">@lang('global.m_conditions_view')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_conditions_create" value="1" name="m_conditions_create">
                            <label for="m_conditions_create">@lang('global.m_conditions_create')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_conditions_edit" value="1" name="m_conditions_edit">
                            <label for="m_conditions_edit">@lang('global.m_conditions_edit')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_notifications_view" value="1" name="m_notifications_view">
                            <label for="m_notifications_view">@lang('global.m_notifications_view')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_notifications_create" value="1" name="m_notifications_create">
                            <label for="m_notifications_create">@lang('global.m_notifications_create')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_post_view" value="1" name="m_post_view">
                            <label for="m_post_view">@lang('global.m_post_view')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_post_solds_view" value="1" name="m_post_solds_view">
                            <label for="m_post_solds_view">@lang('global.m_post_solds_view')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_post_report_view" value="1" name="m_post_report_view">
                            <label for="m_post_report_view">@lang('global.m_post_report_view')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_ticket_view" value="1" name="m_ticket_view">
                            <label for="m_ticket_view">@lang('global.m_ticket_view')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_settings_view" value="1" name="m_settings_view">
                            <label for="m_settings_view">@lang('global.m_settings_view')</label>
                        </div>
                        <div class="form-check form-control-sm col-md-4 mb-0">
                            <input  type="checkbox"    id="m_settings_edit" value="1" name="m_settings_edit">
                            <label for="m_settings_edit">@lang('global.m_settings_edit')</label>
                        </div>
                    </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>


@stop

@push('js')
<script>
  $("#btn-select-id_image").on("click",function(){
        $("#id_image").click();
        $("#id_image").change(function(event){
          var fullPath =URL.createObjectURL(event.target.files[0])
          $("#img-image-id_image").attr("src",fullPath);
        });
      });

function checkboximageID(){
    var div_image_id = document.getElementById('div_image_id');
    var div_number_id = document.getElementById('div_number_id');
    var is_id_image = document.getElementById('is_id_image');
    if (is_id_image.checked) {
      div_image_id.style.display = "block";
      div_number_id.style.display = "block";
    }else{
        div_image_id.style.display = "none";
        div_number_id.style.display = "none";
    }
  }

  function checkedRadioButtonSubAdmin(){
      var permissions = document.getElementById('permissions');
      permissions.style.display = "block";
  }

  function checkedRadioButtonSuperAdmin(){
      var permissions = document.getElementById('permissions');
      permissions.style.display = "none";
  }

</script>

@endpush
