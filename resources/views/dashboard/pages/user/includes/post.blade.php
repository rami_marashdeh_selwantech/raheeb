<div class="card-content">
    <div class="card-body">
        <table id="dt-posts" class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>@lang('global.user')</th>
                    <th>@lang('global.title')</th>
                    <th>@lang('global.category')</th>
                    <th>@lang('global.status')</th>
                    <th>@lang('global.created_at')</th>
                    <th>@lang('global.actions')</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>@lang('global.user')</th>
                    <th>@lang('global.title')</th>
                    <th>@lang('global.category')</th>
                    <th>@lang('global.status')</th>
                    <th>@lang('global.created_at')</th>
                    <th>@lang('global.actions')</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>




@push('js')
<script>
$(function(){

    var table = $("#dt-posts").DataTable({
        serverSide: true,
        processing: true,
        autoWidth:false,
        ajax: '{{route("dashboard.post.user_datatable" , $user->id )}}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'user', name: 'user' },
            { data: 'title', name: 'title' },
            { data: 'category', name: 'category' },
            { data: 'status', name: 'status' },
            { data: 'created_at', name: 'created_at' },
            { data: 'actions', name: 'actions' },

        ],
        initComplete: function () {

        }

    });
});
</script>
@endpush
