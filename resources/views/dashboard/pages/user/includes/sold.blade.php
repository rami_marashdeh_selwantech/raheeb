<div class="card-content">
    <div class="card-body">
        <table id="dt-sold" class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>@lang('global.user')</th>
                    <th>@lang('global.title')</th>
                    <th>@lang('global.category')</th>
                    <th>@lang('global.total_price')</th>
                    <th>@lang('global.created_at')</th>
                    <th>@lang('global.actions')</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>@lang('global.user')</th>
                    <th>@lang('global.title')</th>
                    <th>@lang('global.category')</th>
                    <th>@lang('global.total_price')</th>
                    <th>@lang('global.created_at')</th>
                    <th>@lang('global.actions')</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>


@push('js')
<script>
$(function(){

    var table = $("#dt-sold").DataTable({
        serverSide: true,
        processing: true,
        autoWidth:false,
        ajax: '{{route("dashboard.user.user_post_sold_datatable" , $user->id )}}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'user', name: 'user' },
            { data: 'title', name: 'title' },
            { data: 'category', name: 'category' },
            { data: 'total_price', name: 'total_price' },
            { data: 'created_at', name: 'created_at' },
            { data: 'actions', name: 'actions' },

        ],
        initComplete: function () {

        }

    });
});
</script>
@endpush
