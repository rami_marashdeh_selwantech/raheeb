<section id="cardAnimation" class="cardAnimation">
    <div class="card">
        <div class="row">
            <div class="form-group col-md-6 p-2">
                <div class="card-header white bg-warning">
                    <h3 class="card-title white"> <strong>@lang('global.user_information')</strong></h3>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body border-bottom-warning">
                        <label class="col-md-6"> <strong>@lang('global.user') : </strong>  </label> {{$user->name}}
                        <label class="col-md-6"> <strong>@lang('global.email') : </strong>  </label> {{$user->email ? $user->email : "NULL"}}
                        <label class="col-md-6"> <strong>@lang('global.phone_number') : </strong>  </label> {{$user->phone_number}}
                        <label class="col-md-6"> <strong>@lang('global.followers') : </strong>  </label> {{$user->countFollowers($user->id)}}
                        <label class="col-md-6"> <strong>@lang('global.following') : </strong>  </label> {{$user->countFollowing($user->id)}}
                        <label class="col-md-6"> <strong>@lang('global.balance') : </strong>  </label> {{$user->balance}}
                        <label class="col-md-6"> <strong>@lang('global.login_with') : </strong>  </label> {{$user->login_with}}
                        <label class="col-md-6"> <strong>@lang('global.id_number') : </strong>  </label> {{$user->id_number ? $user->id_number : "NULL"}}
                        <label class="col-md-6"> <strong>@lang('global.is_valid') : </strong>  </label> @if($user->is_valid == 1) @lang('global.true') @else @lang('global.false') @endif
                        <label class="col-md-6"> <strong>@lang('global.rate') : </strong>  </label> {{$user->getRate($user->id)}}
                        <label class="col-md-6"> <strong>@lang('global.create_date') : </strong>  </label> {{$user->created_at->diffForHumans()}}
                        <label class="col-md-6"> <strong>@lang('global.update_date') : </strong>  </label> {{$user->updated_at->diffForHumans()}}
                    </div>
                </div>
            </div>

            <div class="from-group col-md-6 p-2">
                <div class="card-header white bg-warning">
                    <h3 class="card-title white"> <strong>@lang('global.user_image')</strong></h3>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body border-bottom-warning">
                        <img style="width: 100%; height: 325px;" src="{{$user->avatar == null ? asset('images-app/image-not-found.png') : $user->avatar}}">
                    </div>
                </div>
            </div>

            <div class="from-group col-md-6 p-2">
                <div class="card-header white bg-warning">
                    <h3 class="card-title white"> <strong>@lang('global.id_image')</strong></h3>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body border-bottom-warning">
                        <img style="width: 100%; height: 325px;" src="{{$user->id_image == null ? asset('images-app/image-not-found.png') : $user->id_image}}">
                    </div>
                </div>
            </div>

            <div class="from-group col-md-6 p-2">
                <div class="card-header white bg-warning">
                    <h3 class="card-title white"> <strong>@lang('global.cover_image')</strong></h3>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body border-bottom-warning">
                        <img style="width: 100%; height: 325px;" src="{{$user->cover_image == null ? asset('images-app/image-not-found.png') : $user->cover_image}}">
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
