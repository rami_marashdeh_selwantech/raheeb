<div class="card">
    <div class="row">
        <div class="form-group col-md-6 p-2">
            <div class="card-header white bg-warning">
                <h3 class="card-title white"> <strong>@lang('global.user_information')</strong></h3>
            </div>
            <div class="card-content collapse show">
                <div class="card-body border-bottom-warning">
                    <h3 class="card-title"> <strong>@lang('global.user_report_information')</strong></h3>
                    <label class="col-md-6"> <strong>@lang('global.user') : </strong>  </label> {{$user_report->user->name}}
                    <label class="col-md-6"> <strong>@lang('global.email') : </strong>  </label> {{$user_report->user->email ? $user_report->user->email : "NULL"}}
                    <label class="col-md-6"> <strong>@lang('global.phone_number') : </strong>  </label> {{$user_report->user->phone_number}}
                    <label class="col-md-6"> <strong>@lang('global.followers') : </strong>  </label> {{$user_report->user->countFollowers($user_report->user->id)}}
                    <label class="col-md-6"> <strong>@lang('global.following') : </strong>  </label> {{$user_report->user->countFollowing($user_report->user->id)}}
                    <label class="col-md-6"> <strong>@lang('global.balance') : </strong>  </label> {{$user_report->user->balance}}
                    <label class="col-md-6"> <strong>@lang('global.login_with') : </strong>  </label> {{$user_report->user->login_with}}
                    <label class="col-md-6"> <strong>@lang('global.id_number') : </strong>  </label> {{$user_report->user->id_number ? $user_report->user->id_number : "NULL"}}
                    <label class="col-md-6"> <strong>@lang('global.is_valid') : </strong>  </label> @if($user_report->user->is_valid == 1) @lang('global.true') @else @lang('global.false') @endif
                    <label class="col-md-6"> <strong>@lang('global.rate') : </strong>  </label> {{$user_report->user->getRate($user_report->user->id)}}
                    <label class="col-md-6"> <strong>@lang('global.create_date') : </strong>  </label> {{$user_report->user->created_at->diffForHumans()}}
                    <label class="col-md-6"> <strong>@lang('global.update_date') : </strong>  </label> {{$user_report->user->updated_at->diffForHumans()}}
                </div>
            </div>
        </div>

        <div class="from-group col-md-6 p-2">
            <div class="card-header white bg-warning">
                <h3 class="card-title white"> <strong>@lang('global.message')</strong></h3>
            </div>
            <div class="card-content collapse show">
                <div class="card-body border-bottom-warning">
                    <label class="col-md-6"> {{$user_report->message}}  </label>
                </div>
            </div>
        </div>

        <div class="from-group col-md-6 p-2">
            <div class="card-header white bg-warning">
                <h3 class="card-title white"> <strong>@lang('global.report_image')</strong></h3>
            </div>
            <div class="card-content collapse show">
                <div class="card-body border-bottom-warning">
                    <img style="width: 100%; height: 320px;" src="{{$user_report->image == null ? asset('images-app/image-not-found.png') : $user_report->image}}">
                </div>
            </div>
        </div>

    </div>
</div>
