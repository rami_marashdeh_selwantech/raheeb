<div class="card-content">
    <div class="card-body">
        <table id="dt-offer"  class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>@lang('global.title')</th>
                    <th>@lang('global.category')</th>
                    <th>@lang('global.price_offer')</th>
                    <th>@lang('global.price_post')</th>
                    <th>@lang('global.status')</th>
                    <th>@lang('global.created_at')</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>@lang('global.title')</th>
                    <th>@lang('global.category')</th>
                    <th>@lang('global.price_offer')</th>
                    <th>@lang('global.price_post')</th>
                    <th>@lang('global.status')</th>
                    <th>@lang('global.created_at')</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

@push('js')
<script>
$(function(){

    var table = $("#dt-offer").DataTable({
        serverSide: true,
        processing: true,
        autoWidth:false,
        ajax: '{{route("dashboard.user.user_offer_datatable" , $user->id )}}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'title', name: 'title' },
            { data: 'category', name: 'category' },
            { data: 'price_offer', name: 'price_offer' },
            { data: 'price_post', name: 'price_post' },
            { data: 'status', name: 'status' },
            { data: 'created_at', name: 'created_at' },

        ],
        initComplete: function () {

        }

    });
});
</script>
@endpush
