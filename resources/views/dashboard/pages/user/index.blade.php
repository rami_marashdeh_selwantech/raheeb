@extends('dashboard.layouts.index')
@section('title')
@lang('global.users')
@stop

@section('content')

<div class="card">
  <div class="card-header">
    <h4 class="card-title">@lang('global.users')</h4>
    <div class="heading-elements">
      <a href="{{route('dashboard.user.create')}}" id="btn-create" class="btn btn-success">
        <i class="ft-plus"></i>
        @lang('global.create')
      </a>
    </div>
  </div>
  <div class="card-content">
    <div class="card-body">
      @if (session('success'))
      <div class="alert alert-success" role="alert">
        {{ session('success') }}
      </div>
      @endif
      <table id="dt-users" class="table table-striped table-bordered complex-headers">
        <thead>
          <tr>
            <th>#</th>
            <th>@lang('global.avatar')</th>
            <th>@lang('global.name')</th>
            <th>@lang('global.phone_number')</th>
            <th>@lang('global.balance')</th>
            <th>@lang('global.is_blocked')</th>
            <th>@lang('global.created_at')</th>
            <th>@lang('global.acction')</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>#</th>
            <th>@lang('global.avatar')</th>
            <th>@lang('global.name')</th>
            <th>@lang('global.phone_number')</th>
            <th>@lang('global.balance')</th>
            <th>@lang('global.is_blocked')</th>
            <th>@lang('global.created_at')</th>
            <th>@lang('global.acction')</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>

@stop

@push('js')

<script>
$(function(){
    var table = $("#dt-users").DataTable({
        processing: true,
        serverSide: true,
        responsive: {
            details: true
        },
        ajax: '{{route("dashboard.user.datatable")}}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'avatar', name: 'avatar' },
            { data: 'name', name: 'name' },
            { data: 'phone_number', name: 'phone_number' },
            { data: 'balance', name: 'balance' },
            { data: 'is_blocked', name: 'is_blocked' },
            { data: 'created_at', name: 'created_at' },
            { data: 'actions', name: 'actions' },
        ],
        initComplete: function () {
            // Button bloacked
            $("table").on("click","#dt-btn-action",function(){
                var action_id = $(this).attr("data-actionId");
                let url = $(this).data("url");
                $.ajax({
                    url: url,
                    method:"POST",
                    success:function(){
                        table.ajax.reload(null, false);
                    }
                })
            });
            //End Button bloacked
        }
    });
});

</script>

@endpush
