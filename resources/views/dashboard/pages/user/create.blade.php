@extends('dashboard.layouts.index')
@section('title')
@lang('global.users')
@stop

@section('content')

<form class="form" id="form" enctype="multipart/form-data" action="{{route("dashboard.user.store")}}" novalidate method="POST">
    @csrf
  <div class="card">
    <div class="card-header">
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard.index')}}">@lang('global.dashboard')</a></li>
          <li><a href="{{route('dashboard.user.index')}}">@lang('global.users')</a></li>
          <li>@lang('global.create_user')</li>
        </ul>
      <div class="heading-elements">
        <a href="{{route("dashboard.user.index")}}" class="btn btn-black">
          <i class="ft-arrow-left"></i>
          @lang('global.back')
        </a>
        <a href="{{route("dashboard.user.store")}}"
          onclick="event.preventDefault();document.getElementById('form').submit();" class="btn btn-info ">
          <i class="ft-save"></i>
          @lang('global.save')
        </a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 col-md-4">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">@lang('global.avatar')</h4>
        </div>
        <div class="card-content">
          <div class="card-body">
            @component('dashboard.components.edit_image')

            @endcomponent
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-md-8">

      <div class="card">
        <div class="card-header">
          <h4 class="card-title">@lang('global.user_information')</h4>
        </div>
        <div class="card-content">
          <div class="card-body">
            <div class="form-body">
              <div class="row">
                <div class="form-group form-control-sm col-md-6 mb-0">
                  <label for="name">@lang('global.name')</label>
                  <input type="text" id="name" class="form-control form-control-sm @error('name') is-invalid @enderror"
                    placeholder="@lang('global.name')" required value="{{old("name")}}" name="name">
                  @error('name')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
                <div class="form-group form-control-sm col-md-6 mb-0">
                  <label for="email">@lang('global.email')</label>
                  <input type="email" id="email" class="form-control form-control-sm @error('email') is-invalid @enderror"
                    placeholder="@lang('global.email')" required value="{{old("email")}}" name="email">
                  @error('email')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="row">
                <div class="form-group form-control-sm col-md-6 mb-0">
                  <label for="phone_number">@lang('global.phone_number')</label>
                  <input type="number" id="phone_number" class="form-control form-control-sm @error('phone_number') is-invalid @enderror"
                    placeholder="@lang('global.phone_number')" required value="{{old("phone_number")}}"
                    name="phone_number">
                  @error('phone_number')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
                <div class="form-group form-control-sm col-md-6 mb-0">
                  <label for="email">@lang('global.password')</label>
                  <input type="password" id="password" class="form-control form-control-sm @error('password') is-invalid @enderror"
                    placeholder="@lang('global.password')" required autocomplete="new-password" name="password">
                    <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                  @error('password')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
                <div class="form-group form-control-sm col-md-12 mb-0">
                    <input  type="checkbox" onclick="checkboximageID()"  id="is_id_image" value="1" name="is_id_image" checked>
                    <label for="is_id_image">@lang('global.upload_id_image')</label>
                </div>


                <div  id="div_image_id" class="form-check form-control-sm col-md-6 mb-0">
                  <div class="update-avatar">
                    <label for="name">@lang('global.id_image')</label>
                    <div>
                      <img  style="width:200px; height: 100px;" id="img-image-id_image" src="{{asset('images-app/image-not-found.png')}}">
                    </div>
                    <div class="form-group form-control-sm col-md-12 mb-0">
                        <button type="button" id="btn-select-id_image" class="btn btn-primary  btn-sm">
                          <i class="ft-image"></i>
                          @lang('global.select_image')</button>
                        <input style="display:none;" name="id_image" type="file" id="id_image">
                    </div>
                  </div>
                </div>

                <div  id="div_number_id" class="form-check form-control-sm col-md-6 mb-0">
                    <label for="id_number">@lang('global.id_number')</label>
                    <input type="number" id="id_number" class="form-control form-control-sm @error('id_number') is-invalid @enderror"
                      placeholder="@lang('global.id_number')"  value="{{old("id_number")}}"
                      name="id_number">
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>


@stop

@push('js')
<script>
  $("#btn-select-id_image").on("click",function(){
        $("#id_image").click();
        $("#id_image").change(function(event){
          var fullPath =URL.createObjectURL(event.target.files[0])
          $("#img-image-id_image").attr("src",fullPath);
        });
      });


function checkboximageID(){
    var div_image_id = document.getElementById('div_image_id');
    var div_number_id = document.getElementById('div_number_id');
    var is_id_image = document.getElementById('is_id_image');
    if (is_id_image.checked) {
      div_image_id.style.display = "block";
      div_number_id.style.display = "block";
    }else{
        div_image_id.style.display = "none";
        div_number_id.style.display = "none";
    }
  }

</script>

@endpush
