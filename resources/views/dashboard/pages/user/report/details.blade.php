@extends('dashboard.layouts.index')
@section('title')
@lang('global.user_details')
@stop

@section('content')



<div class="card">
    <div class="card-header">
        <ul class="breadcrumb">
            <li><a href="{{route('dashboard.index')}}">@lang('global.dashboard')</a></li>
            <li><a href="{{route('dashboard.user_report.index')}}">@lang('global.users_report')</a></li>
            <li>@lang('global.user_details')</li>
        </ul>
        <div class="heading-elements">
            <a href="{{route("dashboard.user.index")}}" class="btn btn-black">
                <i class="ft-arrow-left"></i>
                @lang('global.back')
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="form-body">
                        <ul class="nav nav-tabs nav-linetriangle no-hover-bg">
                            <li class="nav-item">
                                <a class="nav-link active" id="setting-tab-all-tab" data-toggle="pill" href="#setting-tab-all" role="tab" aria-controls="setting-tab-general-all">@lang('global.home')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="setting-tab-user-tab" data-toggle="pill" href="#setting-tab-user" role="tab" aria-controls="setting-tab-general-user">@lang('global.user')</a>
                            </li>
                           <li class="nav-item">
                               <a class="nav-link" id="setting-tab-posts-tab" data-toggle="pill" href="#setting-tab-posts" role="tab" aria-controls="setting-tab-general-posts">@lang('global.posts')</a>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link" id="setting-tab-faverate-tab" data-toggle="pill" href="#setting-tab-faverate" role="tab" aria-controls="setting-tab-general-faverate">@lang('global.post_faverates')</a>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link" id="setting-tab-sold-tab" data-toggle="pill" href="#setting-tab-sold" role="tab" aria-controls="setting-tab-general-sold">@lang('global.user_post_sold')</a>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link" id="setting-tab-offer-tab" data-toggle="pill" href="#setting-tab-offer" role="tab" aria-controls="setting-tab-general-offer">@lang('global.user_offer')</a>
                           </li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="setting-tab-all" role="tabpanel" aria-labelledby="setting-tab-all-tab">
                					<br>
                					<br>
                                  @include("dashboard.pages.user.includes.home_report")
                				</div>

                                <div class="tab-pane fade" id="setting-tab-user" role="tabpanel" aria-labelledby="setting-tab-user-tab">
                                  <br>
                                  <br>
                                  @include("dashboard.pages.user.includes.home")
                                </div>


                                  <div class="tab-pane fade" id="setting-tab-posts" role="tabpanel" aria-labelledby="setting-tab-posts-tab">
                                    <br>
                                    <br>
                                    @include("dashboard.pages.user.includes.post")
                                  </div>

                    				<div class="tab-pane fade" id="setting-tab-faverate" role="tabpanel" aria-labelledby="setting-tab-faverate-tab">
                                      <br>
                                      <br>
                                      @include("dashboard.pages.user.includes.faverate")
                    				</div>


                                <div class="tab-pane fade" id="setting-tab-sold" role="tabpanel" aria-labelledby="setting-tab-sold-tab">
                                    <br>
                                    <br>
                                    @include("dashboard.pages.user.includes.sold")
                                </div>


                                <div class="tab-pane fade" id="setting-tab-offer" role="tabpanel" aria-labelledby="setting-tab-offer-tab">
                                  <br>
                                  <br>
                                   @include("dashboard.pages.user.includes.offer")
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@push('js')
<script>

</script>



@endpush
