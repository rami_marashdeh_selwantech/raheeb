@extends('dashboard.layouts.index')
@section('title')
@lang('global.users_report')
@stop

@section('content')

<div class="card">
  <div class="card-header">
    <h4 class="card-title">@lang('global.users_report')</h4>
  </div>
  <div class="card-content">
    <div class="card-body">
      @if (session('success'))
      <div class="alert alert-success" role="alert">
        {{ session('success') }}
      </div>
      @endif
      <table id="dt-user_report" class="table table-striped table-bordered complex-headers">
        <thead>
          <tr>
              <th>#</th>
              <th>@lang('global.user_report_to')</th>
              <th>@lang('global.user_report_from')</th>
              <th>@lang('global.message')</th>
              <th>@lang('global.image')</th>
              <th>@lang('global.created_at')</th>
              <th>@lang('global.actions')</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
              <th>#</th>
              <th>@lang('global.user_report_to')</th>
              <th>@lang('global.user_report_from')</th>
              <th>@lang('global.message')</th>
              <th>@lang('global.image')</th>
              <th>@lang('global.created_at')</th>
              <th>@lang('global.actions')</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>

@stop

@push('js')

<script>
$(function(){
    var table = $("#dt-user_report").DataTable({
        processing: true,
        serverSide: true,
        responsive: {
            details: true
        },
        ajax: '{{route("dashboard.user_report.datatable")}}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'user_report_to', name: 'user_report_to' },
            { data: 'user_report_from', name: 'user_report_from' },
            { data: 'message', name: 'message' },
            { data: 'image', name: 'image' },
            { data: 'created_at', name: 'created_at' },
            { data: 'actions', name: 'actions' },
        ],
        initComplete: function () {
            // Button bloacked
            $("table").on("click","#dt-btn-action",function(){
                var action_id = $(this).attr("data-actionId");
                let url = $(this).data("url");
                $.ajax({
                    url: url,
                    method:"POST",
                    success:function(){
                        table.ajax.reload(null, false);
                    }
                })
            });
            //End Button bloacked
        }
    });
});

</script>

@endpush
