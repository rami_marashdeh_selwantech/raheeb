<div id="div_create-model-condition" class="modal-create">

     <div class="modal-content-create">
       <div class="modal-header-create">
         <span class="model-font-header">@lang('global.condition')</span>
       </div>
       <div class="modal-body-create">
         <form class="storForm-condition" id="storForm-condition" enctype="multipart/form-data" action="#"
           novalidate method="POST">
           @csrf



           <div class="row">
               <div class="form-check form-control-sm col-md-8 mb-0">
                 <label for="title">@lang('global.name')</label>
                 <input type="text" id="name" class="form-control form-control-sm @error('name') is-invalid @enderror"
                   placeholder="@lang('global.name')" required  name="name">
               </div>
               <div class="form-check form-control-sm col-md-8 mb-0">
                 <label for="message">@lang('global.message')</label>
                 <input type="number" id="ordering" class="form-control form-control-sm @error('ordering') is-invalid @enderror"
                   placeholder="@lang('global.ordering')" required  name="ordering">
               </div>

           </div>

           <div class="row">
             <div class="form-check form-control-sm col-md-12 mb-0">
               <button type="submit" id="submit" class="btn btn-success">@lang('global.add')</button>
             </div>
           </div>

         </form>
       </div>
       <div class="modal-footer-create">
         <span class="model-font-header">@lang('global.condition')</span>
       </div>
     </div>

   </div>



@push('js')
<script>

</script>
@endpush
