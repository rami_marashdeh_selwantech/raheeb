@extends('dashboard.layouts.index')
@section('title')
@lang('global.conditions')
@stop

@section('content')
<div class="card">
  <div class="card-header">
    <h4 class="card-title">@lang('global.conditions')</h4>
    <div class="heading-elements">
      <a href="#" id="create-model-condition" class="btn btn-success">
        <i class="ft-plus"></i>
        @lang('global.create')
      </a>
    </div>
    @include('dashboard.pages.condition.create')
  </div>
  <div class="card-content">
    <div class="card-body">
      @if (session('success'))
      <div class="alert alert-success" role="alert">
        {{ session('success') }}
      </div>
      @endif
      <table id="dt-condition" class="table table-striped table-bordered complex-headers">
        <thead>
          <tr>
            <th>#</th>
            <th>@lang('global.name')</th>
            <th>@lang('global.is_show')</th>
            <th>@lang('global.ordering')</th>
            <th>@lang('global.created_at')</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
              <th>#</th>
              <th>@lang('global.name')</th>
              <th>@lang('global.is_show')</th>
              <th>@lang('global.ordering')</th>
              <th>@lang('global.created_at')</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
@stop

@push("js")
<script>
$(function(){


    var table = $("#dt-condition").DataTable({
        processing: true,
        serverSide: true,
        responsive: {
            details: true
        },
        ajax: '{{route("dashboard.condition.datatable")}}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'is_show', name: 'is_show' },
            { data: 'ordering', name: 'ordering' },
            { data: 'created_at', name: 'created_at' },
        ],
        initComplete: function () {
            // Button is
            $("table").on("click","#dt-btn-action",function(){
                var action_id = $(this).attr("data-actionId");
                let url = $(this).data("url");
                $.ajax({
                    url: url,
                    method:"POST",
                    success:function(){
                        table.ajax.reload(null, false);
                    }
                })
            });
            //End Button is
        }
    });
    App.reloadTable = function(){
        table.ajax.reload();
    }
    var modal = document.getElementById("div_create-model-condition");
    var btn = document.getElementById("create-model-condition");
    btn.onclick = function() {
        modal.style.display = "block";
    }
    window.onclick = function(event) {
        if (event.target == modal){
            modal.style.display = "none";
        }
    }
    $(document).ready(function (e){
        $("#storForm-condition").on('submit',(function(e){
            e.preventDefault();
             $.ajax({
                  url: "{{route('dashboard.condition.store')}}",
                  type: "POST",
                  data: new FormData(this),
                  contentType: false,
                  cache: false,
                  processData:false,
                  success: function(data){
                      alert(data.message);
                      modal.style.display = "none";
                      table.ajax.reload(null, false);
                  },
                  error: function(){
                      alert("The Error From Data Send Check Input And Send Agean ... ");
                  }
             });
        }));
    });
});

</script>
@endpush
