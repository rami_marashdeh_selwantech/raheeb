@extends('dashboard.layouts.index')
@section('title')
@lang('global.settings')
@stop

@section('content')
<div class="card">
  <div class="card-header">
    <h4 class="card-title">@lang('global.settings')</h4>
    <div class="heading-elements">

    </div>

    @if (session('success'))
    <br>
    <div class="alert alert-success" role="alert">
      {{ session('success') }}
    </div>
    @endif

  </div>

</div>

<div class="row">


  <div class="col-md-3">
    <div class="card">
      <div class="card-body">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

          <a class="nav-link active" id="setting-tab-general-tab" data-toggle="pill" href="#setting-tab-general"
            role="tab" aria-controls="setting-tab-general">@lang('global.general')</a>

          <a class="nav-link " id="setting-tab-privacypolicy-tab" data-toggle="pill" href="#setting-tab-privacypolicy"
            role="tab" aria-controls="setting-tab-privacypolicy">@lang('global.privacy_policy')</a>

          <a class="nav-link " id="setting-tab-aboutus-tab" data-toggle="pill" href="#setting-tab-aboutus" role="tab"
            aria-controls="setting-tab-aboutus">@lang('global.about_us')</a>

          <a class="nav-link " id="setting-tab-terms_and_condations-tab" data-toggle="pill" href="#setting-tab-terms_and_condations"
            role="tab" aria-controls="setting-tab-terms_and_condations">@lang('global.terms_and_condations')</a>

          <a class="nav-link " id="setting-tab-emailcontact-tab" data-toggle="pill" href="#setting-tab-emailcontact"
            role="tab" aria-controls="setting-tab-emailcontact">@lang('global.email_contact')</a>

          <a class="nav-link " id="setting-tab-phonecontact-tab" data-toggle="pill" href="#setting-tab-phonecontact"
            role="tab" aria-controls="setting-tab-phonecontact">@lang('global.phone_contact')</a>

          <a class="nav-link " id="setting-tab-tax_rate-tab" data-toggle="pill" href="#setting-tab-tax_rate"
            role="tab" aria-controls="setting-tab-tax_rate">@lang('global.tax_rate')</a>

            <a class="nav-link " id="setting-tab-comuntion-tab" data-toggle="pill" href="#setting-tab-comuntion"
              role="tab" aria-controls="setting-tab-comuntion">@lang('global.comuntion')</a>

              <a class="nav-link " id="setting-tab-user_line_time-tab" data-toggle="pill" href="#setting-tab-user_line_time"
                role="tab" aria-controls="setting-tab-user_line_time">@lang('global.user_line_time')</a>

                <a class="nav-link " id="setting-tab-pickup-tab" data-toggle="pill" href="#setting-tab-pickup"
                  role="tab" aria-controls="setting-tab-pickup">@lang('global.pickup')</a>

                  <a class="nav-link " id="setting-tab-unit_distance-tab" data-toggle="pill" href="#setting-tab-unit_distance"
                    role="tab" aria-controls="setting-tab-tax_rate">@lang('global.unit_distance')</a>

          <a class="nav-link " id="setting-tab-default_timezone-tab" data-toggle="pill" href="#setting-tab-default_timezone"
            role="tab" aria-controls="setting-tab-default_timezone">@lang('global.default_timezone')</a>

        </div>
      </div>
    </div>
  </div>


  <div class="col-md-9">
    <div class="card">
      <div class="card-body">
        <div class="tab-content" id="v-pills-tabContent">

          <!--Start Tab General-->
          <div class="tab-pane fade show active" id="setting-tab-general" role="tabpanel"
            aria-labelledby="setting-tab-general-tab">
            <div class="form-group">
              <form method="POST" action="{{route('dashboard.settings.update')}}">
                @csrf

                <div class="form-group">
                  <label>@lang('global.country_iso')</label>
                  <input class="form-control" name="settings[country_iso]" value="{{setting('country_iso')}}" />
                </div>
                <div class="form-group">
                  <label>@lang('global.default_currency')</label>
                  <input class="form-control" name="settings[default_currency]"
                    value="{{setting('default_currency')}}" />
                </div>
                <div class="form-group">
                  <label>@lang('global.country_name')</label>
                  <input class="form-control" name="settings[country_name]" value="{{setting('country_name')}}" />
                </div>

                <div class="form-group">
                  <label>@lang('global.primary_country_name')</label>
                  <input class="form-control" name="settings[primary_country_name]" value="{{setting('primary_country_name')}}" />
                </div>

                <div class="form-group">
                  <button class="btn btn-primary">@lang('global.save')</button>
                </div>
              </form>
            </div>
          </div>
          <!-- End Tab General-->

          <!-- Start Tab Privacy policy -->
          <div class="tab-pane fade " id="setting-tab-privacypolicy" role="tabpanel"
            aria-labelledby="setting-tab-privacypolicy-tab">
            <div class="form-group">
              <form method="POST" action="{{route('dashboard.settings.update')}}">
                @csrf
                <div class="form-group">
                  <textarea class="form-control"
                    name="settings[translatable][privacy_policy]">{{setting("privacy_policy")}}</textarea>
                </div>
                <div class="form-group">
                  <button class="btn btn-primary">@lang('global.save')</button>
                </div>
              </form>
            </div>
          </div>
          <!--End Tab Privacy policy-->

          <!-- Start Tab About Us -->
          <div class="tab-pane fade " id="setting-tab-aboutus" role="tabpanel"
            aria-labelledby="setting-tab-aboutus-tab">
            <div class="form-group">
              <form method="POST" action="{{route('dashboard.settings.update')}}">
                @csrf
                <div class="form-group">
                  <textarea class="form-control"
                    name="settings[translatable][about_app]">{{setting("about_app")}}</textarea>
                </div>
                <div class="form-group">
                  <button class="btn btn-primary">@lang('global.save')</button>
                </div>
              </form>
            </div>
          </div>
          <!--End Tab About Us-->


          <!-- Start Terms And Condations -->
          <div class="tab-pane fade " id="setting-tab-terms_and_condations" role="tabpanel"
            aria-labelledby="setting-tab-terms_and_condations-tab">
            <div class="form-group">
              <form method="POST" action="{{route('dashboard.settings.update')}}">
                @csrf
                <div class="form-group">
                  <textarea class="form-control"
                    name="settings[translatable][terms_and_condations]">{{setting("terms_and_condations")}}</textarea>
                </div>
                <div class="form-group">
                  <button class="btn btn-primary">@lang('global.save')</button>
                </div>
              </form>
            </div>
          </div>
          <!-- End Terms And Condations -->


          <!-- Start Tab Email Contact -->
          <div class="tab-pane fade " id="setting-tab-emailcontact" role="tabpanel"
            aria-labelledby="setting-tab-emailcontact-tab">
            <div class="form-group">
              <form method="POST" action="{{route('dashboard.settings.update')}}">
                @csrf
                @foreach (setting("email_contact") as $email)
                <div class="form-group">
                  <input type="email" class="form-control" name="settings[email_contact][]" value="{{$email}}">
                </div>
                @endforeach

                <div class="form-group">
                  <button class="btn btn-primary">@lang('global.save')</button>
                </div>
              </form>
            </div>
          </div>
          <!--End Tab Email Contact-->

          <!-- Start Tab Phone Contact  -->
          <div class="tab-pane fade " id="setting-tab-phonecontact" role="tabpanel"
            aria-labelledby="setting-tab-phonecontact-tab">
            <div class="form-group">

              <form method="POST" action="{{route('dashboard.settings.update')}}">
                @csrf
                @foreach (setting("phone_contact") as $phone)
                <div class="form-group">
                  <input class="form-control" name="settings[phone_contact][]" value="{{$phone}}">
                </div>
                @endforeach
                <div class="form-group">
                  <button class="btn btn-primary">@lang('global.save')</button>
                </div>
              </form>
            </div>
          </div>
          <!--End Tab Phone Contact-->


          <!-- Rate Tab -->
          <div class="tab-pane fade " id="setting-tab-tax_rate" role="tabpanel"
            aria-labelledby="setting-tab-tax_rate-tab">
            <div class="form-group">

              <form method="POST" action="{{route('dashboard.settings.update')}}">
                @csrf
                <div class="form-group">
                  <label>@lang('global.tax_rate')</label>
                  <input type="number" step="0.001" class="form-control" name="settings[tax_rate]" value="{{setting('tax_rate')}}" />
                </div>

                <div class="form-group">
                  <button class="btn btn-primary">@lang('global.save')</button>
                </div>
              </form>
            </div>
          </div>

          <!-- end Rate tab -->

          <!-- Rate comuntion -->
          <div class="tab-pane fade " id="setting-tab-comuntion" role="tabpanel"
            aria-labelledby="setting-tab-comuntion-tab">
            <div class="form-group">

              <form method="POST" action="{{route('dashboard.settings.update')}}">
                @csrf
                <div class="form-group">
                  <label>@lang('global.comuntion')</label>
                  <input type="number" step="0.001" class="form-control" name="settings[comuntion]" value="{{setting('comuntion')}}" />
                </div>

                <div class="form-group">
                  <button class="btn btn-primary">@lang('global.save')</button>
                </div>
              </form>
            </div>
          </div>

          <!-- end comuntion tab -->


          <!-- user_line_time Tab -->
          <div class="tab-pane fade " id="setting-tab-user_line_time" role="tabpanel"
            aria-labelledby="setting-tab-user_line_time-tab">
            <div class="form-group">

              <form method="POST" action="{{route('dashboard.settings.update')}}">
                @csrf
                <div class="form-group">
                  <label>@lang('global.user_line_time')</label>
                  <input type="number" step="0.001" class="form-control" name="settings[user_line_time]" value="{{setting('user_line_time')}}" />
                </div>

                <div class="form-group">
                  <button class="btn btn-primary">@lang('global.save')</button>
                </div>
              </form>
            </div>
          </div>

          <!-- end user_line_time tab -->


          <!-- pickup Tab -->
          <div class="tab-pane fade " id="setting-tab-pickup" role="tabpanel"
            aria-labelledby="setting-tab-pickup-tab">
            <div class="form-group">

              <form method="POST" action="{{route('dashboard.settings.update')}}">
                @csrf
                <div class="form-group">
                  <label>@lang('global.pickup')</label>
                  <input type="number" step="0.001" class="form-control" name="settings[pickup]" value="{{setting('pickup')}}" />
                </div>

                <div class="form-group">
                  <button class="btn btn-primary">@lang('global.save')</button>
                </div>
              </form>
            </div>
          </div>

          <!-- end pickup tab -->


          <!-- unit_distance Tab -->
          <div class="tab-pane fade " id="setting-tab-unit_distance" role="tabpanel"
            aria-labelledby="setting-tab-unit_distance-tab">
            <div class="form-group">

              <form method="POST" action="{{route('dashboard.settings.update')}}">
                @csrf
                <div class="form-group">
                  <label>@lang('global.tax_rate')</label>
                  <input type="text" step="0.001" class="form-control" name="settings[unit_distance]" value="{{setting('unit_distance')}}" />
                </div>

                <div class="form-group">
                  <button class="btn btn-primary">@lang('global.save')</button>
                </div>
              </form>
            </div>
          </div>

          <!-- end unit_distance tab -->





          <!-- time zone Tab -->
          <div class="tab-pane fade " id="setting-tab-default_timezone" role="tabpanel"
            aria-labelledby="setting-tab-default_timezone-tab">
            <div class="form-group">

              <form method="POST" action="{{route('dashboard.settings.update')}}">
                @csrf
                <div class="form-group">
                  <label>@lang('global.default_timezone')</label>
                  <input type="number" class="form-control" name="settings[default_timezone]" value="{{setting('default_timezone')}}" />
                </div>


                <div class="form-group">
                  <button class="btn btn-primary">@lang('global.save')</button>
                </div>
              </form>
            </div>
          </div>

          <!-- end time zone tab -->

        </div>
      </div>
    </div>
  </div>



</div>
</div>
@stop

@push('js')
<script src="https://cdn.tiny.cloud/1/n7ktzsgqxg5xtbpjdizih48kxpa99cyvff45mr62naqhy7wy/tinymce/5/tinymce.min.js"
  referrerpolicy="origin"></script>
<script>
  tinymce.init({selector:'textarea',height : "500"});
</script>
@endpush
