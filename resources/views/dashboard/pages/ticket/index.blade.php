@extends('dashboard.layouts.index')
@section('title')
@lang('global.tickets')
@stop

@section('content')
<div class="card">
  <div class="card-header">
    <h4 class="card-title">@lang('global.tickets')</h4>
  </div>
  <div class="card-content">
    <div class="card-body">
      @if (session('success'))
      <div class="alert alert-success" role="alert">
        {{ session('success') }}
      </div>
      @endif
      <table id="dt-ticket" class="table table-striped table-bordered complex-headers">
        <thead>
          <tr>
            <th>#</th>
            <th>@lang('global.user')</th>
            <th>@lang('global.message')</th>
            <th>@lang('global.image')</th>
            <th>@lang('global.created_at')</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
              <th>#</th>
              <th>@lang('global.user')</th>
              <th>@lang('global.message')</th>
              <th>@lang('global.image')</th>
              <th>@lang('global.created_at')</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
@stop

@push("js")
<script>
$(function(){
    var table = $("#dt-ticket").DataTable({
        processing: true,
        serverSide: true,
        responsive: {
            details: true
        },
        ajax: '{{route("dashboard.ticket.datatable")}}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'user', name: 'user' },
            { data: 'message', name: 'message' },
            { data: 'image', name: 'image' },
            { data: 'created_at', name: 'created_at' }
        ],
        initComplete: function () {
        }
    });

});

</script>
@endpush
