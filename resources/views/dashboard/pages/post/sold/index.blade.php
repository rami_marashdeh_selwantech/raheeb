@extends('dashboard.layouts.index')
@section('title')
@lang('global.posts_sold')
@stop

@section('content')

<div class="card">
  <div class="card-header">
    <h4 class="card-title">@lang('global.posts_sold')</h4>
  </div>
  <div class="card-content">
    <div class="card-body">
      @if (session('success'))
      <div class="alert alert-success" role="alert">
        {{ session('success') }}
      </div>
      @endif
      <table id="dt-post_sold" class="table table-striped table-bordered complex-headers">
        <thead>
          <tr>
              <th>#</th>
              <th>@lang('global.user')</th>
              <th>@lang('global.user_sold')</th>
              <th>@lang('global.title')</th>
              <th>@lang('global.category')</th>
              <th>@lang('global.total_price')</th>
              <th>@lang('global.created_at')</th>
              <th>@lang('global.actions')</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
              <th>#</th>
              <th>@lang('global.user')</th>
              <th>@lang('global.user_sold')</th>
              <th>@lang('global.title')</th>
              <th>@lang('global.category')</th>
              <th>@lang('global.total_price')</th>
              <th>@lang('global.created_at')</th>
              <th>@lang('global.actions')</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>

@stop

@push('js')

<script>
$(function(){
    var table = $("#dt-post_sold").DataTable({
        processing: true,
        serverSide: true,
        responsive: {
            details: true
        },
        ajax: '{{route("dashboard.post_sold.datatable")}}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'user', name: 'user' },
            { data: 'user_sold', name: 'user_sold' },
            { data: 'title', name: 'title' },
            { data: 'category', name: 'category' },
            { data: 'total_price', name: 'total_price' },
            { data: 'created_at', name: 'created_at' },
            { data: 'actions', name: 'actions' },
        ],
        initComplete: function () {
            // Button bloacked
            $("table").on("click","#dt-btn-action",function(){
                var action_id = $(this).attr("data-actionId");
                let url = $(this).data("url");
                $.ajax({
                    url: url,
                    method:"POST",
                    success:function(){
                        table.ajax.reload(null, false);
                    }
                })
            });
            //End Button bloacked
        }
    });
});

</script>

@endpush
