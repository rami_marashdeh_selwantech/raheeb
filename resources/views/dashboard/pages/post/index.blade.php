@extends('dashboard.layouts.index')
@section('title')
@lang('global.posts')
@stop

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="form-body">
                        <ul class="nav nav-tabs nav-linetriangle no-hover-bg">
                             <li class="nav-item">
                                 <a class="nav-link active" id="setting-tab-all-tab" data-toggle="pill" href="#setting-tab-all" role="tab" aria-controls="setting-tab-general-all">
                                    @lang('global.allposts')
                                 </a>
                             </li>

                             @foreach($status as $stat)
                                <li class="nav-item">
                                     <a class="nav-link " id="setting-tab-{{$stat}}-tab" data-toggle="pill" href="#setting-tab-{{$stat}}" role="tab" aria-controls="setting-tab-general-{{$stat}}">
                                          @lang("global.$stat")
                                     </a>
                                 </li>
                              @endforeach

                         </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="setting-tab-all" role="tabpanel" aria-labelledby="setting-tab-all-tab">
                    <table id="dt-post-all" style="width:100%;" class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('global.user')</th>
                                <th>@lang('global.title')</th>
                                <th>@lang('global.category')</th>
                                <th>@lang('global.status')</th>
                                <th>@lang('global.created_at')</th>
                                <th>@lang('global.actions')</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>@lang('global.user')</th>
                                <th>@lang('global.title')</th>
                                <th>@lang('global.category')</th>
                                <th>@lang('global.status')</th>
                                <th>@lang('global.created_at')</th>
                                <th>@lang('global.actions')</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                @foreach($status as $stat)
                <div class="tab-pane fade " id="setting-tab-{{$stat}}" role="tabpanel" aria-labelledby="setting-tab-{{$stat}}-tab">
                    <table id="dt-post-{{$stat}}" data-url="{{route('dashboard.post.datatable' , '$stat')}}" style="width:100%;" class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('global.user')</th>
                                <th>@lang('global.title')</th>
                                <th>@lang('global.category')</th>
                                <th>@lang('global.status')</th>
                                <th>@lang('global.created_at')</th>
                                <th>@lang('global.actions')</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>@lang('global.user')</th>
                                <th>@lang('global.title')</th>
                                <th>@lang('global.category')</th>
                                <th>@lang('global.status')</th>
                                <th>@lang('global.created_at')</th>
                                <th>@lang('global.actions')</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@stop

@push('js')
<script>
$(function(){
    var tableall = null;
    var table;
    if (tableall == null) {
        table = $("#dt-post-all").DataTable({
            serverSide: true,
            processing: true,
            responsive: {
                details: true
            },
            ajax: '{{route("dashboard.post.datatable" , "all")}}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'user', name: 'user' },
                { data: 'title', name: 'title' },
                { data: 'category', name: 'category' },
                { data: 'status', name: 'status' },
                { data: 'created_at', name: 'created_at' },
                { data: 'actions', name: 'actions' },

            ],
            initComplete: function () {
                tableall = table;
            }

        });
    }//end if
    else{
        tableall.ajax.reload(null , false);
    }
    $( "#setting-tab-all-tab" ).click(function() {
        tableall.ajax.reload(null , false);
    });

    '@foreach($status as $stat)'
    $(function(){
        var tableall2 = null;
        $( "#setting-tab-{{$stat}}-tab" ).click(function() {
            var table;
            if (tableall2 == null) {
                table = $("#dt-post-{{$stat}}").DataTable({
                    serverSide: true,
                    processing: true,
                    responsive: {
                        details: true
                    },
                    ajax:"{{route('dashboard.post.datatable' , $stat)}}",
                    columns: [
                        { data: 'id', name: 'id' },
                        { data: 'user', name: 'user' },
                        { data: 'title', name: 'title' },
                        { data: 'category', name: 'category' },
                        { data: 'status', name: 'status' },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'actions', name: 'actions' },
                    ],
                    initComplete: function () {
                        tableall2 = table;
                    }
                });
            } // end if
            else{
                 tableall2.ajax.reload(null , false);
            }
        });
    });
    '@endforeach'
});
</script>
@endpush
