<div class="card-content">
    <div class="card-body">
            <table id="dt-faverates" class="table table-striped table-bordered complex-headers">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>@lang('global.avatar')</th>
                        <th>@lang('global.name')</th>
                        <th>@lang('global.phone_number')</th>
                        <th>@lang('global.email')</th>
                        <th>@lang('global.created_at')</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>@lang('global.avatar')</th>
                        <th>@lang('global.name')</th>
                        <th>@lang('global.phone_number')</th>
                        <th>@lang('global.email')</th>
                        <th>@lang('global.created_at')</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>


@push('js')
<script>
$(function(){
    var table = $("#dt-faverates").DataTable({
        processing: true,
        serverSide: true,
        autoWidth:false,
        ajax: '{{route("dashboard.post.faverate_datatable" , $post->id)}}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'avatar', name: 'avatar' },
            { data: 'name', name: 'name' },
            { data: 'phone_number', name: 'phone_number' },
            { data: 'email', name: 'email' },
            { data: 'created_at', name: 'created_at' },
        ],
        initComplete: function () {
            // Button bloacked
            $("table").on("click","#dt-btn-action",function(){
                var action_id = $(this).attr("data-actionId");
                let url = $(this).data("url");
                $.ajax({
                    url: url,
                    method:"POST",
                    success:function(){
                        table.ajax.reload(null, false);
                    }
                })
            });
            //End Button bloacked
        }
    });
});

</script>

@endpush
