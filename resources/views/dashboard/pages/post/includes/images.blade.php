<!-- The four columns -->
<div class="card-content">
    <div class="card-body">
		<div class="row">

		  @foreach($post->images as $image)
		  <div class="gallery">
			  <a target="_blank" href="{{$image->image}}">
			    <img src="{{$image->image}}" alt="Cinque Terre" width="600" height="400">
			  </a>

		  </div>

		  @endforeach

		</div>
	</div>
</div>

@push('js')
<script>

</script>
@endpush
