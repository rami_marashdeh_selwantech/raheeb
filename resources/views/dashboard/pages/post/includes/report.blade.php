<div class="card-content">
    <div class="card-body">
    <table id="dt-report"  class="table table-striped">
        <thead>
          <tr>
              <th>#</th>
              <th>@lang('global.user_report')</th>
              <th>@lang('global.title')</th>
              <th>@lang('global.message')</th>
              <th>@lang('global.created_at')</th>
              <th>@lang('global.actions')</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
              <th>#</th>
              <th>@lang('global.user_report')</th>
              <th>@lang('global.title')</th>
              <th>@lang('global.message')</th>
              <th>@lang('global.created_at')</th>
              <th>@lang('global.actions')</th>
          </tr>
        </tfoot>
    </table>
</div>
</div>

@push('js')
<script>
$(function(){

    var table = $("#dt-report").DataTable({
        serverSide: true,
        processing: true,
        autoWidth:false,
        ajax: '{{route("dashboard.post.report_data_table" , $post->id )}}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'user_report', name: 'user_report' },
            { data: 'title', name: 'title' },
            { data: 'message', name: 'message' },
            { data: 'created_at', name: 'created_at' },
            { data: 'actions', name: 'actions' },
        ],
        initComplete: function () {

        }

    });
});
</script>
@endpush
