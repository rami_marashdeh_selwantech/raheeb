<div class="card">
    <div class="row">
        @if($post->status == \App\Models\Post\Post::STATUS_SOLD_OTHER_APP)
        <div class="form-group col-md-12 p-2">
            <h3 class="card-title"> <strong>@lang('global.sold_other_app')</strong></h3>
        </div>
        @elseif($post->status == \App\Models\Post\Post::STATUS_SOLD)

        <div class="form-group col-md-6 p-2">
            <div class="card-header white bg-warning">
                <h3 class="card-title white"> <strong>@lang('global.user_sold_information')</strong></h3>
            </div>
            <div class="card-content collapse show">
                <div class="card-body border-bottom-warning">
                    <label class="col-md-6"> <strong>@lang('global.user') : </strong>  </label> {{$post->postSold->user->name}}
                    <label class="col-md-6"> <strong>@lang('global.email') : </strong>  </label> {{$post->postSold->user->email ? $post->postSold->user->email : "NULL"}}
                    <label class="col-md-6"> <strong>@lang('global.phone_number') : </strong>  </label> {{$post->postSold->user->phone_number}}
                    <label class="col-md-6"> <strong>@lang('global.followers') : </strong>  </label> {{$post->postSold->user->countFollowers($post->user->id)}}
                    <label class="col-md-6"> <strong>@lang('global.following') : </strong>  </label> {{$post->postSold->user->countFollowing($post->user->id)}}
                    <label class="col-md-6"> <strong>@lang('global.balance') : </strong>  </label> {{$post->postSold->user->balance}}
                    <label class="col-md-6"> <strong>@lang('global.login_with') : </strong>  </label> {{$post->postSold->user->login_with}}
                    <label class="col-md-6"> <strong>@lang('global.is_valid') : </strong>  </label> @if($post->postSold->user->is_valid == 1) @lang('global.true') @else @lang('global.false') @endif

                </div>
            </div>
        </div>

        <div class="form-group col-md-6 p-2">
            <div class="card-header white bg-warning">
                <h3 class="card-title white"> <strong>@lang('global.sold_informations')</strong></h3>
            </div>
            <div class="card-content collapse show">
                <div class="card-body border-bottom-warning">
                    <label class="col-md-6"> <strong>@lang('global.payment_method') : </strong>  </label> {{$post->postSold->payment_method}}
                    <label class="col-md-6"> <strong>@lang('global.total_price') : </strong>  </label> {{$post->postSold->total_price}}
                    <label class="col-md-6"> <strong>@lang('global.date') : </strong>  </label> {{$post->postSold->created_at->diffForHumans()}}
                </div>
            </div>
        </div>


        @foreach($post->postSold->postSoldRate as $rate)
        <div class="form-group col-md-6 p-2">
            <div class="card-header white bg-warning">
                @if($rate->user_id == $post->user_id)
                <h3 class="card-title white"> <strong>@lang('global.user_post_rate')</strong></h3>
                @else
                <h3 class="card-title white"> <strong>@lang('global.user_sold_rate')</strong></h3>
                @endif
            </div>
            <div class="card-content collapse show">
                <div class="card-body border-bottom-warning">
                    <label class="col-md-6"> <strong>@lang('global.id') : </strong>  </label> {{$rate->user->id}}
                    <label class="col-md-6"> <strong>@lang('global.user') : </strong>  </label> {{$rate->user->name}}
                    <label class="col-md-6"> <strong>@lang('global.rate') : </strong>  </label> {{$rate->rate}}
                    <label class="col-md-6"> <strong>@lang('global.commint') : </strong>  </label> {{$rate->commint}}
                    <label class="col-md-6"> <strong>@lang('global.date') : </strong>  </label> {{$rate->created_at->diffForHumans()}}
                </div>
            </div>
        </div>
        @endforeach
        @endif
    </div>
</div>
