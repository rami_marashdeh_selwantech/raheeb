<div class="card">
    <div class="row">

        <div class="form-group col-md-6 p-2">
            <div class="card-header white bg-warning">
                <h3 class="card-title white"> <strong>@lang('global.post_information')</strong></h3>
            </div>
            <div class="card-content collapse show">
                <div class="card-body border-bottom-warning">
                    <label class="col-md-6"> <strong>@lang('global.title') : </strong>  </label> {{$post->title}}
                    <label class="col-md-6"> <strong>@lang('global.description') : </strong>  </label> <label> {{$post->description}}</label>
                    <label class="col-md-6"> <strong>@lang('global.condition') : </strong>  </label> {{$post->condition->name}}
                    <label class="col-md-6"> <strong>@lang('global.status') : </strong>  </label> @lang("global.$post->status")
                    <label class="col-md-6"> <strong>@lang('global.price') : </strong>  </label> {{$post->price}}
                    <label class="col-md-6"> <strong>@lang('global.is_ship_nationwide') : </strong>  </label> @if($post->is_ship_nationwide == 1) @lang('global.true') @else @lang('global.false') @endif
                    <label class="col-md-6"> <strong>@lang('global.category_box_size') : </strong>  </label>@if($post->is_ship_nationwide == 1) {{$post->categoryBoxSize->size}} {{$post->categoryBoxSize->unit}} @else NULL @endif
                    <label class="col-md-6"> <strong>@lang('global.category_box_price') : </strong>  </label>@if($post->is_ship_nationwide == 1) {{$post->categoryBoxSize->price}} @else NULL @endif
                    <label class="col-md-6"> <strong>@lang('global.count_faverate') : </strong>  </label> {{$post->faverates->count()}}
                    <label class="col-md-6"> <strong>@lang('global.date') : </strong>  </label> {{$post->created_at->diffForHumans()}}
                </div>
            </div>
        </div>

        <div class="form-group col-md-6 p-2">
            <div class="card-header white bg-warning">
                <h3 class="card-title white"> <strong>@lang('global.post_address')</strong></h3>
            </div>
            <div id="googlemap"></div>
        </div>

        <div class="form-group col-md-6 p-2">
            <div class="card-header white bg-warning">
                <h3 class="card-title white"> <strong>@lang('global.user_post_information')</strong></h3>
            </div>
            <div class="card-content collapse show">
                <div class="card-body border-bottom-warning">
                    <label class="col-md-6"> <strong>@lang('global.user') : </strong>  </label> {{$post->user->name}}
                    <label class="col-md-6"> <strong>@lang('global.email') : </strong>  </label> {{$post->user->email ? $post->user->email : "NULL"}}
                    <label class="col-md-6"> <strong>@lang('global.phone_number') : </strong>  </label> {{$post->user->phone_number}}
                    <label class="col-md-6"> <strong>@lang('global.followers') : </strong>  </label> {{$post->user->countFollowers($post->user->id)}}
                    <label class="col-md-6"> <strong>@lang('global.following') : </strong>  </label> {{$post->user->countFollowing($post->user->id)}}
                    <label class="col-md-6"> <strong>@lang('global.balance') : </strong>  </label> {{$post->user->balance}}
                    <label class="col-md-6"> <strong>@lang('global.login_with') : </strong>  </label> {{$post->user->login_with}}
                    <label class="col-md-6"> <strong>@lang('global.is_valid') : </strong>  </label> @if($post->user->is_valid == 1) @lang('global.true') @else @lang('global.false') @endif
                </div>
            </div>
        </div>

    </div>
</div>



@push('js')
<script>
  function initMap() {
    var locations = [
      ['{{$post->title}}', '{{$post->lat}}', '{{$post->lon}}', 1]
    ];

    var map = new google.maps.Map(document.getElementById('googlemap'), {
      zoom: 12,
      center: new google.maps.LatLng( '{{$post->lat}}', '{{$post->lon}}'),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  }
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMmGjjUUaUbpd-j8wjKI9YaufzaVI5LgY&callback=initMap">
</script>

@endpush
