@extends('dashboard.layouts.index')
@section('title')
@lang('global.post_details')
@stop

@section('content')

  <div class="card">
      <div class="card-header">
          <ul class="breadcrumb">
            <li><a href="{{route('dashboard.index')}}">@lang('global.dashboard')</a></li>
            <li><a href="{{route('dashboard.post.index')}}">@lang('global.posts')</a></li>
            <li>@lang('global.post_details')</li>
          </ul>
        <div class="heading-elements">
          <a href="{{route("dashboard.post.index")}}" class="btn btn-black">
            <i class="ft-arrow-left"></i>
            @lang('global.back')
          </a>
        </div>
      </div>
  </div>
  <div class="row">

    <div class="col-sm-12 col-md-12">
      <div class="card">

        <div class="card-content">
          <div class="card-body">
            <div class="form-body">
                <ul class="nav nav-tabs nav-linetriangle no-hover-bg">

                    <li class="nav-item">
                        <a class="nav-link active" id="setting-tab-all-tab" data-toggle="pill" href="#setting-tab-all" role="tab" aria-controls="setting-tab-general-all">@lang('global.home')</a>
                    </li>

                    @if($post->status != \App\Models\Post\Post::STATUS_AVAILABLE)
                    <li class="nav-item">
                        <a class="nav-link" id="setting-tab-sold-tab" data-toggle="pill" href="#setting-tab-sold" role="tab" aria-controls="setting-tab-general-sold">@lang('global.sold')</a>
                    </li>
                    @endif

                    <li class="nav-item">
                        <a class="nav-link" id="setting-tab-offer-tab" data-toggle="pill" href="#setting-tab-offer" role="tab" aria-controls="setting-tab-general-offer">@lang('global.offer')</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="setting-tab-faverate-tab" data-toggle="pill" href="#setting-tab-faverate" role="tab" aria-controls="setting-tab-general-faverate">@lang('global.post_faverates')</a>

                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="setting-tab-images-tab" data-toggle="pill" href="#setting-tab-images" role="tab" aria-controls="setting-tab-general-images">@lang('global.post_images')</a>

                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="setting-tab-report-tab" data-toggle="pill" href="#setting-tab-report" role="tab" aria-controls="setting-tab-general-report">@lang('global.post_report')</a>
                    </li>

                </ul>


              	</div>

              </div>


              <div class="row">
                  <div class="col-12">

                      <div class="tab-content" id="v-pills-tabContent">

                          <div class="tab-pane fade show active" id="setting-tab-all" role="tabpanel" aria-labelledby="setting-tab-all-tab">
                              <br>
                              <br>
                              @include("dashboard.pages.post.includes.home")
                          </div>

                          <div class="tab-pane fade " id="setting-tab-sold" role="tabpanel" aria-labelledby="setting-tab-sold-tab">
                              <br>
                              <br>
                              @include("dashboard.pages.post.includes.sold")
                          </div>

                          <div class="tab-pane fade" id="setting-tab-offer" role="tabpanel" aria-labelledby="setting-tab-offer-tab">

                            @include("dashboard.pages.post.includes.offer")
                          </div>

                          <div class="tab-pane fade" id="setting-tab-faverate" role="tabpanel" aria-labelledby="setting-tab-faverate-tab">
                              <br>
                              <br>
                              @include("dashboard.pages.post.includes.faverate")
                          </div>


                        <div class="tab-pane fade" id="setting-tab-images" role="tabpanel" aria-labelledby="setting-tab-images-tab">
                            <br>
                            <br>
                            @include("dashboard.pages.post.includes.images")
                        </div>


                        <div class="tab-pane fade" id="setting-tab-report" role="tabpanel" aria-labelledby="setting-tab-report-tab">
                          <br>
                          <br>
                          @include("dashboard.pages.post.includes.report")
                        </div>




                      </div>
                  </div>


      		  </div>


            </div>
          </div>
        </div>
      </div>
    </div>
   </div>

@stop

@push('js')
<script>

</script>



@endpush
