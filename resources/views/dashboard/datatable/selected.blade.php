<div>
	    <label class="td-lable"> 
        {{$value}}
    </label> 
    <select data-url="{{$url}}" data-namedata="{{$name}}"  style="display: none;" class="td-input form-control"  name="{{$name}}">
    	<option value="{{$value}}">{{$value}}</option>
    	@if($value == $type_user)
    	<option value="{{$type_worker}}">{{$type_worker}}</option>
    	@else
    	<option value="{{$type_user}}">{{$type_user}}</option>
    	@endif

    </select>
</div>