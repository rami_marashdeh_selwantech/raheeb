
<div class="card">

  <div style="height: 100%;" id="googlemaps-{{$id}}"></div>

</div>


 @push('js')
<script>
  function initMap() {
    var locations = [
      ['location', '{{$lat}}', '{{$lon}}', 1]
    ];

    var map = new google.maps.Map(document.getElementById('googlemaps-{{$id}}'), {
      zoom: 12,
      center: new google.maps.LatLng( '{{$lat}}', '{{$lon}}'),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  }
</script>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjo591fFC7SNHbLzmbj6Np1jpPSbt5K2U&callback=initMap">
    </script>

@endpush
