<?php

return [
  "roles" => [
    "super-admin",
    "admin",
    "user"
  ],
  "permissions" => [
        "m_user" => [
            "view",
            "edit",
            "create",
            "delete",
            "blocked"
        ],
        "m_admin" => [
            "view",
            "edit",
            "create",
            "delete",
            "blocked"
        ],
        "m_super_admin" => [
            "view",
            "edit",
            "create",
            "delete"
        ],
        "m_categories" => [
            "view",
            "edit",
            "create"
        ],
        "m_category_box_size" => [
            "view",
            "edit",
            "create"
        ],
        "m_conditions" => [
            "view",
            "edit",
            "create"
        ],
        "m_notifications" => [
            "view",
            "create"
        ],
        "m_post" => [
            "view"
        ],
        "m_post_solds" => [
            "view"
        ],
        "m_post_report" => [
            "view"
        ],
        "m_ticket" => [
            "view"
        ],
        "m_settings" => [
            "view",
            "edit",
        ]
    ]
];
