<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace("Api")->group(function(){
	Route::prefix("auth")->namespace("Auth")->group(function(){

		Route::post("login" , "LoginController@login");
		Route::post("new_register" , "RegisterController@register");

		//  Verify Phone Routes
        Route::prefix('verifyPhone')->group(function () {
        	Route::post('/', 'VerifyPhoneController@verifyPhoneNumber');
            Route::post('reset-code', 'VerifyPhoneController@resetCode');
            Route::post('check-code', 'VerifyPhoneController@checkCode');
        });

        //  Verify Forgot Password Routes
        Route::prefix('forgotPassword')->group(function () {
            Route::post('/verifyPhone', 'ForgotPasswordController@verifyPhoneNumber');
            Route::post('/update/password', 'ForgotPasswordController@updatePassword');
        });
	});

    Route::prefix("user")->namespace("User")->middleware("auth:api")->group(function(){
        Route::get("profile", "UserController@profile");
        Route::post("validation", "UserController@validation");
        Route::post("update/coverimage", "UserController@updateCoverImage");
        Route::post("update/avatar", "UserController@updateAvatar");
        Route::post("update/password", "UserController@updatePassword");
		Route::post("update/check_send_email", "UserController@check_send_email");
		Route::post("update/check_send_notification", "UserController@check_send_notification");
		Route::post("update/email", "UserController@updateEmail");
		Route::post("update/phone_number", "UserController@updatePhoneNumber");
		Route::post("{user}/report", "UserController@report");
        Route::post("logout", "UserController@logout");

		Route::post("ticket" , "UserTicketController@ticket");

        Route::prefix("post")->group(function(){
            Route::get("/" , "UserPostController@getUserPost");
            Route::get("/buying" , "UserPostController@getUserPostBuying");
			Route::get("{post}/mark_sold" , "UserPostController@getUserMarkSold");
            Route::post("/" , "UserPostController@post");
            Route::post("{post}/update" , "UserPostController@updatePost");
            Route::post("image", "UserPostController@image");
            Route::delete("{image}/image", "UserPostController@deleteImage");
            Route::post("{post}/faverate" , "UserFavoritePostController@faverate");
            Route::post("{post}/unFaverate" , "UserFavoritePostController@unFaverate");
			Route::post("{post}/report" , "UserPostController@report");

            Route::post("{post}/offer" , "UserOfferPostController@setOffer");
			Route::post("offer/{post_offer}/accept" , "UserOfferPostController@acceptOffer");

            Route::prefix("sold")->group(function(){
                Route::get("{post}/calculate_price" , "UserPostSoldController@calculatePostPrice");
                Route::post("{post}/buy_now" , "UserPostSoldController@buyNowPost");
                Route::post("{post}/set_sold" , "UserPostSoldController@setSoldPost");

                Route::post("rate/{post_sold}" ,  "UserPostSoldController@setRatePostSold");
            });
        });

        Route::prefix("chat")->group(function(){
            Route::get("/" , "UserMessageController@userChats");
			Route::get("{chat_post}/show" , "UserMessageController@getChatFromID");
            Route::get("{chat}/messages" , "UserMessageController@userChatMessages");
            Route::post("post/{post}/create" , "UserMessageController@createChate");
            Route::post("{chat}/messages" , "UserMessageController@sendMessage");
        });

        Route::prefix("faverate")->group(function(){
            Route::get("/" , "UserFavoritePostController@userFaverate");
        });

        Route::prefix("follower")->group(function(){
            Route::post("follow" , "UserFollowerController@follow");
            Route::post("unfollow" , "UserFollowerController@unFollow");
            Route::get("followers" , "UserFollowerController@getFollowers");
            Route::get("following" , "UserFollowerController@getFollowing");
        });

        Route::prefix("address")->group(function(){
            Route::get("/", "UserAddressController@show");
            Route::post("/", "UserAddressController@store");
            Route::post("distance", "UserAddressController@storeDistance");
        });

        Route::prefix("notifications")->group(function(){
            Route::get("/" , "UserNotificationController@getNotifications");
        });
    });

    Route::prefix("categories")->namespace("Category")->group(function () {
        Route::get("/" , "CategoryController@all");
        Route::get("{category}/boxsize" , "CategoryController@categoryBoxSize");
    });

    Route::prefix("posts")->namespace("Post")->group(function () {
        Route::get("/" , "PostController@getPost");
		Route::get("{post}/show" , "PostController@show");
    });

    Route::prefix("condition_type")->namespace("Condition")->group(function () {
        Route::get("/" , "ConditionController@show");
    });

    Route::prefix("users/{user}")->namespace("User")->group(function(){
        Route::get("profile" , "UserController@userProfile");
		Route::get("followers" , "UserFollowerController@getFollowers");
		Route::get("following" , "UserFollowerController@getFollowing");
		Route::get("offers" , "UserOfferPostController@userOfferPost")->middleware("auth:api");
    });


    Route::prefix("app")->namespace("App")->group(function () {
        Route::get("privacy-policy", "AppController@privacyPolicy");
        Route::get("about-us", "AppController@aboutUs");
        Route::get("distance", "AppController@distance");
		Route::get("currency" , "AppController@currency");
    });

});
