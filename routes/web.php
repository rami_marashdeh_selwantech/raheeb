<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('dashboard.index');
});

Auth::routes(["register" => false]);

Route::prefix("dashboard")->namespace("Dashboard")->name("dashboard.")->middleware(["auth:web"])->group(function () {

	Route::get("/", "HomeController@index")->name("index");

    Route::prefix("users")->namespace("User")->name("user.")->group(function(){
        Route::get("/" , "UserController@index")->name("index");
        Route::get("datatable" , "UserController@datatable")->name("datatable");
        Route::get("create" , "UserController@create")->name("create");
        Route::get("{user}/edit" , "UserController@edit")->name("edit");
        Route::get("{user}/details" , "UserController@details")->name("details");
        Route::post("store" , "UserController@store")->name("store");
        Route::get("{user}/user_post_sold_datatable" , "UserController@user_post_sold_datatable")->name("user_post_sold_datatable");
        Route::get("{user}/user_offer_datatable" , "UserController@user_offer_datatable")->name("user_offer_datatable");
        Route::prefix("{user}/update")->name("update.")->group(function(){
            Route::post("user" , "UpdateUserController@updateUser")->name("user");
            Route::post("admin" , "UpdateUserController@updateAdmin")->name("admin");
            Route::post("name" , "UpdateUserController@name")->name("name");
			Route::post("phone_number" , "UpdateUserController@phone_number")->name("phone_number");
			Route::post("is_blocked" , "UpdateUserController@is_blocked")->name("is_blocked");
        });
    });

    Route::prefix("admin")->namespace("User")->name("admin.")->group(function(){
        Route::get("/" , "AdminController@index")->name("index");
        Route::get("datatable" , "AdminController@datatable")->name("datatable");
        Route::get("create" , "AdminController@create")->name("create");
        Route::get("{user}/edit" , "AdminController@edit")->name("edit");
        Route::post("store" , "AdminController@store")->name("store");
    });

    Route::prefix("categories")->namespace("Category")->name("category.")->group(function(){
        Route::get("/" , "CategoryController@index")->name("index");
        Route::get("datatable" , "CategoryController@datatable")->name("datatable");
        Route::get("create" , "CategoryController@create")->name("create");
        Route::get("{category}/edit" , "CategoryController@edit")->name("edit");
        Route::get("{category}/details" , "CategoryController@details")->name("details");
        Route::post("store" , "CategoryController@store")->name("store");
        Route::prefix("{category}/update")->name("update.")->group(function(){
            Route::post("all" , "UpdateCategoryController@all")->name("all");
            Route::post("name" , "UpdateCategoryController@name")->name("name");
            Route::post("is_ship_nationwide" , "UpdateCategoryController@is_ship_nationwide")->name("is_ship_nationwide");
            Route::post("is_valid" , "UpdateCategoryController@is_valid")->name("is_valid");
            Route::post("ordering" , "UpdateCategoryController@ordering")->name("ordering");
        });
        Route::prefix("{category}/box_size")->name("box_size.")->group(function(){
            Route::get("/" , "BoxSizeCategoryController@index")->name("index");
            Route::get("datatable" , "BoxSizeCategoryController@datatable")->name("datatable");
            Route::get("create" , "BoxSizeCategoryController@create")->name("create");
            Route::get("{category_box_size}/edit" , "BoxSizeCategoryController@edit")->name("edit");
            Route::post("store" , "BoxSizeCategoryController@store")->name("store");
            Route::post("{category_box_size}/update" , "BoxSizeCategoryController@update")->name("update");
        });
    });

    Route::prefix("condition")->namespace("Condition")->name("condition.")->group(function(){
        Route::get("/" , "ConditionController@index")->name("index");
        Route::get("datatable" , "ConditionController@datatable")->name("datatable");
        Route::get("create" , "ConditionController@create")->name("create");
        Route::get("{condition}/edit" , "ConditionController@edit")->name("edit");
        Route::post("store" , "ConditionController@store")->name("store");
        Route::prefix("{condition}/update")->name("update.")->group(function(){
            Route::post("all" , "ConditionController@all")->name("all");
            Route::post("name" , "ConditionController@name")->name("name");
            Route::post("is_show" , "ConditionController@is_show")->name("is_show");
            Route::post("ordering" , "ConditionController@ordering")->name("ordering");
        });
    });

    Route::prefix("distance")->namespace("Distance")->name("distance.")->group(function(){
        Route::get("/" , "DistanceController@index")->name("index");
        Route::get("datatable" , "DistanceController@datatable")->name("datatable");
        Route::post("store" , "DistanceController@store")->name("store");
        Route::post("{distance}/delete" , "DistanceController@delete")->name("delete");
        Route::prefix("{distance}/update")->name("update.")->group(function(){
            Route::post("distance" , "DistanceController@distance")->name("distance");
            Route::post("is_show" , "DistanceController@is_show")->name("is_show");
        });
    });

    Route::prefix("post")->namespace("Post")->name("post.")->group(function(){
        Route::get("/" , "PostController@index")->name("index");
        Route::get("{status}/datatable" , "PostController@datatable")->name("datatable");
        Route::get("{user}/user_datatable" , "PostController@user_datatable")->name("user_datatable");
        Route::get("{user}/user_faverate_datatable" , "PostController@user_faverate_datatable")->name("user_faverate_datatable");
        Route::get("{post}/details" , "PostController@details")->name("details");
        Route::get("{post}/faverate_datatable" , "PostController@faverateDatatable")->name("faverate_datatable");
        Route::get("{post}/offer_datatable" , "PostController@offerDatatable")->name("offer_datatable");
        Route::get("{post}/report_data_table" , "PostController@report_data_table")->name("report_data_table");
    });

    Route::prefix("post_sold")->namespace("Post")->name("post_sold.")->group(function(){
        Route::get("/" , "PostSoldController@index")->name("index");
        Route::get("datatable" , "PostSoldController@datatable")->name("datatable");
        Route::get("{post}/details" , "PostSoldController@details")->name("details");
    });

    Route::prefix("post_report")->namespace("Post")->name("post_report.")->group(function(){
        Route::get("/" , "PostReportController@index")->name("index");
        Route::get("datatable" , "PostReportController@datatable")->name("datatable");
        Route::get("{post}/details" , "PostReportController@details")->name("details");
    });

    Route::prefix("user_report")->namespace("User")->name("user_report.")->group(function(){
        Route::get("/" , "UserReportController@index")->name("index");
        Route::get("datatable" , "UserReportController@datatable")->name("datatable");
        Route::get("{user_report}/details" , "UserReportController@details")->name("details");
    });

    Route::prefix("ticket")->namespace("Ticket")->name("ticket.")->group(function(){
        Route::get("/" , "TicketController@index")->name("index");
        Route::get("datatable" , "TicketController@datatable")->name("datatable");
    });

    Route::prefix("settings")->namespace("Setting")->name("settings.")->group(function () {
        Route::get("/", "SettingController@index")->name("index");
        Route::post("/update", "SettingController@update")->name("update");
    });



});
