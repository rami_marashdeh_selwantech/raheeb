<?php

namespace App\Models\Condition;

use Illuminate\Database\Eloquent\Model;

class ConditionTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = ['name'];
}
