<?php

namespace App\Models\Chat;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Post\PostOffer;

class ChatPostMessage extends Model
{
    public function sender(){
        return $this->belongsTo(User::class, "sender_id");
    }

     public function getMessageAttribute($message){

         return $this->message_type == ChatPost::TYPE_VOICE && $message ? url($message) : $message;
     }

    public function postOffer(){
        return $this->belongsTo(PostOffer::class);
    }
}
