<?php

namespace App\Models\Chat;

use Illuminate\Database\Eloquent\Model;
use App\Models\Post\Post;
use App\User;

class ChatPost extends Model
{
    const TYPE_FILE = "file";
    const TYPE_VOICE = "voice";
    const TYPE_TEXT = "text";
    const TYPE_TEXT_OFFER = "offer";
    const TYPE_SOLD = "sold";

    public function post(){
        return $this->belongsTo(Post::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function messages(){
        return $this->hasMany(ChatPostMessage::class);
    }

    public static function createChat($post_id,$data , $chat = null){
        if ($chat != null) {
            $chat_post = ChatPost::find($chat);
        }else {
            $chat_post = static::where("user_id" , authApi()->id())
        		->where("post_id" , $post_id)
        		->first();
        }

    	if (! $chat_post) {
    		$chat_post = new ChatPost;
    		$chat_post->user_id = authApi()->id();
    		$chat_post->post_id = $post_id;
    		$chat_post->save();
    	}
    	return self::sendMessage($chat_post->id,$data);
    }

    private static function sendMessage($chat_id , $data){
    	$message = new ChatPostMessage;
    	$message->chat_post_id = $chat_id;
    	$message->sender_id = authApi()->id();
    	$message->post_offer_id = isset($data["offer_id"]) ? $data["offer_id"] : null;
    	$message->message_type = isset($data["offer_id"]) ? static::TYPE_TEXT_OFFER : $data["message_type"] ;
    	$message->message = $data["message"];
    	$message->save();

        return $message;
    }
}
