<?php

namespace App\Models\PhoneVerification;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class PhoneVerification extends Model
{
    protected $fillable = ["code", "token", "phone_number", "is_verify"];


    public static function getByPhoneNumber($phone)
    {
        return static::where("phone_number", $phone)->first();
    }

    public static function checkCode($token, $code)
    {
        $phoneVerification =  static::where("code", $code)->where("token", $token)->first();
        if ($phoneVerification) {
            $phoneVerification->is_verify = 1;
            $phoneVerification->save();
        }
        return $phoneVerification;
    }

    public static function getByToken($token)
    {
        return static::where("token", $token)->first();
    }

    public static function addOrUpdate($phone)
    {
        $phoneVerification = self::getByPhoneNumber($phone);
        if (!$phoneVerification) {
            $phoneVerification = new PhoneVerification;
            $phoneVerification->phone_number = $phone;
        }
        $phoneVerification->code = generateVerifyCode();
        $phoneVerification->token = Str::random(60);
        $phoneVerification->is_verify = 0;
        $phoneVerification->save();
        return $phoneVerification;
    }
}
