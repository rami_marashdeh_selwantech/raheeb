<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    const NEW_MESSAGE = "message";
    const TYPE_OFFER = "offer";
    const TYPE_RATE = "rate";
    const TYPE_ADMIN = "admin";
    const TYPE_APPROVED_OFFER = "approved_offer";
    const TYPE_REJECT_OFFER = "reject_offer";
    const TYPE_FOLLOWING = "following";

    
}
