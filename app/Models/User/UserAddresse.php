<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserAddresse extends Model
{
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public static function createOrUpdate($data)
    {
        $address = static::where("user_id", authApi()->id())->first();
        if (!$address) {
            $address = new UserAddresse;
	        $address->user_id = authApi()->id();
        }
        $address->building_number = $data['building_number'];
	    $address->lat = $data['lat'];
	    $address->lon = $data['lon'];
	    $address->floor = $data['floor'];
	    $address->title = $data['title'];
        $address->save();
    }

    public static function distanceCreateOrUpdate($data){
    	$address = static::where("user_id", authApi()->id())->first();
        if (!$address) {
            $address = new UserAddresse;
	        $address->user_id = authApi()->id();
        }
        $address->distance = $data['distance'];
        $address->save();
    }
}
