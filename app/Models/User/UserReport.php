<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserReport extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }
}
