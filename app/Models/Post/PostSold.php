<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;
use App\User;

class PostSold extends Model
{
    const SHIPPING_CACH = "cash";
    const SHIPPING_ONLINE = "online";

    public function postSoldAddress(){
        return $this->hasOne(PostSoldAddress::class);
    }

    public function postSoldRate(){
        return $this->hasMany(PostSoldRate::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function post(){
        return $this->belongsTo(Post::class);
    }
}
