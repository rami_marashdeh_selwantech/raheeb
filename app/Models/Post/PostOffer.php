<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;
use App\User;

class PostOffer extends Model
{
    const STATUS_APPROVED = "approved";
    const STATUS_WAITING = "waiting";
    const STATUS_REJECT = "reject";

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function post(){
        return $this->belongsTo(Post::class);
    }
}
