<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category\Category;
use App\Models\Category\CategoryBoxSize;
use App\User;
use App\Support\Money;
use App\Models\Chat\ChatPost;
use App\Models\Condition\Condition;

class Post extends Model
{
    const STATUS_AVAILABLE = "available";
    const STATUS_SOLD = "sold";
    const STATUS_SOLD_OTHER_APP = "sold_other_app";

    public function getPriceAttribute($price){
        $priceOffer = $this->postoffers()->where("status",PostOffer::STATUS_APPROVED)
            ->where("user_id" , authApi()->id())
            ->orderBy("created_at","DESC")
            ->first();
        if ($priceOffer) {
            return Money::inDefaultCurrency($priceOffer->price);
        }
        return Money::inDefaultCurrency($price);
    }

    public function getCalculatePostPrice(){
        $price_box =  $this->categoryBoxSize ? $this->categoryBoxSize->price->amount() : 0;
        return Money::inDefaultCurrency(
            $this->price->amount() + $price_box + setting("tax_rate")
            );
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function categoryBoxSize()
    {
        return $this->belongsTo(CategoryBoxSize::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function faverates()
    {
        return $this->hasMany(PostFaverate::class);
    }

    public function images()
    {
        return $this->hasMany(PostImage::class);
    }

    public function condition(){
        return $this->belongsTo(Condition::class);
    }

    public function postoffers(){
        return $this->hasMany(PostOffer::class);
    }

    public function postSold(){
        return $this->hasOne(PostSold::class);
    }

    public function chatposts(){
        return $this->hasMany(ChatPost::class);
    }

    public function postReport(){
        return $this->hasMany(PostReport::class);
    }

    public function getChatid($post_id){
        $chat = $this->chatposts()->where("user_id" , authApi()->id())
            ->where("post_id" , $post_id)->first();

        return $chat ? $chat->id : null ;
    }

    public static function isFaverate($user_id,$post_id){
    	$faverate = PostFaverate::where("user_id" , $user_id)
            ->where("post_id",$post_id)
            ->first();
    	if ($faverate) {
    		return true;
    	}
    	return false;
    }

    public function isOffer(){
        $offer = $this->postoffers()->where("status",PostOffer::STATUS_APPROVED)
            ->where("user_id" , authApi()->id())
            ->orderBy("created_at","DESC")
            ->first();

        return $offer ? true : false ;
    }

    public function getOfferPost(){
        $offer = $this->postoffers()->where("status",PostOffer::STATUS_APPROVED)
            ->where("user_id" , authApi()->id())
            ->orderBy("created_at","DESC")
            ->first();

        return $offer->id;
    }

}
