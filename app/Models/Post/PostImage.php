<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;

class PostImage extends Model
{
    public function getImageAttribute($image){
        return $image ? url($image) : null;
    }


}
