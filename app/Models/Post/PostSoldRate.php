<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;
use App\User;

class PostSoldRate extends Model
{
    public function postSold(){
    	return $this->belongsTo(PostSold::class);
    }

    public static function isUserRate($post_sold_id){
    	return static::where("user_id" , authApi()->id())
    		->where("post_sold_id" , $post_sold_id)->first() ? true : false;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
