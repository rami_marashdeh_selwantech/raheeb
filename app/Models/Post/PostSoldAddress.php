<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;

class PostSoldAddress extends Model
{
    public function postSold(){
        return $this->belongsTo(PostSold::class);
    }
}
