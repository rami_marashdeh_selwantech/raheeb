<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;
use App\User;

class PostReport extends Model
{
    public function post(){
        return $this->belongsTo(Post::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function getImageAttribute($image){
        return $image ? url($image) : null;
    }
}
