<?php

namespace App\Models\DeviceToken;

use Illuminate\Database\Eloquent\Model;
use App\User;

class DeviceToken extends Model
{
    const PLATFORM_ANDROID = "android";
    const PLATFORM_IOS = "ios";
    const PLATFORM_WEB = "web";

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function getIosTokens(){
        return static::where("platform", static::PLATFORM_IOS)->pluck("token")->toArray();
    }

    public function getAndroidTokens(){
        return static::where("platform", static::PLATFORM_ANDROID)->pluck("token")->toArray();
    }

    public static function getByUserId($user_id){
        return static::where("user_id", $user_id)->pluck("token")->toArray();
    }

    public static function getByUsersId($Ids){
        return static::whereIn("user_id", $Ids)->pluck("token")->toArray();
    }

    public static function createOrUpdate($data){
        $device_token = static::where("token", $data['token'])->first();
        if (!$device_token) {
            $device_token = new DeviceToken;
            $device_token->token = $data['token'];
            if (isset($data['platform'])) {
                $device_token->platform = $data['platform'];
            }
        }
        $device_token->locale = app()->getLocale();
        if (authApi()->check()) {
            $device_token->user_id = authApi()->id();
        } else {
            $device_token->user_id = null;
        }
        if (isset($data["user_id"])) {
            $device_token->user_id = $data["user_id"];
        }
        $device_token->save();
    }
}
