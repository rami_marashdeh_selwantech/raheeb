<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Model;
use App\Support\Money;

class CategoryBoxSize extends Model
{
    public function getPriceAttribute($price){
        return Money::inDefaultCurrency($price);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function getImageAttribute($image){
        return $image ? url($image) : null;
    }
}
