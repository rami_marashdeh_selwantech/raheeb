<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Category extends Model implements TranslatableContract
{
    use Translatable;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    public $translatedAttributes = ['name'];

    public function boxsizes(){
    	return $this->hasMany(CategoryBoxSize::class);
    }

    public function getImageAttribute($image){
        return $image ? url($image) : null;
    }
}
