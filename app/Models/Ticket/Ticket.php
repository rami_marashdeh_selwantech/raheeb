<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Ticket extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function getImageAttribute($image){
        return $image ? url($image) : null;
    }
}
