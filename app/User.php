<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Spatie\Permission\Traits\HasRoles;
use App\Models\User\UserAddresse;
use App\Models\Post\Post;
use App\Models\Post\PostSoldRate;
use App\Models\Post\PostFaverate;
use App\Models\Post\PostOffer;
use App\Models\Post\PostSold;
use App\Support\Money;
use Illuminate\Database\Eloquent\Builder;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable , HasRoles;


    const TWITTER = "twitter";
    const RAHEEB = "raheeb";

    const ROLE_SUPER_ADMIN = "super-admin";
    const ROLE_ADMIN = "admin";
    const ROLE_USER = "user";


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function getJWTIdentifier(){
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(){
        return [];
    }

    public function getAvatarAttribute($avatar){
        return $avatar ? url($avatar) : null;
    }

    public function getIdImageAttribute($avatar){
        return $avatar ? url($avatar) : null;
    }

    public function getCoverImageAttribute($cover_image){
        return $cover_image ? url($cover_image) : null;
    }

    public function getBalanceAttribute($price){
        return Money::inDefaultCurrency($price);
    }

    public static function findByEmail($email, $role = null){
        if ($role) {
            return static::where('email', $email)->role($role)->first();
        }
        return static::where('email', $email)->first();
    }

    public static function findByPhone($phone){
        return static::where("phone_number", $phone)->first();
    }

    public function addresse(){
        return $this->hasOne(UserAddresse::class);
    }

    public function posts(){
        return $this->hasMany(Post::class);
    }

    public function postfaverates(){
        return $this->hasMany(PostFaverate::class);
    }

    public function postSoldRates(){
        return $this->hasMany(PostSoldRate::class);
    }

    public function faverates(){
        return $this->hasManyThrough(PostFaverate::class , Post::class);
    }

    public function postoffers(){
        return $this->hasMany(PostOffer::class);
    }

    public function postsolds(){
        return $this->hasMany(PostSold::class);
    }


    public function following() {
        return $this->belongsToMany(User::class, 'user_followers', 'follower_id', 'following_id');
    }

    public function followers() {
        return $this->belongsToMany(User::class, 'user_followers', 'following_id', 'follower_id');
    }

    public static function userFollow($user_id){
        $user = static::find(authApi()->id());
        $user->following()->attach($user_id);
    }

    public static function userUnFollow($user_id){
        $user = static::find(authApi()->id());
        $user->following()->detach($user_id);
    }

    public function hasFollow($user_id)
    {
        return $this->followers()->where("follower_id", authApi()->id())
            ->where("following_id" , $user_id)
            ->first() ? true : false;
    }

    public function countFollowers($user_id){
         return $this->followers()->where("following_id",$user_id)
            ->count();
    }

    public function countFollowing($user_id){
         return $this->following()->where("follower_id", $user_id)
            ->count();
    }

    public function getRate($user_id){
         return PostSoldRate::where("user_id" , "<>" , $user_id)
             ->whereHas("postSold" , function(Builder $query) use ($user_id){
                 $query->where(function($q) use ($user_id){
                     $q->where("user_id" , $user_id)
                        ->orwhereHas("post" , function(Builder $qq) use ($user_id){
                            $qq->where("user_id" , $user_id);
                        });
                 });
             })->avg("rate");
    }
}
