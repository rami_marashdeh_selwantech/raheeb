<?php

use App\Support\RTLDetector;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Support\Str;

function getDatesBetweenTowDates()
{
  $nowDate = \Carbon\Carbon::now()->format('Y-m-d');
  $period = \Carbon\CarbonPeriod::create(
    date("Y-m-d", strtotime('-2 month ' . $nowDate)),
    $nowDate
  );
  $dates = [];
  foreach ($period as $date) {
    $dates[] = $date->format('Y-m-d');
  }
  return $dates;
}

if (!function_exists('intl_number')) {
  /**
   * Returns the number in given locale..
   *
   * @param int $number
   * @param string|null $locale
   * @return string
   */
  function intl_number($number, $locale = null)
  {
    $locale = is_null($locale) ? locale() : $locale;

    $numberFormatter = new NumberFormatter($locale, NumberFormatter::DECIMAL);

    return $numberFormatter->format($number);
  }
}

if (!function_exists('locale')) {
  /**
   * Get current locale.
   *
   * @return string
   */
  function locale()
  {
    return app()->getLocale();
  }
}

if (!function_exists('is_rtl')) {
  /**
   * Determine if the given / current locale is RTL script.
   *
   * @param string|null $locale
   * @return bool
   */
  function is_rtl($locale = null)
  {
    return RTLDetector::detect($locale ?: locale());
  }
}

if (!function_exists('currency')) {
  /**
   * Get current currency.
   *
   * @return string
   */
  function currency()
  {


    $currency = Cookie::get('currency');

    if (!in_array($currency, setting('supported_currencies'))) {
      $currency = setting('default_currency');
    }

    return $currency;
  }
}



if (!function_exists('is_json')) {
  /**
   * Determine if the given string is valid json.
   *
   * @param string $string
   * @return bool
   */
  function is_json($string)
  {
    json_decode($string);

    return json_last_error() === JSON_ERROR_NONE;
  }
}





if (!function_exists("upImage")) {
  function upImage($image, $directory)
  {
    $base_path = "public/uploads/" . $directory;
    $filename = md5(strtotime(date("Y-m-d H:i:s")) . rand(1111, 9999)) . "." . $image->getClientOriginalExtension();
    $image->storePubliclyAs($base_path, $filename);
    return "storage/uploads/" . $directory . "/" . $filename;
  }
}



if (!function_exists("deleteImage")) {
  function deleteImage($path)
  {
    File::delete($path);
  }
}



if (!function_exists("phoneFormat")) {
  function phoneFormat($phoneNumber, $countriesIos = null)
  {
    if (!$countriesIos) {
      $countriesIos = setting("country_iso", "JO");
    }
    return Propaganistas\LaravelPhone\PhoneNumber::make($phoneNumber, $countriesIos);
  }
}

if (!function_exists('setting')) {

  /**
   * Get / set the specified setting value.
   *
   * If an array is passed, we'll assume you want to set settings.
   *
   * @param string|array $key
   * @param mixed $default
   * @return mixed|\Modules\Setting\Repository
   */
  function setting($key = null, $default = null)
  {
    if (is_null($key)) {
      return app('setting');
    }

    if (is_array($key)) {
      return app('setting')->set($key);
    }

    try {
      return app('setting')->get($key, $default);
    } catch (PDOException $e) {
      return $default;
    }
  }
}


if (!function_exists("generateVerifyCode")) {
  function generateVerifyCode()
  {
    return 1234;
    return rand(1111, 9999);
  }
}


if (!function_exists("authApi")) {
  function authApi()
  {
    return auth()->guard("api");
  }
}


function vincentyGreatCircleDistance(
  $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371)
{
  // convert from degrees to radians
  $latFrom = deg2rad($latitudeFrom);
  $lonFrom = deg2rad($longitudeFrom);
  $latTo = deg2rad($latitudeTo);
  $lonTo = deg2rad($longitudeTo);

  $lonDelta = $lonTo - $lonFrom;
  $a = pow(cos($latTo) * sin($lonDelta), 2) +
    pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
  $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

  $angle = atan2(sqrt($a), $b);
  return $angle * $earthRadius  ;
}
