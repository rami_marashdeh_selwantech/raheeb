<?php

namespace App\Support;

class PushNotify
{

  private $baseUrl = "https://fcm.googleapis.com/fcm/send";
  private $registration_ids_limit = 1000;

  // Notify Static Type
  const NEW_MESSAGE = "message";



  public function sendDataMessage($tokens, $data)
  {
    $fields = [];
    $fields["data"] = $data;
    return $this->send($tokens, $fields);
  }

  public function sendTopicDataMessage($topic, $data)
  {
    $fields = [];
    $fields["data"] = $data;
    return $this->send("/topics/" . $topic, $fields);
  }

  public function sendTopicNotification($topic, $notification, $data = null)
  {
    $fields = [];
    $fields["notification"] = $notification;
    $fields["notification"]["sound"] = "default";
    $fields["notification"]["vibrate"] = 1;

    if ($data != null) {
      $fields['data'] = $data;
    }
    return $this->send("/topics/" . $topic, $fields);
  }


  public function sendNotification($tokens, $notification, $data = null)
  {
    $fields = [];
    $fields["notification"] = $notification;
    $fields["notification"]["sound"] = "default";
    $fields["notification"]["vibrate"] = 1;

    if ($data != null) {
      $fields['data'] = $data;
    }
    return $this->send($tokens, $fields);
  }

  private function getHeaders()
  {
    return [
      'Authorization:key = ' . env("FIREBASE_SERVER_KEY"),
      'Content-Type: application/json'
    ];
  }

  public function arrTokensChunk($tokens)
  {
    return array_chunk($tokens, $this->registration_ids_limit);
  }

  private function send($tokens, $fields)
  {
    $fields["content_available"] = true;

    if (!is_array($tokens)) {
      $fields["to"] = $tokens;
    } else {
      $fields["registration_ids"] = $tokens;
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->baseUrl);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeaders());
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    if (!$result) {
      \Log::info("Error send notifyfExpiration :" . curl_error($ch));
    } else {
      \Log::info("resposne notify  :" . json_encode($result));
    }

    curl_close($ch);
    return $result;
  }
}
