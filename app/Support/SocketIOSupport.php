<?php

namespace App\Support;

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;

use ElephantIO\Exception\ServerConnectionFailureException;

class SocketIOSupport
{
  /**
   * Static Emit Keys
   */
  const SEND_MESSAGE = "send_message";

  private $host;
  private $port;
  private $urlConnect;
  private $client;


  public function __construct()
  {
    // ini_set("default_socket_timeout", -1);
    $this->host = env('SOCKET_HOST', '157.245.252.177');
    $this->port = env('SOCKET_PORT', '3000');
    $this->urlConnect = "{$this->host}:{$this->port}";
    $this->initialize();
  }


  private function initialize()
  {

    $this->client = new Client(
      new Version2X($this->urlConnect)
    );
    $this->client->initialize();
  }


  public function close()
  {
    $this->client->close();
  }

  public function emit($key, $data)
  {

    $this->client->emit($key, $data);
  }
}
