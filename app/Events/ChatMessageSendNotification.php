<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Models\DeviceToken\DeviceToken;
use App\Support\PushNotify;

class ChatMessageSendNotification
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $chatMessage;
    private $userReceiverId;
    private $senderInfo;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($chatMessage, $userReceiverId, $senderInfo)
    {
        $this->chatMessage = $chatMessage;
        $this->senderInfo = $senderInfo;
        $this->userReceiverId = $userReceiverId;
        $this->send();
    }

    private function send()
    {
        $notify = [
            "title" => $this->senderInfo->name,
            "type" => PushNotify::NEW_MESSAGE,
            "body" => $this->chatMessage->message_type
        ];
        $data = [
            "chat_message" => $this->chatMessage,
            "type_id" => $this->chatMessage->id
        ];
        $tokens = DeviceToken::getByUserId($this->userReceiverId);
        $pushNotify = new PushNotify;
        $pushNotify->sendNotification($tokens, $notify, $data);
    }
}
