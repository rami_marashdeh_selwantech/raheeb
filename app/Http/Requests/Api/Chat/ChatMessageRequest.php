<?php

namespace App\Http\Requests\Api\Chat;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Chat\ChatPost;

class ChatMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "message" => "required",
            "message_type" => "required|in:" . implode(",", [
                ChatPost::TYPE_FILE,
                ChatPost::TYPE_VOICE,
                ChatPost::TYPE_TEXT
            ])
        ];
    }
}
