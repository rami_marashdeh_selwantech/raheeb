<?php

namespace App\Http\Requests\Api\Auth;

use Illuminate\Foundation\Http\FormRequest;

class VerifyPhoneNumberRequest extends FormRequest
{
    public function __construct()
    {
        if (request()->phone_number) {
            $phone_number =  phoneFormat(request()->phone_number);
            request()->merge(["phone_number" => $phone_number]);
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_number' => "required|unique:users|phone:" . setting("country_iso"),
        ];
    }
}
