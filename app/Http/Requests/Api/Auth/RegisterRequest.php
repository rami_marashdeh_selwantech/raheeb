<?php

namespace App\Http\Requests\Api\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string|max:50",
            'phone_number' => "required|unique:users|phone:" . setting("country_iso"),
            "email" => "nullable|unique:users",
            "password" => "required|min:8|max:20|confirmed",
            "token" => "required"
        ];
    }
}
