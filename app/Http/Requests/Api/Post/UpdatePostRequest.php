<?php

namespace App\Http\Requests\Api\Post;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{

    public function __construct(){
        if (! request()->has("data")) {
            return ;
        }
        request()->merge(json_decode(request()->data , true));
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "category_id" => "required",
            "condition_id" => "required",
            "title" => "required|max:50|min:2",
            "description" => "required:max:250|min:10",
            "price" => "required",
            "lon" => "required",
            "lat" => "required",
            "is_ship_nationwide" => "required",
           // "category_box_size_id" => "required",

        ];
    }
}
