<?php

namespace App\Http\Requests\Api\Post;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Post\PostSold;

class BuyNowPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "payment_method" => "required|in:" . implode(",", [
                PostSold::SHIPPING_CACH,
                PostSold::SHIPPING_ONLINE
            ])
        ];
    }
}
