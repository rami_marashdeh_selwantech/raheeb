<?php

namespace App\Http\Controllers\Api\Post;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post\Post;
use App\Http\Resources\Post\PostResource;

class PostController extends Controller
{
    public function getPost(Request $request){

        if ($request->has("data")) {
            $request->merge(json_decode($request->data , true));
        }

    	$posts = Post::where(function($q) use ($request){
	    		if ($request->category_id) {
	    			$q->where("category_id" , $request->category_id);
	    		}
    		})
    		->where(function($q) use ($request){
    			if ($request->has("pick_up")) {
    				$q->where("is_ship_nationwide" , 0);
    			}
    		})
    		->where(function($q) use ($request){
    			if ($request->has("shipping")) {
    				$q->where("is_ship_nationwide" , 1);
    			}
    		})
    		->where(function($q) use ($request){
    			if ($request->price_min && $request->price_max) {
    				$q->where("price" , '>' , $request->price_min)
    					->where("price" , '<=' , $request->price_max);
    			}
    		})
    		->where(function($q) use ($request){
    			if ($request->title) {
    				$q->where("title"  , "LIKE", "%$request->title%");
    			}
    		})
    		->where(function($q){
                if(authApi()->check()){
                    $q->where("user_id","<>",authApi()->id());
                }
            })
            ->where(function($q) use ($request){
            	if ($request->has("ordering")) {
            		if ($request->ordering == "newest") {
            			$q->orderBy("created_at", "DESC");
            		}
            		if ($request->ordering == "price_heigh") {
            			$q->orderBy("price", "DESC");
            		}
            		if ($request->ordering == "price_low") {
            			$q->orderBy("price", "ASC");
            		}
            	}
            })
            ->where(function($q) use ($request){
            	if ($request->lat && $request->lon && $request->distance) {
            		$post = Post::where("status" , Post::STATUS_AVAILABLE)->get();
            		$post_distance_id = [];
            		foreach ($post as $p) {
            			$dis = vincentyGreatCircleDistance(
	            			$request->lat , $request->lon , $p->lat , $p->lon
	            		);
	            		if ($dis < $request->distance) {
	            			$post_distance_id[] = $p->id;
	            		}
            		}
            		$q->whereIn("id" , $post_distance_id);
            	}
            })
            ->where("status" , Post::STATUS_AVAILABLE)
            ->skip($request->get("skip", 0))
            ->take($request->get("takeItems",$this->takeItems))
    		->get();

    	return $this->successResponse(PostResource::collection($posts));
    }

    public function show(Post $post){
        return $this->successResponse(new PostResource($post));
    }
}
