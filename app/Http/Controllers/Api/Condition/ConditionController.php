<?php

namespace App\Http\Controllers\Api\Condition;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Condition\Condition;
use App\Http\Resources\Condition\ConditionPostResource;

class ConditionController extends Controller
{
    public function show(){
    	return $this->successResponse(
    		ConditionPostResource::collection(
    			Condition::where("is_show" , 1)
	    			->orderBy("ordering", "ASC")
	    			->get()
    		)
    	);
    }
}
