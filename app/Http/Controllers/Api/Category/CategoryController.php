<?php

namespace App\Http\Controllers\Api\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\Category\CategoryBoxSizeResource;
use App\Models\Category\Category;
use App\Models\Category\CategoryBoxSize;

class CategoryController extends Controller
{
    public function all(){
    	return $this->successResponse(
    		CategoryResource::collection(
    			Category::where("is_valid" , 1)
    			->orderBy("ordering", "ASC")
    			->get()
    		)
    	);
    }

    public function categoryBoxSize(Category $category){
    	return $this->successResponse(
    		CategoryBoxSizeResource::collection(
    			CategoryBoxSize::where("category_id" , $category->id)
    			->orderBy("size", "ASC")
    			->get()
    		)
    	);
    }

}
