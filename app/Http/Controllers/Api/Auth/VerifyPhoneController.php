<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\Auth\CheckCodeRequest;
use App\Http\Requests\Api\Auth\ResetCodeRequest;
use App\Http\Requests\Api\Auth\VerifyPhoneNumberRequest;
use App\Models\PhoneVerification\PhoneVerification;
use App\Events\SendVerifyCodeEvent;

class VerifyPhoneController extends Controller
{
    public function checkCode(CheckCodeRequest $request)
    {
        if (PhoneVerification::checkCode($request->token, $request->code)) {
            return $this->successResponse(null, __("app.success"), 200);
        } else {
            return $this->errorResponse(__("app.invalid_code"), 422);
        }
    }

    public function verifyPhoneNumber(VerifyPhoneNumberRequest $request)
    {
        $phone_number =  phoneFormat($request->phone_number);

        $phoneVerify = PhoneVerification::addOrUpdate($phone_number);
        event(new SendVerifyCodeEvent($phone_number, $phoneVerify->code));
        return $this->successResponse([
            "token" => $phoneVerify->token
        ]);
    }

    public function resetCode(ResetCodeRequest $request)
    {
        $phoneVerification = PhoneVerification::getByToken($request->token);
        if ($phoneVerification) {
            $phoneVerification = PhoneVerification::addOrUpdate($phoneVerification->phone_number);
            event(new SendVerifyCodeEvent($phoneVerification->phone_number, $phoneVerification->code));
            return $this->successResponse(["token" => $phoneVerification->token], null);
        } else {
            return $this->errorResponse(__("app.invalid_token"), 404);
        }
    }
}
