<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\DeviceToken\DeviceToken;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Resources\User\UserInfoRecource;

class LoginController extends Controller
{
    public function login(LoginRequest $request)
    {
        $phone_number = phoneFormat($request->phone_number);
        $credentials = ["phone_number" => $phone_number, "password" => $request->password];

        try {
            if (!$token = authApi()->attempt($credentials)) {
                return $this->errorResponse(__("app.invalid_credentials"), 401);
            }
            if ($token && !authApi()->user()->hasRole(User::ROLE_USER)) {
                authApi()->logout();
                return $this->errorResponse(__('app.invalid_credentials'), 401);
            }
            if ($token && authApi()->user()->is_blocked) {
                authApi()->logout();
                return $this->errorResponse(__('app.account_not_activated'));
            }
        } catch (JWTException $ex) {
            return $this->errorResponse(__("app.internal_error"), 500);
        }

        $user = authApi()->user();
        if ($request->firebase_token && $request->platform) {
            DeviceToken::createOrUpdate([
                "token" => $request->firebase_token,
                "platform" => $request->platform,
                "user_id" => $user->id
            ]);
        }
        $user = new UserInfoRecource($user);
        
        return $this->successResponse([
            "user" => $user ,
            "token" => $token
        ]);
    }
}
