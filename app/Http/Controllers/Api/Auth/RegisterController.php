<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\PhoneVerification\PhoneVerification;
use App\Models\DeviceToken\DeviceToken;
use App\User;
use App\Http\Resources\User\UserInfoRecource;
use App\Http\Requests\Api\Auth\RegisterRequest;

class RegisterController extends Controller
{
    public function register(RegisterRequest $request)
    {
        $phoneVerification = PhoneVerification::getByToken($request->token);
        if (!$phoneVerification || !$phoneVerification->is_verify) {
            return $this->errorResponse(__('app.phone_number_is_not_verify'));
        }
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone_number = $phoneVerification->phone_number;
        $user->password = bcrypt($request->password);
        $user->is_valid = 0;
        $user->login_with = User::RAHEEB;
        $user->send_email = $request->email ? true : false;
        $user->save();

        $phoneVerification->delete();
        $user->assignRole(User::ROLE_USER);
        $user = User::find($user->id);
        $token = JWTAuth::fromUser($user);
        $user = new UserInfoRecource($user);
        if ($request->firebase_token && $request->platform) {
            DeviceToken::createOrUpdate([
                "token" => $request->firebase_token,
                "platform" => $request->platform,
                "user_id" => $user->id
            ]);
        }


        return $this->successResponse([
            "user" => $user ,
            "token" => $token
        ]);
    }
}
