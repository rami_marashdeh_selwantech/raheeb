<?php

namespace App\Http\Controllers\Api\Auth;

use App\Events\SendVerifyCodeEvent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\Auth\UpdatePasswordRequest;
use App\Http\Requests\Api\Auth\VerifyPhoneNumberRequest;
use App\Models\PhoneVerification\PhoneVerification;
use App\User;

class ForgotPasswordController extends Controller
{
    public function verifyPhoneNumber(Request $request)
    {

        if (request()->phone_number) {
            $phone_number =  phoneFormat(request()->phone_number);
            request()->merge(["phone_number" => $phone_number]);
        }

        $request->validate([
            'phone_number' => "required|phone:" . setting("country_iso"),
        ]);
        $phone_number =  phoneFormat($request->phone_number);
        $user = User::findByPhone($phone_number);
        if (!$user) {
            return $this->errorResponse(__("app.user_is_not_exist"));
        }
        $phoneVerify = PhoneVerification::addOrUpdate($phone_number);
        event(new SendVerifyCodeEvent($phone_number, $phoneVerify->code));
        return $this->successResponse([
            "token" => $phoneVerify->token
        ]);
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            "token" => "required",
            "password" => "required|min:8|max:20|confirmed"
        ]);
        $phoneVerification = PhoneVerification::getByToken($request->token);
        if (!$phoneVerification || !$phoneVerification->is_verify) {
            return $this->errorResponse(__('app.phone_number_is_not_verify'));
        }
        $user = User::findByPhone($phoneVerification->phone_number);
        $user->password = bcrypt($request->password);
        $user->save();
        $phoneVerification->delete();
        return $this->successResponse(null, __("app.success_updated_password"));
    }
}
