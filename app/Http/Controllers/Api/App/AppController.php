<?php

namespace App\Http\Controllers\Api\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Distance\Distance;
use  App\Http\Resources\App\DistanceResource;

class AppController extends Controller
{
    public function privacyPolicy(){
        return $this->successResponse(setting("privacy_policy"));
    }

    public function aboutUs(){
        return $this->successResponse(setting("about_us"));
    }

    public function distance(){
        return $this->successResponse(
        	DistanceResource::collection(
                    Distance::where("is_show" , 1)->orderBy("distance", "ASC")->get()
                )
        );
    }

    public function currency(){
        return $this->successResponse(setting("default_currency"));
    }
}
