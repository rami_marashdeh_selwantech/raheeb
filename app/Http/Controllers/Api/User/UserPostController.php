<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\Post\PostImageRequest;
use App\Http\Requests\Api\Post\PostRequest;
use App\Http\Requests\Api\Post\UpdatePostRequest;
use App\Models\Post\Post;
use App\Models\Post\PostImage;
use App\Models\Post\PostReport;
use App\Models\Chat\ChatPost;
use App\Models\Category\Category;
use DB;
use Carbon\Carbon;
use App\Http\Resources\Post\PostResource;
use App\Http\Resources\Post\PostSellingResource;
use App\Http\Resources\User\UserMarkSoldResource;

class UserPostController extends Controller
{
    public function getUserPost(Request $request){

        return $this->successResponse(
            PostSellingResource::collection(
                Post::where("user_id" , authApi()->id())
                    ->skip($request->get("skip", 0))
                    ->take($request->get("takeItems",$this->takeItems))
                    ->get()
            )
        );
    }

    public function getUserPostBuying(Request $request){

        return $this->successResponse(
            PostResource::collection(
                Post::whereHas("postSold" , function($q){
                        $q->where("user_id" , authApi()->id());
                    })
                    ->skip($request->get("skip", 0))
                    ->take($request->get("takeItems",$this->takeItems))
                    ->get()
            )
        );
    }

    public function getUserMarkSold(Post $post){
        $chat = ChatPost::where("post_id" , $post->id)->get();
        return $this->successResponse(
                UserMarkSoldResource::collection($chat)
            );
    }

    public function image(PostImageRequest $request){
    	if (sizeof($request->images) > 10) {
    		return $this->errorResponse(__("app.max_upload_image"));
    	}
    	$images = [];
        foreach ($request->images as $image) {
            $images[] =  upImage($image, "post");
        }
        return $this->successResponse([
            "paths" => $images,
        ]);
    }

    public function deleteImage(PostImage $image){
        $image->delete();
        return $this->errorResponse(__("app.succes_delete_image"));
    }

    public function post(PostRequest $request){

    	$user_post = Post::where('created_at', '>=', Carbon::now()->subDay())
            ->where("user_id" , authApi()->id())
            ->count();
    	if ($user_post > setting("user_line_time")) {
    	 	return $this->errorResponse(__("app.user_limet_max_post"));
    	 }

         if (sizeof($request->images) > 10) {
     		return $this->errorResponse(__("app.max_upload_image"));
     	}

    	if ($this->checkCategoryShipping($request->category_id , $request->is_ship_nationwide)) {
    		return $this->errorResponse(__("app.category_not_ship_nationwide"));
    	}
    	DB::beginTransaction();
        try {

	    	$post = new Post;
            $is_ship = $request->is_ship_nationwide == 0 ? null : $request->get("category_box_size_id", null);
            $post = $this->saveDataPost($post , $request->all() , $is_ship);
            $images = [];
            foreach ($request->images as $index => $image) {
                list($width, $height, $type, $attr) = getimagesize($image);
                $images[$index]["image"] =  upImage($image, "post");
                $images[$index]["height"] = $height;
                $images[$index]["width"] = $width;
            }
	    	$this->saveImagePath($images , $post);

	    	DB::commit();
	    }catch (QueryException $error) {
	        DB::rollBack();
	        return $this->errorResponse($error);
	    }
        return $this->successResponse(__("app.succes_submit_post"));
    }

    public function updatePost(UpdatePostRequest $request , Post $post){

        if ($this->checkCategoryShipping($request->category_id , $request->is_ship_nationwide)) {
            return $this->errorResponse(__("app.category_not_ship_nationwide"));
        }

        DB::beginTransaction();
        try {

            $is_ship = $request->is_ship_nationwide == 0 ? null : $request->get("category_box_size_id", null);
            $post = $this->saveDataPost($post , $request->all() , $is_ship);
            if ($request->has("images")) {
                if (sizeof($request->images) > 10) {
            		return $this->errorResponse(__("app.max_upload_image"));
            	}
                $images = [];
                foreach ($request->images as $index => $image) {
                    list($width, $height, $type, $attr) = getimagesize($image);
                    $images[$index]["image"] =  upImage($image, "post");
                    $images[$index]["height"] = $height;
                    $images[$index]["width"] = $width;
                }
    	    	$this->saveImagePath($images , $post);
            }

            DB::commit();
        }catch (QueryException $error) {
            DB::rollBack();
            return $this->errorResponse($error);
        }
        return $this->successResponse(__("app.succes_update_post"));
    }

    private function saveDataPost($post , $request , $is_ship){

        $post->user_id = authApi()->id();
        $post->category_id = $request['category_id'];
        $post->condition_id = $request['condition_id'];
        $post->status = Post::STATUS_AVAILABLE;
        $post->title = $request['title'];
        $post->description = $request['description'];
        $post->price = $request['price'];
        $post->lon = $request['lon'];
        $post->lat = $request['lat'];
        $post->is_ship_nationwide = $request['is_ship_nationwide'];
        $post->category_box_size_id = $is_ship;
        $post->save();

        return $post;
    }

    private function saveImagePath($paths , $post){

        foreach ($paths as $index => $path) {
                $image = new PostImage;
                $image->post_id = $post->id;
                $image->image = $path["image"];
                $image->height = $path["height"];
                $image->width = $path['width'];
                $image->save();
            }
    }

    private function checkCategoryShipping($category_id , $is_ship_nationwide){
        $category = Category::find($category_id);
        return ( ( $category->is_ship_nationwide == 0 ) && ( $is_ship_nationwide == 1 ) ) ;
    }

    public function report(Request $request , Post $post){
        $report = new PostReport;
        $report->post_id = $post->id;
        $report->user_id = authApi()->id();
        $report->message = $request->message;
        if ($request->has("image")) {
            $report->image = upImage($request->image, "post/report");
        }
        $report->save();
        return $this->successResponse(__("app.success_report"), "success");
    }

}
