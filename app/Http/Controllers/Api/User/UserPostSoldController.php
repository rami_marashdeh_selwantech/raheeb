<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post\Post;
use App\Models\Post\PostSold;
use App\Models\Post\PostSoldRate;
use App\Models\Post\PostSoldAddress;
use App\Http\Resources\Post\CalculatePostResource;
use App\Http\Requests\Api\Post\BuyNowPostRequest;
use App\Http\Requests\Api\Post\SetSoldRequest;
use App\User;
use DB;
use App\Http\Controllers\Notification\NotificationController;

class UserPostSoldController extends Controller
{
    public function calculatePostPrice(Post $post){

    	return $this->successResponse(
    		new CalculatePostResource($post)
    	);
    }

    public function buyNowPost(BuyNowPostRequest $request , Post $post){
    	if ($post->status == Post::STATUS_SOLD || $post->status == Post::STATUS_SOLD_OTHER_APP) {
    		return $this->errorResponse(__("app.error_already_sold"));
    	}

    	if ($post->is_ship_nationwide == 1 && !$request->has("address")) {
    		return $this->errorResponse(__("app.not_found_address"));
    	}

    	DB::beginTransaction();
        try {

        	$post->status = Post::STATUS_SOLD;
        	$post->save();

        	$post_sold = new PostSold;
        	$post_sold->post_id = $post->id;
        	$post_sold->user_id = authApi()->id();
        	$post_sold->payment_method = $request->payment_method;
        	$post_sold->post_offer_id = $post->isOffer() ? $post->getOfferPost() : null ;
        	$post_sold->total_price = $post->getCalculatePostPrice()->amount();
        	$post_sold->save();

        	if ($request->has("address")) {
        		$post_sold_address = new PostSoldAddress;
        		$post_sold_address->post_sold_id = $post_sold->id;
        		$post_sold_address->building_number = $request->address['building_number'];
        		$post_sold_address->name = $request->address['name'];
        		$post_sold_address->floor = $request->address['floor'];
        		$post_sold_address->lat = $request->address['lat'];
        		$post_sold_address->lon = $request->address['lon'];
        		$post_sold_address->save();
        	}

        	DB::commit();
        }catch (QueryException $error) {
	        DB::rollBack();
	        return $this->errorResponse($error);
	    }
	    return $this->successResponse(__("app.succes_buy_post"));
    }

    public function setSoldPost( SetSoldRequest $request , Post $post){

    	if ($post->status == Post::STATUS_SOLD || $post->status == Post::STATUS_SOLD_OTHER_APP) {
    		return $this->errorResponse(__("app.error_already_sold"));
    	}

    	if ($request->has("user_id")) {
    		if (! $request->has("rate")) {
    			return $this->errorResponse(__("app.not_found_rate_key"));
    		}
    		$user = User::find($request->user_id);
	    	if (! $user) {
	    		return $this->errorResponse(__("app.not_found_user"));
	    	}
    	}else{
    		$post->status = Post::STATUS_SOLD_OTHER_APP;
        	$post->save();
        	return $this->successResponse(__("app.succes_set_sold"));
    	}

    	DB::beginTransaction();
        try {

        	$post->status = Post::STATUS_SOLD;
        	$post->save();

        	$post_sold = new PostSold;
        	$post_sold->post_id = $post->id;
        	$post_sold->user_id = $request->get("user_id" , null);
        	$post_sold->shipping_method = PostSold::SHIPPING_CACH;
        	$post_sold->post_offer_id = null ;
        	$post_sold->total_price = $request->get("price" , null);
        	$post_sold->save();

        	$post_sold_rate = new PostSoldRate;
        	$post_sold_rate->post_sold_id = $post_sold->id;
        	$post_sold_rate->user_id = authApi()->id();
        	$post_sold_rate->rate = $request->rate;
        	$post_sold_rate->commint = $request->get("commint" , null);
        	$post_sold_rate->save();

            $notification = new NotificationController();
            $notification->sendNotificationsRate($user,$post);

        	DB::commit();
        }catch (QueryException $error) {
	        DB::rollBack();
	        return $this->errorResponse($error);
	    }
	    return $this->successResponse(__("app.succes_set_sold"));

    }

    public function setRatePostSold(PostSold $post_sold, Request $request){

    	$request->validate([
            "rate" => "required",
        ]);

        if (PostSoldRate::isUserRate($post_sold->id)) {
        	return $this->errorResponse(__("app.user_already_rate_this"));
        }

       	$post_sold_rate = new PostSoldRate;
        $post_sold_rate->post_sold_id = $post_sold->id;
        $post_sold_rate->user_id = authApi()->id();
        $post_sold_rate->rate = $request->rate;
        $post_sold_rate->commint = $request->get("commint" , null);
        $post_sold_rate->save();

        return $this->successResponse(__("app.succes_rate"));
    }
}
