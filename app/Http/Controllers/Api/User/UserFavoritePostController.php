<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post\Post;
use App\Models\Post\PostFaverate;
use App\Http\Resources\Post\PostResource;

class UserFavoritePostController extends Controller
{
    public function faverate(Post $post){

    	$faveratePost = new PostFaverate;
    	$faveratePost->post_id = $post->id;
    	$faveratePost->user_id = authApi()->id();
    	$faveratePost->save();

    	return $this->successResponse(__("app.success_faverate"));
    }

    public function unFaverate(Post $post){

    	$unFaverate = PostFaverate::where("post_id" , $post->id)
    		->where("user_id" , authApi()->id())
    		->delete();

    	return $this->successResponse(__("app.success_un_faverate"));
    }

    public function userFaverate(){

        return $this->successResponse(PostResource::collection(
            Post::whereHas('faverates', function ($query){
                $query->where('user_id', '=',authApi()->id());
            })->get()
        ));
    }
}
