<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User\Notification;
use App\Http\Resources\Notification\NotificationResource;

class UserNotificationController extends Controller
{
    public function getNotifications(){

    	return $this->successResponse(
    		NotificationResource::collection(
    			Notification::where("user_id" , authApi()->id())
    				->get()
    		)
    	);
    }
}
