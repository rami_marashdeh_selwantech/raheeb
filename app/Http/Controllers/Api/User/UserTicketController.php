<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\User\UserTicketRequest;
use App\Models\Ticket\Ticket;

class UserTicketController extends Controller
{
     public function ticket(UserTicketRequest $request){
         $ticket = new Ticket;
         $ticket->user_id = authApi()->id();
         $ticket->message = $request->message;
         if ($request->has("image")) {
             $ticket->image = upImage($request->image, "ticket");
         }
         $ticket->save();
         return $this->successResponse( __("app.success_ticket") );
     }
}
