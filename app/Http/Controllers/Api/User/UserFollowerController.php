<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\Api\User\UserFollowerRequest;
use App\Http\Resources\User\UserInfoRecource;
use App\Http\Resources\User\UsersDataResource;
use App\Http\Controllers\Notification\NotificationController;

class UserFollowerController extends Controller
{
    public function follow(UserFollowerRequest $request){

    	$user = User::find($request->user_id);
    	if (! $user) {
    		return $this->errorResponse(__("app.not_found_user_id"));
    	}
    	if ($user->hasFollow($user->id)){
    		return $this->errorResponse(__("app.error_alrady_following"));
    	}
    	User::userFollow($user->id);

        $notification = new NotificationController();
        $notification->sendNotificationsFollowing($user);

    	return $this->successResponse(__("app.success_follow"));
    }

    public function unFollow(UserFollowerRequest $request){

    	$user = User::find($request->user_id);
    	if (! $user) {
    		return $this->errorResponse(__("app.not_found_user_id"));
    	}
    	if (! $user->hasFollow($user->id)){
    		return $this->errorResponse(__("app.error_alrady_un_following"));
    	}
    	User::userUnFollow($user->id);
    	return $this->successResponse(__("app.success_un_follow"));
    }

    public function getFollowers(User $user = null){
        if ($user) {
            $user = User::find($user->id);
        }else{
            $user = User::find(authApi()->id());
        }
        return $this->successResponse(
            UsersDataResource::collection($user->followers()->get())
        );
    }

    public function getFollowing(User $user = null){
        if ($user) {
            $user = User::find($user->id);
        }else{
            $user = User::find(authApi()->id());
        }
        return $this->successResponse(
            UsersDataResource::collection($user->following()->get())
        );
    }


}
