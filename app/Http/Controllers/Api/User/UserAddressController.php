<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\User\UserAddressRequest;
use App\Http\Requests\Api\User\UserDistanceRequest;
use App\Models\User\UserAddresse;

class UserAddressController extends Controller
{
    public function show(){
        $addresse =authApi()->user()->addresse;
        return $this->successResponse($addresse , $addresse ? null : __("app.not_found_address"));
    }

    public function store(UserAddressRequest $request){
    	UserAddresse::createOrUpdate($request->all());
    	$user = authApi()->user();

    	return $this->successResponse($user->addresse, __("app.success_save_address"));
    }

    public function storeDistance(UserDistanceRequest $request){
    	UserAddresse::distanceCreateOrUpdate($request->all());
    	$user = authApi()->user();

    	return $this->successResponse($user->addresse, __("app.success_save_distance"));
    }
}
