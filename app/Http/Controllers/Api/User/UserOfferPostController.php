<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post\PostOffer;
use App\Models\Post\Post;
use App\Http\Requests\Api\Post\OfferPostRequest;
use App\Models\Chat\ChatPost;
use App\Support\SocketIOSupport;
use App\Events\ChatMessageSendNotification;
use App\Http\Resources\Chat\UserMessagesResource;
use App\Http\Resources\Post\PostResource;
use App\User;
use Illuminate\Database\Eloquent\Builder;

class UserOfferPostController extends Controller
{
    public function setOffer(OfferPostRequest $request , Post $post){

    	$offer = new PostOffer;
    	$offer->post_id = $post->id;
    	$offer->user_id = authApi()->id();
    	$offer->status = PostOffer::STATUS_WAITING;
    	$offer->price = $request->price;
    	$offer->save();

    	$chatMessage = ChatPost::createChat($post->id , [
    		"offer_id" => $offer->id,
    		"message" => $offer->price
    	]);


        $userReceiverId = $post->user_id ;

        $chat  = new UserMessagesResource($chatMessage);

        $socket = new SocketIOSupport;
        $socket->emit(SocketIOSupport::SEND_MESSAGE, [
            "chat" => $chat
        ]);

        $socket->close();

        event(new ChatMessageSendNotification($chatMessage, $userReceiverId, authApi()->user()));

    	return $this->successResponse(__("app.success_send_offer"), __("app.success_send_offer"));
    }

    public function acceptOffer(PostOffer $post_offer){
        if ($post_offer->status == PostOffer::STATUS_APPROVED) {
            return $this->errorResponse(__("app.already_accepted"));
        }
        $post_offer->status = PostOffer::STATUS_APPROVED;
        $post_offer->save();
        $chatMessage = ChatPost::createChat($post_offer->post_id , [
    		"offer_id" => $post_offer->id,
    		"message" => ( __("app.accept_your_offer") . $post_offer->price)
    	]);

        $userReceiverId = $post_offer->user_id;

        $chat  = new UserMessagesResource($chatMessage);

        $socket = new SocketIOSupport;
        $socket->emit(SocketIOSupport::SEND_MESSAGE, [
            "chat" => $chat
        ]);

        $socket->close();

        event(new ChatMessageSendNotification($chatMessage, $userReceiverId, authApi()->user()));

    	return $this->successResponse( __("app.success_accept_offer"), __("app.success_accept_offer"));

    }

    public function userOfferPost(Request $request , User $user){
        return $this->successResponse(
            PostResource::collection(
                Post::where("user_id" , $user->id)
                    ->whereHas('postoffers', function (Builder $query){
                        $query->where("user_id" , authApi()->id());
                    })
                    ->where("status" , Post::STATUS_AVAILABLE)
                    ->skip($request->get("skip", 0))
                    ->take($request->get("takeItems",$this->takeItems))
                    ->get()
            )
        );
    }
}
