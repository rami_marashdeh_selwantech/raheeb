<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Chat\ChatPost;
use App\Http\Resources\Chat\UserChatResource;
use App\Http\Resources\Chat\UserMessagesResource;
use App\Http\Requests\Api\Chat\ChatMessageRequest;
use App\Events\ChatMessageSendNotification;
use App\Support\SocketIOSupport;
use App\Models\Post\Post;
use DB;

class UserMessageController extends Controller
{
    public function userChats(Request $request){

    	return $this->successResponse(UserChatResource::collection(
    		ChatPost::whereHas("post",function($q) {
    			$q->where("user_id" , authApi()->id());
    		})
    		->orwhere("user_id" , authApi()->id())
            ->selectRaw("chat_posts.*, (SELECT MAX(created_at) from chat_post_messages WHERE chat_post_messages.chat_post_id=chat_posts.id) as latest_message_on")
            ->orderBy("latest_message_on", "DESC")
            ->skip($request->get("skip", 0))
            ->take($request->get("takeItems",$this->takeItems))
            ->get()
    	));
    }

    public function userChatMessages(Request $request , ChatPost $chat){

    	return $this->successResponse(UserMessagesResource::collection(
    		$chat->messages()
    			->skip($request->get("skip", 0))
    			->take($request->get("takeItems",$this->takeItems))
    			->latest()
    			->get()
    			->reverse()
    	));
    }

    public function createChate(Post $post){
        $chat_post = ChatPost::where("user_id" , authApi()->id())
            ->where("post_id" , $post->id)
            ->first();
        if (! $chat_post) {
            $chat_post = new ChatPost;
            $chat_post->user_id = authApi()->id();
            $chat_post->post_id = $post->id;
            $chat_post->save();
        }
        return $this->successResponse(new UserChatResource($chat_post), __("app.success_created_chat"));
    }

    public function getChatFromID(ChatPost $chat_post){
        return $this->successResponse(new UserChatResource($chat_post));
    }

    public function sendMessage(ChatMessageRequest $request , ChatPost $chat){


        if ($request->message_type == ChatPost::TYPE_FILE) {
            if ($request->hasFile("message")) {
                $message = upImage($request->file("message"), "chats/files" . $chat->id . "/");
            } else {
                return $this->errorResponse("message_file_not_found");
            }
        }elseif ($request->message_type == ChatPost::TYPE_VOICE) {
            if ($request->hasFile("message")) {
                $message = upImage($request->file("message"), "chats/voices" . $chat->id . "/");
            } else {
                return $this->errorResponse("message_voice_not_found");
            }
        }

        DB::beginTransaction();
        try {
            $chatMessage = ChatPost::createChat($chat->post_id , [
                "message" => $request->message_type == ChatPost::TYPE_VOICE || $request->message_type == ChatPost::TYPE_FILE  ? $message : $request->message ,
                "message_type" => $request->message_type
            ] , $chat->id);

            $userReceiverId = $chat->post->user_id == authApi()->id() ? $chat->user_id : $chat->post->user_id ;

            $chat  = new UserMessagesResource($chatMessage);

            $socket = new SocketIOSupport;
            $socket->emit(SocketIOSupport::SEND_MESSAGE, [
                "chat" => $chat
            ]);

            $socket->close();

            event(new ChatMessageSendNotification($chatMessage, $userReceiverId, authApi()->user()));
            DB::commit();
        }catch (QueryException $error) {
            DB::rollBack();
            return $this->errorResponse($error);
        }

        return $this->successResponse($chat, __("app.success_send_message"));

    }
}
