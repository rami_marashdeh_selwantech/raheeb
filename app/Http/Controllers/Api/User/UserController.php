<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\User\UserInfoRecource;
use App\Http\Resources\User\UsersDataResource;
use App\Models\DeviceToken\DeviceToken;
use App\Http\Requests\Api\User\LogoutRequest;
use App\Http\Requests\Api\User\UserAvatarRequest;
use App\Http\Requests\Api\User\UserCoverImageRequest;
use App\Http\Requests\Api\User\UpdatePasswordRequest;
use App\Http\Requests\Api\User\ValidationProfileRequest;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\User\UserReport;
use App\Models\PhoneVerification\PhoneVerification;
use App\Http\Requests\Api\Auth\CheckCodeRequest;

class UserController extends Controller
{
    public function profile(){
    	return $this->successResponse(
            new UserInfoRecource(authApi()->user())
        );
    }

    public function check_send_email(){
        $user = User::find(authApi()->id());
        if($user->send_email){
            $user->send_email = false;
        }else{
            $user->send_email = true;
        }
        $user->save();
    	return $this->successResponse(new UserInfoRecource($user));
    }

    public function check_send_notification(){
        $user = User::find(authApi()->id());
        if($user->send_notifications){
            $user->send_notifications = false;
        }else{
            $user->send_notifications = true;
        }
        $user->save();
    	return $this->successResponse(new UserInfoRecource($user));
    }

    public function userProfile(User $user){
        return $this->successResponse(
            new UsersDataResource($user)
        );
    }

    public function updateEmail(Request $request){
        $request->validate([
            "email" => "required|email|unique:users,email," . authApi()->id()
        ]);

        $user = authApi()->user();
        $user->email = $request->email;
        $user->save();
        return $this->successResponse(
            new UserInfoRecource($user)
        , __("app.success_updated_email_user"));
    }

    public function updatePhoneNumber(CheckCodeRequest $request){

        if (PhoneVerification::checkCode($request->token, $request->code)) {
            $phoneVerification = PhoneVerification::getByToken($request->token);

            $user = authApi()->user();
            $user->phone_number = $phoneVerification->phone_number;
            $user->save();
            $phoneVerification->delete();
            return $this->successResponse(
                new UserInfoRecource($user)
            , __("app.success_update_phone_number"));
        } else {
            return $this->errorResponse(__("app.invalid_code"), 422);
        }

    }

    public function validation(ValidationProfileRequest $request){
        $user = authApi()->user();
        $user->id_image = upImage($request->image, "users/id_image");
        $user->save();
        return $this->successResponse(
            new UserInfoRecource($user)
        , __("app.success_upload_image_waite_admin_approved_this"));
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $user = authApi()->user();
        if (!Hash::check($request->current_password, $user->password)) {
            return $this->errorResponse(__("app.current_password_not_valid"));
        }
        $user->password = bcrypt($request->password);
        $user->save();
        return $this->successResponse(null, __("app.success_updated_password"));
    }

    public function updateAvatar(UserAvatarRequest $request)
    {
        $user = authApi()->user();
        $user->avatar = upImage($request->avatar, "users/avatar");
        $user->save();
        return $this->successResponse(
            new UserInfoRecource($user)
        , __("app.success_updated_avatar"));
    }

    public function updateCoverImage(UserCoverImageRequest $request)
    {
        $user = authApi()->user();
        $user->cover_image = upImage($request->cover_image, "users/cover_image");
        $user->save();
        return $this->successResponse(
            new UserInfoRecource($user)
        , __("app.success_updated_cover_image"));
    }

    public function logout(LogoutRequest $request){
        authApi()->logout();
        DeviceToken::createOrUpdate([
            "token" => $request->token,
        ]);
        return $this->successResponse(null, "success");
    }

    public function report(Request $request , User $user){
        $report = new UserReport;
        $report->report_id = $user->id;
        $report->user_id = authApi()->id();
        $report->message = $request->message;
        if ($request->has("image")) {
            $report->image = upImage($request->image, "users/report");
        }
        $report->save();
        return $this->successResponse(__("app.success_report"), "success");
    }
}
