<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Support\PushNotify;
use App\Models\DeviceToken\DeviceToken;
use App\Models\User\Notification;

class NotificationController extends Controller
{
    public function sendNotificationsFollowing($user){
        $pushNotify = new PushNotify;
        $tokens = DeviceToken::getByUserId($user->id);

        $notify = [
            "title" => __("notify.following"),
            "body" => __("notify.user_following_your") . $user->name
        ];
                
        $notifyData = [
            "type" => Notification::TYPE_FOLLOWING,
            "acction_id" => $user->id,
        ];
        $result = $pushNotify->sendNotification($tokens, $notify, $notifyData);

        $notification = new Notification;
        $notification->user_id = $user->id;
        $notification->type = Notification::TYPE_FOLLOWING;
        //$notification->result_firebase = $result;
        $notification->notify = json_encode($notify);
        $notification->notify_data = json_encode($notifyData);
        $notification->save();
    }

    public function sendNotificationsRate($user , $post){
        $pushNotify = new PushNotify;
        $tokens = DeviceToken::getByUserId($user->id);

        $notify = [
            "title" => __("notify.rate"),
            "body" => __("notify.user_rate_post") . $post->title
        ];
                
        $notifyData = [
            "type" => Notification::TYPE_RATE,
            "acction_id" => $post->id,
        ];
        $result = $pushNotify->sendNotification($tokens, $notify, $notifyData);

        $notification = new Notification;
        $notification->user_id = $user->id;
        $notification->type = Notification::TYPE_RATE;
        //$notification->result_firebase = $result;
        $notification->notify = json_encode($notify);
        $notification->notify_data = json_encode($notifyData);
        $notification->save();
    }
}
