<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{

		static $viewPath = "dashboard.pages.";

	    public function index(){

			//$user = \App\User::find(2);
			//$user->revokePermissionTo("m_user_view");
			//return $user->givePermissionTo("m_providers_view");
	    	return view(self::$viewPath . 'index');
	    }


}
