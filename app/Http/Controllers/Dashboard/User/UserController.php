<?php

namespace App\Http\Controllers\Dashboard\User;

use App\Http\Requests\Dashboard\User\StoreUserRequest;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Post\Post;
use App\Models\Post\PostOffer;

class UserController extends Controller
{
    static $viewPath = "dashboard.pages.user.";
    static $route = "dashboard.user.";

    public function __construct()
    {
        $this->middleware('permission:m_user_view')->only(["index", "datatable"]);
        $this->middleware('permission:m_user_create')->only(["create", "store"]);
    }

    public function index(){
       return view(self::$viewPath . "index");
    }

    public function create(){
        return view(self::$viewPath . "create");
    }

    public function edit(User $user){
        return view(self::$viewPath . "edit" , compact("user"));
    }

    public function details(User $user){
        return view(self::$viewPath . "details" , compact("user"));
    }

    public function datatable(){
        $user = User::role(User::ROLE_USER)->latest()->get();
        $admin = auth()->user();
        return DataTables::of($user)
            ->editColumn('id', function ($user) {
                static $id = 1;
                return $id++;
            })
            ->editColumn('avatar', function ($user) {
                return view("dashboard.datatable.avatar", [
                    "id" => $user->id,
                    "url" => route(self::$route . "index"),
                    "avatar" => $user->avatar,
                ]);
            })
            ->editColumn('name', function ($user) {
                return view("dashboard.datatable.edit", [
                    "name" => $user->name,
                    "url" => route(self::$route . "update.name", $user->id),
                    "value" => $user->name,
                    "type" => "text",
                ]);
            })
            ->editColumn('phone_number', function ($user) {
                return view("dashboard.datatable.edit", [
                    "name" => $user->phone_number,
                    "url" => route(self::$route . "update.phone_number", $user->id),
                    "value" => $user->phone_number,
                    "type" => "text",
                ]);
            })
            ->editColumn('balance', function ($user) {
                return $user->balance;
            })
            ->editColumn('is_blocked', function ($user) use ($admin) {
                if ($admin->can("m_user_blocked")) {
                    if ($user->is_blocked == 0) {
                        $text = __('global.blocked');
                        $class = "btn-success btn-sm";
                        $url = route(self::$route . "update.is_blocked", $user->id);
                    } else {
                        $text = __('global.unblocked');
                        $class = "btn-danger btn-sm";
                        $url = route(self::$route . "update.is_blocked", $user->id);
                    }
                    $action_id = $user->id;
                    return view("dashboard.datatable.toggle_btn", compact('text', 'class', 'action_id', 'url'));
                }
                return __("global.not_have_permissions");
            })
            ->editColumn('created_at', function ($user) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('actions', function ($data) use ($admin) {
                $actions = [];
                if ($admin->can("m_user_edit")) {
                    $actions[] = [
                        "name" => trans("global.edit"),
                        "route" =>  route(self::$route . "edit", $data->id),
                        "icon" => "ft-edit"
                    ];
                }
                 $actions[] = [
                     "name" => trans("global.view"),
                     "route" =>  route(self::$route . "details", $data->id),
                     "icon" => "ft-eye"
                 ];

                if (sizeof($actions) < 1) {
                    return __("global.not_have_permissions");
                }
                return view("dashboard.datatable.actions", compact('actions', 'data'));
            })
            ->make(true);
    }

    public function store(StoreUserRequest $request){
        if ($request->id_number != null) {
            $validNumber = User::where("id_number" , $request->id_number)->first();
            if ($validNumber) {
                return back()
                    ->withInput()
                    ->withErrors(['id_number' => 'ERROR: in validation!']);
            }
        }
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->phone_number = phoneFormat($request->phone_number);
        if ($request->has("image")) {
            $user->avatar = upImage($request->file("image"), "user/users/image");
        }
        if ($request->has("id_image")) {
            $user->id_image = upImage($request->file("id_image"), "user/id_image/image");
            $user->id_number = $request->id_number;
            $user->is_valid = 1;
        }
    	$user->is_blocked = 0;
        $user->login_with = User::RAHEEB;
    	$user->balance = 0;
    	$user->save();
    	$user->assignRole(User::ROLE_USER);

    	return redirect()->route(self::$route . "index");
    }

    public function user_post_sold_datatable(User $user){
        $post = Post::whereHas("postSold" , function($q) use ($user){
            $q->where("user_id" , $user->id);
        })->get();
        return DataTables::of($post)
        ->editColumn('id' , function($post){
            static $id = 1;
            return $id++;
        })
        ->editColumn('user' , function($post){
             return $post->user->name;
        })
        ->editColumn('title' , function($post){
             return $post->title;
        })
        ->editColumn('category' , function($post){
             return $post->category->name;
        })
        ->editColumn('total_price' , function($post){
             return $post->postSold->total_price;
        })
        ->editColumn('created_at' , function($post){
            return  $post->status == Post::STATUS_SOLD ? $post->postSold->created_at->diffForHumans() : $post->updated_at->diffForHumans();
        })
        ->addColumn('actions', function ($data) use ($post) {
            $actions = [];
            $actions[] = [
                "name" => trans("global.view"),
                "route" =>  route(self::$route . "details" , $data->id),
                "icon" => "ft-eye"
            ];

            return view("dashboard.datatable.actions", compact('actions', 'data'));
        })
        ->make(true);
    }

    public function user_offer_datatable(User $user){
        $offer = PostOffer::where("user_id" , $user->id)->get();
        return DataTables::of($offer)
        ->editColumn('id' , function($offer){
            static $id = 1;
            return $id++;
        })
        ->editColumn('title' , function($offer){
             return $offer->post->title;
        })
        ->editColumn('category' , function($offer){
             return $offer->post->category->name;
        })
        ->editColumn('price_offer' , function($offer){
             return $offer->price;
        })
        ->editColumn('price_post' , function($offer){
            return $offer->post->price;
        })
        ->editColumn('status' , function($offer){
            return $offer->status;
        })
        ->editColumn('created_at' , function($offer){
            return $offer->updated_at->diffForHumans();
        })
        ->make(true);

    }


}
