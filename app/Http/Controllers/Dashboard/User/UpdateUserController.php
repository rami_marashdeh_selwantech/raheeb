<?php

namespace App\Http\Controllers\Dashboard\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UpdateUserController extends Controller
{
    public function is_blocked($user){
    	$user = User::find($user);
    	if ($user->is_blocked == 1) {
    		$user->is_blocked = 0;
    	}else{
    		$user->is_blocked = 1;
    	}
    	$user->save();

    	return $this->successResponse(null, "success");
    }

    public function name(Request $request , $user){
    	$user = User::find($user);
    	$request->validate([
            "data" => "required"
        ]);
        $user->name = $request->data;
        $user->save();
        return $this->successResponse(null, $user->name);
    }

    public function phone_number(Request $request , $user){
    	$user = User::find($user);
    	$request->validate([
            "data" => "required|unique:users,phone_number,".$user->id."|phone:" . setting("country_iso")
        ]);
        $user->phone_number = phoneFormat($request->data);
        $user->save();
        return $this->successResponse(null, $user->phone_number);
    }

    public function updateUser(Request $request , User $user){
        $request->validate([
            "name" => "required",
            "email" => "required|email|unique:users,email," . $user->id,
            "phone_number" => "required|unique:users,phone_number," . $user->id ."|phone:" . setting("country_iso")

        ]);
        if ($request->id_number != null) {
            $validNumber = User::where("id_number" , $request->id_number)->first();
            if ($validNumber && $validNumber->id != $user->id) {
                return back()
                    ->withInput()
                    ->withErrors(['id_number' => 'ERROR: in validation!']);
            }
        }
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->has("password") && $request->password != null) {
        	$request->validate(["password" => "max:20|min:8"]);
        	$user->password = bcrypt($request->password);
        }
        $user->phone_number = phoneFormat($request->phone_number);
        if ($request->has("image")) {
            $user->avatar = upImage($request->file("image"), "user/users/image");
        }
        if ($request->has("id_image")) {
            $user->id_image = upImage($request->file("id_image"), "user/id_image/image");
            $user->id_number = $request->id_number;
            $user->is_valid = 1;
        }
    	$user->is_blocked = 0;
    	$user->balance = 0;
    	$user->save();

    	return redirect()->route("dashboard.user." . "index");
    }

    public function updateAdmin(Request $request , User $user){
        $request->validate([
            "name" => "required",
            "email" => "required|email|unique:users,email," . $user->id,
            "phone_number" => "required|unique:users,phone_number," . $user->id ."|phone:" . setting("country_iso")

        ]);
        if ($request->id_number != null) {
            $validNumber = User::where("id_number" , $request->id_number)->first();
            if ($validNumber && $validNumber->id != $user->id) {
                return back()
                    ->withInput()
                    ->withErrors(['id_number' => 'ERROR: in validation!']);
            }
        }
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->has("password") && $request->password != null) {
        	$request->validate(["password" => "max:20|min:8"]);
        	$user->password = bcrypt($request->password);
        }
        $user->phone_number = phoneFormat($request->phone_number);
        if ($request->has("image")) {
            $user->avatar = upImage($request->file("image"), "user/admin/image");
        }
        if ($request->has("id_image")) {
            $user->id_image = upImage($request->file("id_image"), "user/id_image/image");
            $user->id_number = $request->id_number;
            $user->is_valid = 1;
        }
    	$user->is_blocked = 0;
    	$user->balance = 0;
    	$user->save();
        $user->syncRoles($request->adminrole);
        if ($request->adminrole == User::ROLE_ADMIN) {
            $request->has("m_categories_create") ? $user->givePermissionTo("m_categories_create") : $user->revokePermissionTo("m_categories_create") ;
            $request->has("m_categories_edit") ? $user->givePermissionTo("m_categories_edit") :  $user->revokePermissionTo("m_categories_edit")  ;
            $request->has("m_categories_view") ? $user->givePermissionTo("m_categories_view") :  $user->revokePermissionTo("m_categories_view")  ;
            $request->has("m_category_box_size_create") ? $user->givePermissionTo("m_category_box_size_create"):  $user->revokePermissionTo("m_category_box_size_create")  ;
            $request->has("m_category_box_size_edit") ? $user->givePermissionTo("m_category_box_size_edit"):  $user->revokePermissionTo("m_category_box_size_edit")  ;
            $request->has("m_category_box_size_view") ? $user->givePermissionTo("m_category_box_size_view"):  $user->revokePermissionTo("m_category_box_size_view") ;
            $request->has("m_conditions_create") ? $user->givePermissionTo("m_conditions_create"):  $user->revokePermissionTo("m_conditions_create")  ;
            $request->has("m_conditions_edit") ? $user->givePermissionTo("m_conditions_edit"):  $user->revokePermissionTo("m_conditions_edit")  ;
            $request->has("m_conditions_view") ? $user->givePermissionTo("m_conditions_view") :  $user->revokePermissionTo("m_conditions_view") ;
            $request->has("m_notifications_create") ? $user->givePermissionTo("m_notifications_create") :  $user->revokePermissionTo("m_notifications_create") ;
            $request->has("m_notifications_view") ? $user->givePermissionTo("m_notifications_view") :  $user->revokePermissionTo("m_notifications_view") ;
            $request->has("m_post_report_view") ? $user->givePermissionTo("m_post_report_view") :  $user->revokePermissionTo("m_categories_create") ;
            $request->has("m_post_solds_view") ? $user->givePermissionTo("m_post_solds_view") :  $user->revokePermissionTo("m_post_solds_view") ;
            $request->has("m_post_view") ? $user->givePermissionTo("m_post_view") :  $user->revokePermissionTo("m_post_view") ;
            $request->has("m_settings_edit") ? $user->givePermissionTo("m_settings_edit") :  $user->revokePermissionTo("m_settings_edit") ;
            $request->has("m_settings_view") ? $user->givePermissionTo("m_settings_view") :  $user->revokePermissionTo("m_settings_view") ;
            $request->has("m_ticket_view") ? $user->givePermissionTo("m_ticket_view") :  $user->revokePermissionTo("m_ticket_view") ;
            $request->has("m_user_blocked") ? $user->givePermissionTo("m_user_blocked") :  $user->revokePermissionTo("m_user_blocked") ;
            $request->has("m_user_create") ? $user->givePermissionTo("m_user_create") :  $user->revokePermissionTo("m_user_create") ;
            $request->has("m_user_edit") ? $user->givePermissionTo("m_user_edit") :  $user->revokePermissionTo("m_user_edit") ;
            $request->has("m_user_view") ? $user->givePermissionTo("m_user_view") :  $user->revokePermissionTo("m_user_view") ;
        }
    	return redirect()->route("dashboard.admin.". "index");
    }
}
