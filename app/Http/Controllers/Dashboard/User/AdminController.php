<?php

namespace App\Http\Controllers\Dashboard\User;

use App\Http\Requests\Dashboard\User\StoreUserRequest;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    static $viewPath = "dashboard.pages.admin.";
    static $route = "dashboard.admin.";

    public function __construct()
    {
        $this->middleware('permission:m_super_admin_view')->only(["index", "datatable"]);
        $this->middleware('permission:m_super_admin_create')->only(["create", "store"]);

    }

    public function index(){
       return view(self::$viewPath . "index");
    }

    public function create(){
        return view(self::$viewPath . "create");
    }

    public function edit(User $user){
        return view(self::$viewPath . "edit" , compact("user"));
    }

    public function datatable(){
        $authorizedRoles  = [User::ROLE_ADMIN , User::ROLE_SUPER_ADMIN];
        $user = User::whereHas('roles', static function ($query) use ($authorizedRoles) {
                    return $query->whereIn('name', $authorizedRoles);
                })
                ->latest()
                ->get();
        return DataTables::of($user)
            ->editColumn('id', function ($user) {
                static $id = 1;
                return $id++;
            })
            ->editColumn('avatar', function ($user) {
                return view("dashboard.datatable.avatar", [
                    "id" => $user->id,
                    "url" => route(self::$route . "index"),
                    "avatar" => $user->avatar,
                ]);
            })
            ->editColumn('name', function ($user) {
                return view("dashboard.datatable.edit", [
                    "name" => $user->name,
                    "url" => route(UserController::$route . "update.name", $user->id),
                    "value" => $user->name,
                    "type" => "text",
                ]);
            })
            ->editColumn('phone_number', function ($user) {
                return view("dashboard.datatable.edit", [
                    "name" => $user->phone_number,
                    "url" => route(UserController::$route . "update.phone_number", $user->id),
                    "value" => $user->phone_number,
                    "type" => "text",
                ]);
            })
            ->editColumn('is_blocked', function ($user) {
                if ($user->is_blocked == 0) {
                    $text = __('global.blocked');
                    $class = "btn-success btn-sm";
                    $url = route(UserController::$route . "update.is_blocked", $user->id);
                } else {
                    $text = __('global.unblocked');
                    $class = "btn-danger btn-sm";
                    $url = route(UserController::$route . "update.is_blocked", $user->id);
                }
                $action_id = $user->id;
                return view("dashboard.datatable.toggle_btn", compact('text', 'class', 'action_id', 'url'));
            })
            ->editColumn('created_at', function ($user) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('actions', function ($data) use ($user) {
                $actions = [];
                $actions[] = [
                    "name" => trans("global.edit"),
                    "route" =>  route(self::$route . "edit", $data->id),
                    "icon" => "ft-edit"
                ];
                // $actions[] = [
                //     "name" => trans("global.view"),
                //     "route" =>  route(self::$route . "details", $data->id),
                //     "icon" => "ft-eye"
                // ];
                return view("dashboard.datatable.actions", compact('actions', 'data'));
            })
            ->make(true);
    }

    public function store(StoreUserRequest $request){
        if ($request->id_number != null) {
            $validNumber = User::where("id_number" , $request->id_number)->first();
            if ($validNumber) {
                return back()
                    ->withInput()
                    ->withErrors(['id_number' => 'ERROR: in validation!']);
            }
        }
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->phone_number = phoneFormat($request->phone_number);
        if ($request->has("image")) {
            $user->avatar = upImage($request->file("image"), "user/admin/image");
        }
        if ($request->has("id_image")) {
            $user->id_image = upImage($request->file("id_image"), "user/id_image/image");
            $user->id_number = $request->id_number;
            $user->is_valid = 1;
        }
    	$user->is_blocked = 0;
        $user->login_with = User::RAHEEB;
    	$user->balance = 0;
    	$user->save();
        $user->assignRole($request->adminrole);
        if ($request->adminrole == User::ROLE_ADMIN) {
            $request->has("m_categories_create") ? $user->givePermissionTo("m_categories_create") : null ;
            $request->has("m_categories_edit") ? $user->givePermissionTo("m_categories_edit") : null  ;
            $request->has("m_categories_view") ? $user->givePermissionTo("m_categories_view") : null  ;
            $request->has("m_category_box_size_create") ? $user->givePermissionTo("m_category_box_size_create"): null  ;
            $request->has("m_category_box_size_edit") ? $user->givePermissionTo("m_category_box_size_edit"): null  ;
            $request->has("m_category_box_size_view") ? $user->givePermissionTo("m_category_box_size_view"): null ;
            $request->has("m_conditions_create") ? $user->givePermissionTo("m_conditions_create"): null  ;
            $request->has("m_conditions_edit") ? $user->givePermissionTo("m_conditions_edit"): null  ;
            $request->has("m_conditions_view") ? $user->givePermissionTo("m_conditions_view") : null ;
            $request->has("m_notifications_create") ? $user->givePermissionTo("m_notifications_create") : null ;
            $request->has("m_notifications_view") ? $user->givePermissionTo("m_notifications_view") : null ;
            $request->has("m_post_report_view") ? $user->givePermissionTo("m_post_report_view") : null ;
            $request->has("m_post_solds_view") ? $user->givePermissionTo("m_post_solds_view") : null ;
            $request->has("m_post_view") ? $user->givePermissionTo("m_post_view") : null ;
            $request->has("m_settings_edit") ? $user->givePermissionTo("m_settings_edit") : null ;
            $request->has("m_settings_view") ? $user->givePermissionTo("m_settings_view") : null ;
            $request->has("m_ticket_view") ? $user->givePermissionTo("m_ticket_view") : null ;
            $request->has("m_user_blocked") ? $user->givePermissionTo("m_user_blocked") : null ;
            $request->has("m_user_create") ? $user->givePermissionTo("m_user_create") : null ;
            $request->has("m_user_edit") ? $user->givePermissionTo("m_user_edit") : null ;
            $request->has("m_user_view") ? $user->givePermissionTo("m_user_view") : null ;
        }
    	return redirect()->route(self::$route . "index");
    }


}
