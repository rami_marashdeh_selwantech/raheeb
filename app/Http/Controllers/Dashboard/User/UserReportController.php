<?php

namespace App\Http\Controllers\Dashboard\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\User\UserReport;
use App\User;

class UserReportController extends Controller
{
    static $viewPath = "dashboard.pages.user.report.";
    static $route = "dashboard.user_report.";

    public function __construct()
    {
        $this->middleware('permission:m_user_view')->only(["index", "datatable"]);
    }

    public function index(){
       return view(self::$viewPath . "index");
    }

    public function details(UserReport $user_report){
        $user = User::find($user_report->report_id);
        return view(self::$viewPath . "details" , compact("user" , "user_report"));
    }

    public function datatable(){
        $report = UserReport::latest()->get();
        return DataTables::of($report)
        ->editColumn('id' , function($report){
            static $id = 1;
            return $id++;
        })
        ->editColumn('user_report_to' , function($report){
             return $report->report_id;
        })
        ->editColumn('user_report_from' , function($report){
            return $report->user_id;
        })
        ->editColumn('message' , function($report){
             return $report->message;
        })
        ->editColumn('image' , function($report){
            return view("dashboard.datatable.avatar", [
                "id" => $report->id,
                "url" => route(self::$route . "index"),
                "avatar" => $report->image,
            ]);
        })
        ->editColumn('created_at' , function($report){
            return  $report->created_at->diffForHumans();
        })
        ->addColumn('actions', function ($data) use ($report) {
            $actions = [];
            $actions[] = [
                "name" => trans("global.view"),
                "route" =>  route(self::$route . "details" , $data->id),
                "icon" => "ft-eye"
            ];

            return view("dashboard.datatable.actions", compact('actions', 'data'));
        })
        ->make(true);
    }

}
