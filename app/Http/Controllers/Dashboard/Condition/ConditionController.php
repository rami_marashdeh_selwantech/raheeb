<?php

namespace App\Http\Controllers\Dashboard\Condition;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Condition\Condition;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\Dashboard\Condition\ConditionRequest;

class ConditionController extends Controller
{
    static $viewPath = "dashboard.pages.condition.";
    static $route = "dashboard.condition.";

    public function __construct()
    {
        $this->middleware('permission:m_conditions_view')->only(["index", "datatable"]);
        $this->middleware('permission:m_conditions_create')->only(["create", "store"]);
        $this->middleware('permission:m_conditions_edit')->only(["edit"]);
    }

    public function index(){
       return view(self::$viewPath . "index");
    }

    public function create(){
        return view(self::$viewPath . "create");
    }

    public function edit(Condition $condition){
        return view(self::$viewPath . "edit" , compact("category"));
    }

    public function datatable(){
        $condition = Condition::latest()->get();
        $admin = auth()->user();
        return DataTables::of($condition)
            ->editColumn('id', function ($condition) {
                static $id = 1;
                return $id++;
            })
            ->editColumn('name', function ($condition) {
                return view("dashboard.datatable.edit", [
                    "name" => $condition->name,
                    "url" => route(self::$route . "update.name", $condition->id),
                    "value" => $condition->name,
                    "type" => "text",
                ]);
            })
            ->editColumn('is_show', function ($condition) use ($admin) {
                if ($admin->can("m_conditions_edit")) {
                    if ($condition->is_show == 1) {
                        $text = __('global.show');
                        $class = "btn-success btn-sm";
                        $url = route(self::$route . "update.is_show", $condition->id);
                    } else {
                        $text = __('global.hidden');
                        $class = "btn-danger btn-sm";
                        $url = route(self::$route . "update.is_show", $condition->id);
                    }
                    $action_id = $condition->id;
                    return view("dashboard.datatable.toggle_btn", compact('text', 'class', 'action_id', 'url'));
                }
                return __("global.not_have_permissions");
            })
            ->editColumn('ordering', function ($condition) use ($admin) {
                if ($admin->can("m_conditions_edit")) {
                    return view("dashboard.datatable.edit", [
                        "name" => $condition->ordering,
                        "url" => route(self::$route . "update.ordering", $condition->id),
                        "value" => $condition->ordering,
                        "type" => "number",
                    ]);
                }
            })
            ->editColumn('created_at', function ($condition) {
                return $condition->created_at->diffForHumans();
            })
            ->make(true);
    }

    public function store(ConditionRequest $request){
        if ($this->swapOrdering($request->ordering)) {
            $condition = new Condition;
            $condition->name = $request->name;
            $condition->is_show = 1;
            $condition->ordering = $request->ordering;
            $condition->save();
        }
        return $this->successResponse(null, "success");
    }

    public function ordering(Request $request , Condition $condition){
        if ($this->swapOrdering($request->data)) {
            $condition->ordering = $request->data;
            $condition->save();
        }
        return $this->successResponse(null, $condition->ordering);
    }

    public function name(Request $request , Condition $condition){
        $condition->name = $request->data;
        $condition->save();
        return $this->successResponse(null, $condition->name);
    }

    public function is_show(Condition $condition){
    	if ($condition->is_show == 1) {
    		$condition->is_show = 0;
    	}else{
    		$condition->is_show = 1;
    	}
    	$condition->save();

    	return $this->successResponse(null, "success_button_show");
    }

    private function swapOrdering($ordering , $id = null){
        if ($id != null ) {
            $orderCondistion = Condition::where("id" , $id)->where("ordering" , $ordering)->first();
        }else{
            $orderCondistion = Condition::where("ordering" , $ordering)->first();
        }
        if ($orderCondistion) {
            $orderCondistion->ordering = $ordering + 1;
            $lastOrdering = Condition::where("ordering" , $ordering + 1 )->first();
            $orderCondistion->save();
            if ($lastOrdering) {
                return $this->swapOrdering($orderCondistion->ordering , $lastOrdering->id);
            }
        }
        return true;
    }
}
