<?php

namespace App\Http\Controllers\Dashboard\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category\Category;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\Dashboard\Category\CreateCategoryRequest;

class CategoryController extends Controller
{
    static $viewPath = "dashboard.pages.category.";
    static $route = "dashboard.category.";

    public function __construct()
    {
        $this->middleware('permission:m_categories_view')->only(["index", "datatable"]);
        $this->middleware('permission:m_categories_create')->only(["create", "store"]);
        $this->middleware('permission:m_categories_edit')->only(["edit"]);
    }

    public function index(){
       return view(self::$viewPath . "index");
    }

    public function create(){
        return view(self::$viewPath . "create");
    }

    public function edit(Category $category){
        return view(self::$viewPath . "edit" , compact("category"));
    }

    public function datatable(){
        $category = Category::latest()->get();
        $admin = auth()->user();
        return DataTables::of($category)
            ->editColumn('id', function ($category) {
                static $id = 1;
                return $id++;
            })
            ->editColumn('image', function ($category) {
                return view("dashboard.datatable.avatar", [
                    "id" => $category->id,
                    "url" => route(self::$route . "index"),
                    "avatar" => $category->image,
                ]);
            })
            ->editColumn('name', function ($category) {
                return view("dashboard.datatable.edit", [
                    "name" => $category->name,
                    "url" => route(self::$route . "update.name", $category->id),
                    "value" => $category->name,
                    "type" => "text",
                ]);
            })
            ->editColumn('is_ship_nationwide', function ($category) use ($admin) {
                if ($admin->can("m_categories_edit")) {
                    if ($category->is_ship_nationwide == 1) {
                        $text = __('global.ship_nationwide');
                        $class = "btn-success btn-sm";
                        $url = route(self::$route . "update.is_ship_nationwide", $category->id);
                    } else {
                        $text = __('global.pickup');
                        $class = "btn-info btn-sm";
                        $url = route(self::$route . "update.is_ship_nationwide", $category->id);
                    }
                    $action_id = $category->id;
                    return view("dashboard.datatable.toggle_btn", compact('text', 'class', 'action_id', 'url'));
                }
                return __("global.not_have_permissions");
            })
            ->editColumn('is_valid', function ($category) use ($admin) {
                if ($admin->can("m_categories_edit")) {
                    if ($category->is_valid == 1) {
                        $text = __('global.valid');
                        $class = "btn-success btn-sm";
                        $url = route(self::$route . "update.is_valid", $category->id);
                    } else {
                        $text = __('global.unvalid');
                        $class = "btn-danger btn-sm";
                        $url = route(self::$route . "update.is_valid", $category->id);
                    }
                    $action_id = $category->id;
                    return view("dashboard.datatable.toggle_btn", compact('text', 'class', 'action_id', 'url'));
                }
                return __("global.not_have_permissions");
            })
            ->editColumn('ordering', function ($category) use ($admin) {
                if ($admin->can("m_categories_edit")) {
                    return view("dashboard.datatable.edit", [
                        "name" => $category->ordering,
                        "url" => route(self::$route . "update.ordering", $category->id),
                        "value" => $category->ordering,
                        "type" => "number",
                    ]);
                }
            })
            ->editColumn('created_at', function ($category) {
                return $category->created_at->diffForHumans();
            })
            ->addColumn('actions', function ($data) use ($admin) {
                $actions = [];
                $actions[] = [
                    "name" => trans("global.details"),
                    "route" =>  route(self::$route . "details", $data->id),
                    "icon" => "ft-eye"
                ];
                if ($admin->can("m_categories_edit")) {
                    $actions[] = [
                        "name" => trans("global.edit"),
                        "route" =>  route(self::$route . "edit", $data->id),
                        "icon" => "ft-edit"
                    ];
                }
                if ($admin->can("m_category_box_size_view") && $data->is_ship_nationwide == 1) {
                    $actions[] = [
                        "name" => trans("global.box_sizes"),
                        "route" =>  route(self::$route . "box_size.index", $data->id),
                        "icon" => "ft-eye"
                    ];
                }
                if (sizeof($actions) < 1) {
                    return __("global.not_have_permissions");
                }
                return view("dashboard.datatable.actions", compact('actions', 'data'));
            })
            ->make(true);
    }

    public function store (CreateCategoryRequest $request){
        if ($this->swapOrdering($request->ordering)) {
            $category = new Category;
            $category->name = $request->name;
            $category->ordering = $request->ordering;
            if ($request->has("image")) {
                $category->image = upImage($request->file("image"), "category/image");
            }
            $category->is_valid = $request->get("is_valid" , 0);
            $category->is_ship_nationwide = $request->get("is_ship_nationwide" , 0);
            $category->save();
        }

        return redirect()->route(self::$route . "index");
    }

    public function swapOrdering($ordering , $id = null){
        if ($id != null ) {
            $orderCategory = Category::where("id" , $id)->where("ordering" , $ordering)->first();
        }else{
            $orderCategory = Category::where("ordering" , $ordering)->first();
        }
        if ($orderCategory) {
            $orderCategory->ordering = $ordering + 1;
            $lastOrdering = Category::where("ordering" , $ordering + 1 )->first();
            $orderCategory->save();
            if ($lastOrdering) {
                return $this->swapOrdering($orderCategory->ordering , $lastOrdering->id);
            }
        }
        return true;
    }
}
