<?php

namespace App\Http\Controllers\Dashboard\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category\Category;
use App\Http\Requests\Dashboard\Category\CreateCategoryRequest;

class UpdateCategoryController extends Controller
{

    public function all (Request $request , Category $category){
        $request->validate([
            "name" => "required",
            "ordering" => "required"
        ]);
        $category->name = $request->name;
        $category->ordering = $request->ordering;
        if ($request->has("image")) {
            $category->image = upImage($request->file("image"), "category/image");
        }
        $category->is_valid = $request->get("is_valid" , 0);
        $category->is_ship_nationwide = $request->get("is_ship_nationwide" , 0);
        $category->save();

        return redirect()->route("dashboard.category." . "index");
    }

    public function is_ship_nationwide(Category $category){
    	if ($category->is_ship_nationwide == 1) {
    		$category->is_ship_nationwide = 0;
    	}else{
    		$category->is_ship_nationwide = 1;
    	}
    	$category->save();

    	return $this->successResponse(null, "success_button_approved");
    }

    public function is_valid(Category $category){
    	if ($category->is_valid == 1) {
    		$category->is_valid = 0;
    	}else{
    		$category->is_valid = 1;
    	}
    	$category->save();

    	return $this->successResponse(null, "success_button_approved");
    }

    public function name(Category $category , Request $request){
    	$category->name = $request->data;
    	$category->save();

    	return $this->successResponse(null, $category->name);
    }

    public function ordering(Category $category , Request $request){
        $categoryController = new CategoryController();
        if ($categoryController->swapOrdering($request->data)) {
            $category->ordering = $request->data;
        	$category->save();
        }
    	return $this->successResponse(null, $category->ordering);
    }
}
