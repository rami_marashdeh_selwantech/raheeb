<?php

namespace App\Http\Controllers\Dashboard\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category\Category;
use App\Models\Category\CategoryBoxSize;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\Dashboard\BoxSize\BoxSizeRequest;

class BoxSizeCategoryController extends Controller
{
    static $viewPath = "dashboard.pages.box_size.";
    static $route = "dashboard.category.box_size.";

    public function __construct()
    {
        $this->middleware('permission:m_category_box_size_view')->only(["index", "datatable"]);
        $this->middleware('permission:m_category_box_size_create')->only(["create", "store"]);
        $this->middleware('permission:m_category_box_size_edit')->only(["edit"]);
    }

    public function index(Category $category){
        return view(self::$viewPath . "index" , compact("category"));
    }

    public function create(Category $category){
        return view(self::$viewPath . "create" , compact("category"));
    }

    public function store(BoxSizeRequest $request , Category $category){
        $box = new CategoryBoxSize;
        $box->category_id = $category->id;
        $box->size = $request->size;
        $box->unit = $request->unit;
        $box->price = $request->price;
        if ($request->has("image")) {
            $box->image = upImage($request->file("image"), "box_size/image");
        }
        $box->save();
        return redirect()->route(self::$route . "index" , $category->id);
    }

    public function edit(Category $category , CategoryBoxSize $category_box_size){
        return view(self::$viewPath . "edit" , compact("category_box_size"));
    }

    public function update(BoxSizeRequest $request  , Category $category , CategoryBoxSize $category_box_size){
        $box = CategoryBoxSize::find($category_box_size->id);
        $box->category_id = $category->id;
        $box->size = $request->size;
        $box->unit = $request->unit;
        $box->price = $request->price;
        if ($request->has("image")) {
            $box->image = upImage($request->file("image"), "box_size/image");
        }
        $box->save();
        return redirect()->route(self::$route . "index" , $category->id);
    }

    public function datatable(Category $category){
        $box = CategoryBoxSize::where("category_id" , $category->id)->latest()->get();
        $admin = auth()->user();
        return DataTables::of($box)
            ->editColumn('id', function ($box) {
                static $id = 1;
                return $id++;
            })
            ->editColumn('image', function ($box) {
                return view("dashboard.datatable.avatar", [
                    "id" => $box->id,
                    "url" => route(self::$route . "index" , $box->category_id),
                    "avatar" => $box->image,
                ]);
            })
            ->editColumn('category_name', function ($box) {
                return $box->category->name;
            })
            ->editColumn('size', function ($box){
                return $box->size;
            })
            ->editColumn('unit', function ($box){
                return $box->unit;
            })
            ->editColumn('price', function ($box)  {
                return $box->price;
            })
            ->editColumn('created_at', function ($box) {
                return $box->created_at->diffForHumans();
            })
            ->addColumn('actions', function ($data) use ($admin) {
                $actions = [];
                if ($admin->can("m_category_box_size_edit")) {
                    $actions[] = [
                        "name" => trans("global.edit"),
                        "route" =>  route(self::$route . "edit", [ "category" => $data->category_id , "category_box_size" => $data->id]),
                        "icon" => "ft-edit"
                    ];
                }
                if (sizeof($actions) < 1) {
                    return __("global.not_have_permissions");
                }
                return view("dashboard.datatable.actions", compact('actions', 'data'));
            })
            ->make(true);
    }
}
