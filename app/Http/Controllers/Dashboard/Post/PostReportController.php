<?php

namespace App\Http\Controllers\Dashboard\Post;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post\PostReport;
use Yajra\DataTables\Facades\DataTables;
use App\MOdels\Post\Post;

class PostReportController extends Controller
{
    static $viewPath = "dashboard.pages.post.report.";
    static $route = "dashboard.post_report.";

    public function __construct(){
        $this->middleware('permission:m_post_report_view')->only(["index", "datatable"]);
    }

    public function index(){
       return view(self::$viewPath . "index");
    }

    public function details(Post $post){
        return view(self::$viewPath . "details" , compact("post"));
    }

    public function datatable(){
        $report = PostReport::latest()->get();
        return DataTables::of($report)
        ->editColumn('id' , function($report){
            static $id = 1;
            return $id++;
        })
        ->editColumn('user' , function($report){
             return $report->post->user->name;
        })
        ->editColumn('user_report' , function($report){
            return $report->user->name;
        })
        ->editColumn('title' , function($report){
             return $report->post->title;
        })
        ->editColumn('category' , function($report){
             return $report->post->category->name;
        })
        ->editColumn('message' , function($report){
             return $report->message;
        })
        ->editColumn('image' , function($report){
            return view("dashboard.datatable.avatar", [
                "id" => $report->id,
                "url" => route(self::$route . "index"),
                "avatar" => $report->image,
            ]);
        })
        ->editColumn('created_at' , function($report){
            return  $report->updated_at->diffForHumans();
        })
        ->addColumn('actions', function ($data) use ($report) {
            $actions = [];
            $actions[] = [
                "name" => trans("global.view"),
                "route" =>  route(self::$route . "details" , $data->post->id),
                "icon" => "ft-eye"
            ];

            return view("dashboard.datatable.actions", compact('actions', 'data'));
        })
        ->make(true);
    }

}
