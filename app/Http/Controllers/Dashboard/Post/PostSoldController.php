<?php

namespace App\Http\Controllers\Dashboard\Post;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Post\Post;

class PostSoldController extends Controller
{
    static $viewPath = "dashboard.pages.post.sold.";
    static $route = "dashboard.post_sold.";

    public function __construct()
    {
        $this->middleware('permission:m_post_solds_view')->only(["index", "datatable"]);
    }

    public function index(){
       return view(self::$viewPath . "index");
    }

    public function details(Post $post){
        return view(self::$viewPath . "details" , compact("post"));
    }

    public function datatable(){
        $post = Post::where("status" , "<>" , Post::STATUS_AVAILABLE)->latest()->get();
        return DataTables::of($post)
        ->editColumn('id' , function($post){
            static $id = 1;
            return $id++;
        })
        ->editColumn('user' , function($post){
             return $post->user->name;
        })
        ->editColumn('user_sold' , function($post){
            return $post->status == Post::STATUS_SOLD ? $post->postSold->user->name : __("app.sold_other_app");
        })
        ->editColumn('title' , function($post){
             return $post->title;
        })
        ->editColumn('category' , function($post){
             return $post->category->name;
        })
        ->editColumn('total_price' , function($post){
             return $post->postSold->total_price;
        })
        ->editColumn('created_at' , function($post){
            return  $post->status == Post::STATUS_SOLD ? $post->postSold->created_at->diffForHumans() : $post->updated_at->diffForHumans();
        })
        ->addColumn('actions', function ($data) use ($post) {
            $actions = [];
            $actions[] = [
                "name" => trans("global.view"),
                "route" =>  route(self::$route . "details" , $data->id),
                "icon" => "ft-eye"
            ];

            return view("dashboard.datatable.actions", compact('actions', 'data'));
        })
        ->make(true);
    }

}
