<?php

namespace App\Http\Controllers\Dashboard\Post;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Post\Post;
use App\User;
use Illuminate\Database\Eloquent\Builder;

class PostController extends Controller
{
    static $viewPath = "dashboard.pages.post.";
    static $route = "dashboard.post.";

    public function __construct()
    {
        $this->middleware('permission:m_post_view')->only(["index", "datatable"]);
    }

    public function index(){
        $status = [
            Post::STATUS_AVAILABLE,
            Post::STATUS_SOLD,
            Post::STATUS_SOLD_OTHER_APP
        ];
       return view(self::$viewPath . "index" , compact("status"));
    }

    public function details(Post $post){
        return view(self::$viewPath . "details" , compact("post"));
    }

    public function datatable($status){
        if ($status == "all") {
			$post = Post::latest()->get();
		}else{
			$post = Post::where("status" , $status)->latest()->get();
		}
        return $this->datatables($post);
    }

    public function user_datatable(User $user){
        return $this->datatables($user->posts);
    }

    public function user_faverate_datatable(User $user){
        $post = Post::whereHas("faverates" , function(Builder $query) use ($user){
            $query->where("user_id" , $user->id);
        })->get();
        return $this->datatables($post);
    }

    private function datatables($post){
        return DataTables::of($post)
        ->editColumn('id' , function($post){
            static $id = 1;
            return $id++;
        })
        ->editColumn('user' , function($post){
             return $post->user->name;
        })
        ->editColumn('title' , function($post){
             return $post->title;
        })
        ->editColumn('category' , function($post){
             return $post->category->name;
        })
        ->editColumn('status' , function($post){
            return __("global.$post->status");
        })
        ->editColumn('created_at' , function($post){
            return $post->created_at->diffForHumans();
        })
        ->addColumn('actions', function ($data) use ($post) {
            $actions = [];
            $actions[] = [
                "name" => trans("global.view"),
                "route" =>  route(self::$route . "details" , $data->id),
                "icon" => "ft-eye"
            ];

            return view("dashboard.datatable.actions", compact('actions', 'data'));
        })
        ->make(true);
    }

    public function faverateDatatable(Post $post){
        $faverate = $post->faverates;
        return DataTables::of($faverate)
            ->editColumn('id' , function($faverate){
                static $id = 1;
                return $id++;
            })
            ->editColumn('avatar', function ($faverate) {
                return view("dashboard.datatable.avatar", [
                    "id" => $faverate->user->id,
                    "url" => route(self::$route . "index"),
                    "avatar" => $faverate->user->avatar,
                ]);
            })
            ->editColumn('name', function ($faverate) {
                return $faverate->user->name;
            })
            ->editColumn('phone_number', function ($faverate) {
                return $faverate->user->phone_number;
            })
            ->editColumn('email', function ($faverate) {
                return $faverate->user->email;
            })
            ->editColumn('created_at', function ($faverate) {
                return $faverate->created_at->diffForHumans();
            })
            ->make(true);
    }

    public function offerDatatable(Post $post){
        $offers = $post->postoffers;
        return DataTables::of($offers)
            ->editColumn('id' , function($offers){
                static $id = 1;
                return $id++;
            })
            ->editColumn('avatar', function ($offers) {
                return view("dashboard.datatable.avatar", [
                    "id" => $offers->user->id,
                    "url" => route(self::$route . "index"),
                    "avatar" => $offers->user->avatar,
                ]);
            })
            ->editColumn('name', function ($offers) {
                return $offers->user->name;
            })
            ->editColumn('price_offer', function ($offers) {
                return $offers->price;
            })
            ->editColumn('status', function ($offers) {
                return $offers->status;
            })
            ->editColumn('created_at', function ($offers) {
                return $offers->created_at->diffForHumans();
            })
            ->make(true);
    }

    public function report_data_table(Post $post){
        $report = $post->postReport;
        return DataTables::of($report)
        ->editColumn('id' , function($report){
            static $id = 1;
            return $id++;
        })
        ->editColumn('user_report' , function($report){
            return $report->user->name;
        })
        ->editColumn('title' , function($report){
             return $report->post->title;
        })
        ->editColumn('category' , function($report){
             return $report->post->category->name;
        })
        ->editColumn('message' , function($report){
             return $report->message;
        })
        ->editColumn('image' , function($report){
            return view("dashboard.datatable.avatar", [
                "id" => $report->id,
                "url" => route(self::$route . "index"),
                "avatar" => $report->image,
            ]);
        })
        ->editColumn('created_at' , function($report){
            return  $report->updated_at->diffForHumans();
        })
        ->addColumn('actions', function ($data) use ($report) {
            $actions = [];
            $actions[] = [
                "name" => trans("global.view"),
                "route" =>  route(self::$route . "details" , $data->post->id),
                "icon" => "ft-eye"
            ];

            return view("dashboard.datatable.actions", compact('actions', 'data'));
        })
        ->make(true);
    }

}
