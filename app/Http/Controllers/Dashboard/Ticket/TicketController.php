<?php

namespace App\Http\Controllers\Dashboard\Ticket;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Ticket\Ticket;

class TicketController extends Controller
{
    static $viewPath = "dashboard.pages.ticket.";
    static $route = "dashboard.ticket.";

    public function __construct()
    {
        $this->middleware('permission:m_ticket_view')->only(["index", "datatable"]);
    }

    public function index(){
       return view(self::$viewPath . "index");
    }

    public function datatable(){
        $ticket = Ticket::latest()->get();
        return DataTables::of($ticket)
        ->editColumn('id' , function($ticket){
            static $id = 1;
            return $id++;
        })
        ->editColumn('user' , function($ticket){
             return $ticket->user->name;
        })
        ->editColumn('message' , function($ticket){
             return $ticket->message;
        })
        ->editColumn('image' , function($ticket){
            return view("dashboard.datatable.avatar", [
                "id" => $ticket->id,
                "url" => "#",
                "avatar" => $ticket->image,
            ]);
        })
        ->editColumn('created_at' , function($ticket){
            return $ticket->created_at->diffForHumans();
        })
        ->make(true);
    }

}
