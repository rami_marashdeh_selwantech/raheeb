<?php

namespace App\Http\Controllers\Dashboard\Distance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Distance\Distance;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\Dashboard\Distance\DistanceRequest;

class DistanceController extends Controller
{
    static $viewPath = "dashboard.pages.distance.";
    static $route = "dashboard.distance.";

    public function __construct()
    {
        $this->middleware('permission:m_settings_view')->only(["index", "datatable"]);
        $this->middleware('permission:m_settings_edit')->only(["create", "store"]);
    }

    public function index(){
       return view(self::$viewPath . "index");
    }

    public function datatable(){
        $distance = Distance::orderBy("distance" , "ASC")->get();
        $admin = auth()->user();
        return DataTables::of($distance)
            ->editColumn('id', function ($distance) {
                static $id = 1;
                return $id++;
            })
            ->editColumn('distance', function ($distance) {
                return view("dashboard.datatable.edit", [
                    "name" => $distance->distance,
                    "url" => route(self::$route . "update.distance", $distance->id),
                    "value" => $distance->distance,
                    "type" => "number",
                ]);
            })
            ->editColumn('is_show', function ($distance) use ($admin) {
                if ($admin->can("m_settings_edit")) {
                    if ($distance->is_show == 1) {
                        $text = __('global.show');
                        $class = "btn-success btn-sm";
                        $url = route(self::$route . "update.is_show", $distance->id);
                    } else {
                        $text = __('global.pickup');
                        $class = "btn-danger btn-sm";
                        $url = route(self::$route . "update.is_show", $distance->id);
                    }
                    $action_id = $distance->id;
                    return view("dashboard.datatable.toggle_btn", compact('text', 'class', 'action_id', 'url'));
                }
                return __("global.not_have_permissions");
            })
            ->editColumn('delete', function ($distance) use ($admin) {
                if ($admin->can("m_settings_edit")) {

                    $text = __('global.delete');
                    $class = "btn btn-outline-danger btn-sm";
                    $url = route(self::$route . "delete", $distance->id);

                    $action_id = $distance->id;
                    return view("dashboard.datatable.toggle_btn", compact('text', 'class', 'action_id', 'url'));
                }
                return __("global.not_have_permissions");
            })
            ->editColumn('created_at', function ($distance) {
                return $distance->created_at->diffForHumans();
            })
            ->make(true);
    }

    public function store(DistanceRequest $request){
        $distance = Distance::where("distance" , $request->distance)->first();
        if ($distance) {
            return $this->errorResponse(__("app.distance_exisest"));
        }
        $newDistance = new Distance ;
        $newDistance->distance = $request->distance;
        $newDistance->is_show = 1;
        $newDistance->save();

        return $this->successResponse(null, "success");
    }

    public function distance(Request $request , Distance $distance){
        $d_Distance = Distance::where("distance" , $request->data)
            ->where("id" , '<>' , $distance->id)
            ->update(["is_show" => 0 ]);
        $distance->distance = $request->data;
        $distance->save();
        return $this->successResponse(null, $distance->distance);
    }

    public function is_show(Distance $distance){
    	if ($distance->is_show == 1) {
    		$distance->is_show = 0;
    	}else{
            $d_Distance = Distance::where("distance" , $distance->distance)
                ->where("id" , '<>' , $distance->id)
                ->update(["is_show" => 0 ]);
    		$distance->is_show = 1;
    	}
    	$distance->save();

    	return $this->successResponse(null, "success_button_show");
    }

    public function delete(Distance $distance){
        $distance->delete();
        return $this->successResponse(null, "success_button_show");
    }

}
