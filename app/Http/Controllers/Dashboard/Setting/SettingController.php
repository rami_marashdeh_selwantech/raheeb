<?php

namespace App\Http\Controllers\Dashboard\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting\Setting;
use Illuminate\Support\Facades\Artisan;

class SettingController extends Controller
{
    static $viewPath = "dashboard.pages.settings.";
    static $route = "dashboard.settings.";

    public function index()
    {
        return view(self::$viewPath . "index");
    }

    public function update(Request $request)
    {
        //return $request->all();
        $settings = $request->get("settings", []);
        foreach ($settings as $key => $value) {
            Setting::set($key, $value);
            Artisan::call("cache:clear");
        }
        return redirect()->back()->withSuccess(__('global.updated_setting_successfully'));
    }
}
