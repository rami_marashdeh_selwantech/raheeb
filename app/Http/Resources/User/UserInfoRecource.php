<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserInfoRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "phone_number" => $this->phone_number,
            "email" => $this->email,
            "balance" => $this->balance,
            "avatar" => $this->avatar,
            "cover_image" => $this->cover_image,
            "id_image" => $this->id_image,
            "login_with" => $this->login_with,
            "is_valid" => $this->is_valid ? true : false,
            "is_blocked" => $this->is_blocked ? true : false,
            "count_followers" => $this->countFollowers($this->id),
            "count_following" => $this->countFollowing($this->id),
            "is_send_email" => $this->send_email ? true : false,
            "is_send_notifications" => $this->send_notifications ? true : false,
            "rate" => $this->getRate($this->id)
        ];
    }
}
