<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UsersDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "phone_number" => $this->phone_number,
            "email" => $this->email,
            //"balance" => authApi()->id() == $this->id ? $this->balance : null,
            "avatar" => $this->avatar,
            "cover_image" => $this->cover_image,
            //"id_image" => $this->id_image,
            "login_with" => $this->login_with,
            "is_valid" => $this->is_valid ? true : false,
            "is_blocked" => $this->is_blocked ? true : false,
            "is_follow" => $this->hasFollow($this->id),
            "count_followers" => $this->countFollowers($this->id),
            "count_following" => $this->countFollowing($this->id),
            "rate" => $this->getRate($this->id)
        ];
    }
}
