<?php

namespace App\Http\Resources\Chat;

use Illuminate\Http\Resources\Json\JsonResource;

class UserMessagesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "chat_id" => $this->chat_post_id,
            "offer_id" => $this->post_offer_id,
            "message_type" => $this->message_type,
            "message" => $this->message,
            "date" => $this->created_at->diffforhumans(),
            "offer" => $this->postOffer ? [
                "status" => $this->postOffer->status,
                "price" => $this->postOffer->price
            ] : null,
            "sender" => [
                "id" => $this->sender->id,
                "avatar" => $this->sender->avatar,
                "name" => $this->sender->name,
            ]
        ];
    }
}
