<?php

namespace App\Http\Resources\Chat;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Post\PostImageResource;

class UserChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user" => [
                "id" => $this->user_id != authApi()->id() ? $this->user->id : $this->post->user->id,
                "name" => $this->user_id != authApi()->id() ? $this->user->name : $this->post->user->name,
                "avatar" => $this->user_id != authApi()->id() ? $this->user->avatar : $this->post->user->avatar
            ],
            "post" => [
                "id" => $this->post->id,
                "title" => $this->post->title,
                "price" => $this->post->price,
                "images" => PostImageResource::collection($this->post->images),
                "status" => $this->post->status
            ],
            "date" => $this->messages()->orderBy("id" , "DESC")->first() ? $this->messages()->orderBy("id" , "DESC")->first()->created_at->diffForHumans() : null
        ];
    }
}
