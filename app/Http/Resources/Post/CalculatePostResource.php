<?php

namespace App\Http\Resources\Post;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Support\Money;

class CalculatePostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "item_price" => $this->price,
            "shipping" => $this->categoryBoxSize  ? $this->categoryBoxSize->price : null,
            "sale_tax" => Money::inDefaultCurrency(setting("tax_rate")),
            "total_price" => $this->getCalculatePostPrice()
        ];
    }
}
