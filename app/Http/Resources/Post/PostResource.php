<?php

namespace App\Http\Resources\Post;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Category\CategoryBoxSizeResource;
use App\Http\Resources\Condition\ConditionPostResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user_id" =>  $this->user_id,
            "category_id" =>   $this->category_id,
            "condition_id" =>   $this->condition_id,
            "status" =>  $this->status,
            "title" =>   $this->title,
            "description" =>  $this->description,
            "price" =>  $this->price,
            "lon" =>   $this->lon,
            "lat" =>   $this->lat,
            "is_ship_nationwide" =>   $this->is_ship_nationwide ? true : false,
            "category_box_size" => new CategoryBoxSizeResource($this->categoryBoxSize),
            "is_faverate" => $this->isFaverate(authApi()->id() , $this->id),
            "date" => $this->created_at->diffForHumans(),
            "images" => PostImageResource::collection($this->images),
            "user" => [
                "id" => $this->user->id,
                "name" => $this->user->name,
                "avatar" => $this->user->avatar,
                "is_valid" => $this->user->is_valid ? true : false,
                "rate" => 4.5
            ],
            "condition" => new ConditionPostResource($this->condition),
            "shipping_price" =>  $this->categoryBoxSize  ? $this->categoryBoxSize->price : null,
            "chat_id" => $this->getChatid($this->id)
        ];
    }
}
