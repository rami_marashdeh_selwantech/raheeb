"use strict";

class Socket {
    constructor(socket) {
        this.io = socket;
    }

    socketEvents() {
        this.io.on("connection", socket => {
            console.log("connection :", socket.id);

            socket.on("send_message", function(data) {
                console.log("send_message :", data);
                socket
                    .to("chat_" + data.chat.chat_id)
                    .emit("receive_message", data.chat);
            });

            socket.on("join_room", function(data) {
                socket.join("chat_" + data.chat_id);
            });

            socket.on("typing", function(data) {
                socket.to("chat_" + data.chat_id).emit("typing", {
                    typing: data.typing
                });
            });

            socket.on("leave_room", function(data) {
                socket.leave("chat_" + data.chat_id);
            });
        });
    }

    socketConfig() {
        this.socketEvents();
    }
}
module.exports = Socket;
