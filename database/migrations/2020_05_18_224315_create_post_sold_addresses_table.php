<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostSoldAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_sold_addresses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('post_sold_id')->constrained()->onDelete('cascade');   
            $table->string('building_number')->nullable(true);
            $table->string('name')->nullable(true);
            $table->string('floor')->nullable(true);
            $table->float('lat')->nullable(true);
            $table->float('lon')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_sold_addresses');
    }
}
