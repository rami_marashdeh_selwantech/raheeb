<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User\Notification;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->enum("type" , [
                Notification::NEW_MESSAGE,
                Notification::TYPE_OFFER,
                Notification::TYPE_RATE,
                Notification::TYPE_ADMIN,
                Notification::TYPE_APPROVED_OFFER,
                Notification::TYPE_REJECT_OFFER,
                Notification::TYPE_FOLLOWING
            ]);
            $table->text("result_firebase")->nullable();
            $table->text("notify")->nullable();
            $table->text("notify_data")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
