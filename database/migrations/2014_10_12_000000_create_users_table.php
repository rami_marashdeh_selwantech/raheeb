<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->string("phone_number")->unique()->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('password');
            $table->decimal('balance', 18, 4)->default(0);
            $table->string("avatar")->nullable();
            $table->string("cover_image")->nullable();
            $table->enum("login_with", [User::TWITTER, User::RAHEEB]);
            $table->string("id_image")->nullable();
            $table->string("id_number")->nullable();
            $table->boolean('is_blocked')->default(false);
            $table->boolean('is_valid')->default(false);
            $table->boolean('send_email')->default(false);
            $table->boolean('send_notifications')->default(true);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
