<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Chat\ChatPost;

class CreateChatPostMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_post_messages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('chat_post_id')->constrained()->onDelete('cascade'); 
            $table->foreignId('sender_id')->constrained("users")->onDelete('cascade'); 
            $table->foreignId('post_offer_id')->nullable(true)->constrained()->onDelete('cascade'); 
            $table->enum("message_type", [
                ChatPost::TYPE_FILE,
                ChatPost::TYPE_TEXT,
                ChatPost::TYPE_VOICE,
                ChatPost::TYPE_TEXT_OFFER
            ]);
            $table->text("message")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_post_messages');
    }
}
