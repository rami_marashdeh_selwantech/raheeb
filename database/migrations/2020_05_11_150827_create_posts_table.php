<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Post\Post;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade');  
            $table->foreignId('category_id')->constrained()->onDelete('cascade');
            $table->foreignId('condition_id')->constrained()->onDelete('cascade');  
            $table->enum("status" , [
                Post::STATUS_AVAILABLE,
                Post::STATUS_SOLD,
                Post::STATUS_SOLD_OTHER_APP
            ]);
            $table->string("title");
            $table->text("description");
            $table->decimal('price', 18, 4)->nullable();
            $table->float("lon", 8, 6)->nullable();
            $table->float("lat", 8, 6)->nullable();
            $table->boolean("is_ship_nationwide");
            $table->foreignId('category_box_size_id')->nullable(true)->constrained()->onDelete('cascade'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
