<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Post\PostSold;

class CreatePostSoldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_solds', function (Blueprint $table) {
            $table->id();
            $table->foreignId('post_id')->constrained()->onDelete('cascade');
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->enum("payment_method", [
                PostSold::SHIPPING_CACH,
                PostSold::SHIPPING_ONLINE,
            ])->nullable(true);
            $table->foreignId('post_offer_id')->nullable(true)->constrained()->onDelete('cascade');
            $table->decimal('total_price', 18, 4)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_solds');
    }
}
