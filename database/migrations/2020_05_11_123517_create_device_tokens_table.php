<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\DeviceToken\DeviceToken;

class CreateDeviceTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_tokens', function (Blueprint $table) {
            $table->id();
            $table->string("token");
            $table->string("locale")->default("en");
            $table->enum("platform", [
                DeviceToken::PLATFORM_ANDROID,
                DeviceToken::PLATFORM_IOS,
                DeviceToken::PLATFORM_WEB])->nullable();
            $table->foreignId('user_id')->nullable()->constrained()->onDelete('cascade');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_tokens');
    }
}
