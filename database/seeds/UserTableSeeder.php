<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                "name" => "Super Admin",
                "email" => "superadmin@raheeb.net",
                "phone_number" => "+962780321680",
                "role" => User::ROLE_SUPER_ADMIN
            ]
        ];

        foreach ($users as $user) {
            $u = new User;
            $u->name = $user["name"];
            $u->email = $user["email"];
            $u->phone_number = $user["phone_number"];
            $u->password = bcrypt(12345678);
            $u->is_valid = 1;
            $u->login_with = User::RAHEEB;
            $u->save();
            $u->assignRole($user['role']);
        }
    }
}
