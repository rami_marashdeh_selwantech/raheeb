<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleAndPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rolesPermissions = config("roles_permissions");
        $roles = $rolesPermissions["roles"];
        $permissions = $rolesPermissions["permissions"];

        // Create Roles
        foreach ($roles as $role) {
            if (!Role::where("name", $role)->first()) {
                Artisan::call("permission:create-role $role");
            }
        }

        // Create Permissions
        foreach ($permissions as $key => $permission) {
            foreach ($permission as $p) {
                $pName = $key . "_" . $p;
                if (!Permission::where("name", $pName)->first()) {
                    Artisan::call("permission:create-permission $pName web");
                }
            }
        }

        Artisan::call("permission:cache-reset");
    }
}
