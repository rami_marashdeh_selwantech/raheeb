<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use App\Models\Setting\Setting;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $settings = [

            'supported_currencies' => ['USD', 'JOD'],
            'default_currency' => 'JOD',
            "country_iso" => "JO",
            "country_name" => "Jordan",
            "primary_country_name" => "الأردن",
            "phone_contact" => [
                "0792*****",
                "0792*****",
                "0762*****",
            ],
            "email_contact" => [
                "info@raheeb.net",
                "support@raheeb.net",
                "help@raheeb.net",
            ],
            "default_timezone" => "Asia/Amman",

            // For Setings Translations
            "translatable" => [
                "privacy_policy" => "<h1>Privacy Policy </h1>",
                "about_us" => "<h1>About Us </h1>",
                "terms_and_condations" => "<h1>Terms And Condations</h1>"
            ],

            "tax_rate" => 5,

            "comuntion" => 0,
            "user_line_time" => 10,
            "pickup" => 500,

            "max_distance" => 200,
            "min_distance" => 5,
            "unit_distance" => "km"

        ];

        Setting::setMany($settings);
        Artisan::call("cache:clear");
    }
}
